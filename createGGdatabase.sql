USE [gringlobal]
GO
/****** Object:  User [gg_search]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE USER [gg_search] FOR LOGIN [gg_search] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [gg_user]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE USER [gg_user] FOR LOGIN [gg_user] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [gg_search]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [gg_search]
GO
ALTER ROLE [db_datareader] ADD MEMBER [gg_user]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [gg_user]
GO
/****** Object:  Table [dbo].[accession]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession](
	[accession_id] [int] IDENTITY(1,1) NOT NULL,
	[doi] [nvarchar](20) NULL,
	[accession_number_part1] [nvarchar](50) NOT NULL,
	[accession_number_part2] [int] NULL,
	[accession_number_part3] [nvarchar](50) NULL,
	[is_core] [nvarchar](1) NOT NULL,
	[is_backed_up] [nvarchar](1) NOT NULL,
	[backup_location1_site_id] [int] NULL,
	[backup_location2_site_id] [int] NULL,
	[status_code] [nvarchar](20) NOT NULL,
	[life_form_code] [nvarchar](20) NULL,
	[improvement_status_code] [nvarchar](20) NULL,
	[reproductive_uniformity_code] [nvarchar](20) NULL,
	[initial_received_form_code] [nvarchar](20) NULL,
	[initial_received_date] [datetime2](7) NULL,
	[initial_received_date_code] [nvarchar](20) NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession] PRIMARY KEY CLUSTERED 
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_action]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_action](
	[accession_action_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_id] [int] NOT NULL,
	[action_name_code] [nvarchar](20) NOT NULL,
	[started_date] [datetime2](7) NULL,
	[started_date_code] [nvarchar](20) NULL,
	[completed_date] [datetime2](7) NULL,
	[completed_date_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[cooperator_id] [int] NULL,
	[method_id] [int] NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_action] PRIMARY KEY CLUSTERED 
(
	[accession_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_annotation]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_annotation](
	[accession_inv_annotation_id] [int] IDENTITY(1,1) NOT NULL,
	[annotation_type_code] [nvarchar](20) NOT NULL,
	[annotation_date] [datetime2](7) NOT NULL,
	[annotation_date_code] [nvarchar](20) NULL,
	[annotation_cooperator_id] [int] NULL,
	[inventory_id] [int] NOT NULL,
	[order_request_id] [int] NULL,
	[old_taxonomy_species_id] [int] NULL,
	[new_taxonomy_species_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_annotation] PRIMARY KEY CLUSTERED 
(
	[accession_inv_annotation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_attach](
	[accession_inv_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[description_code] [nvarchar](20) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_attach] PRIMARY KEY CLUSTERED 
(
	[accession_inv_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_group]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_group](
	[accession_inv_group_id] [int] IDENTITY(1,1) NOT NULL,
	[group_name] [nvarchar](100) NOT NULL,
	[method_id] [int] NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_group] PRIMARY KEY CLUSTERED 
(
	[accession_inv_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_group_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_group_attach](
	[accession_inv_group_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_inv_group_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_group_attach] PRIMARY KEY CLUSTERED 
(
	[accession_inv_group_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_group_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_group_map](
	[accession_inv_group_map_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[accession_inv_group_id] [int] NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_group_map] PRIMARY KEY CLUSTERED 
(
	[accession_inv_group_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_name]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_name](
	[accession_inv_name_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[category_code] [nvarchar](20) NOT NULL,
	[plant_name] [nvarchar](200) NOT NULL,
	[plant_name_rank] [int] NULL,
	[name_group_id] [int] NULL,
	[name_source_cooperator_id] [int] NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_name] PRIMARY KEY CLUSTERED 
(
	[accession_inv_name_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_inv_voucher]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_inv_voucher](
	[accession_inv_voucher_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[collector_cooperator_id] [int] NULL,
	[collector_voucher_number] [nvarchar](40) NULL,
	[voucher_location] [nvarchar](255) NOT NULL,
	[vouchered_date] [datetime2](7) NULL,
	[vouchered_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_voucher] PRIMARY KEY CLUSTERED 
(
	[accession_inv_voucher_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_ipr]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_ipr](
	[accession_ipr_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_id] [int] NOT NULL,
	[type_code] [nvarchar](20) NOT NULL,
	[ipr_number] [nvarchar](50) NULL,
	[ipr_crop_name] [nvarchar](100) NULL,
	[ipr_full_name] [nvarchar](2000) NULL,
	[issued_date] [datetime2](7) NULL,
	[expired_date] [datetime2](7) NULL,
	[cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[accepted_date] [datetime2](7) NULL,
	[expected_date] [datetime2](7) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_ipr] PRIMARY KEY CLUSTERED 
(
	[accession_ipr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_pedigree]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_pedigree](
	[accession_pedigree_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_id] [int] NOT NULL,
	[released_date] [datetime2](7) NULL,
	[released_date_code] [nvarchar](20) NULL,
	[male_accession_id] [int] NULL,
	[male_external_accession] [nvarchar](50) NULL,
	[female_accession_id] [int] NULL,
	[female_external_accession] [nvarchar](50) NULL,
	[cross_code] [nvarchar](20) NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_pedigree] PRIMARY KEY CLUSTERED 
(
	[accession_pedigree_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_quarantine]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_quarantine](
	[accession_quarantine_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_id] [int] NOT NULL,
	[quarantine_type_code] [nvarchar](20) NOT NULL,
	[progress_status_code] [nvarchar](20) NULL,
	[custodial_cooperator_id] [int] NOT NULL,
	[entered_date] [datetime2](7) NULL,
	[established_date] [datetime2](7) NULL,
	[expected_release_date] [datetime2](7) NULL,
	[released_date] [datetime2](7) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_quarantine] PRIMARY KEY CLUSTERED 
(
	[accession_quarantine_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_source]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_source](
	[accession_source_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_id] [int] NOT NULL,
	[geography_id] [int] NULL,
	[acquisition_source_code] [nvarchar](20) NULL,
	[source_type_code] [nvarchar](20) NOT NULL,
	[source_date] [datetime2](7) NULL,
	[source_date_code] [nvarchar](20) NULL,
	[is_origin] [nvarchar](1) NOT NULL,
	[quantity_collected] [decimal](18, 5) NULL,
	[unit_quantity_collected_code] [nvarchar](20) NULL,
	[collected_form_code] [nvarchar](20) NULL,
	[number_plants_sampled] [int] NULL,
	[elevation_meters] [int] NULL,
	[collector_verbatim_locality] [nvarchar](max) NULL,
	[latitude] [decimal](18, 8) NULL,
	[longitude] [decimal](18, 8) NULL,
	[uncertainty] [int] NULL,
	[formatted_locality] [nvarchar](max) NULL,
	[georeference_datum] [nvarchar](10) NULL,
	[georeference_protocol_code] [nvarchar](20) NULL,
	[georeference_annotation] [nvarchar](max) NULL,
	[environment_description] [nvarchar](max) NULL,
	[associated_species] [nvarchar](max) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_source] PRIMARY KEY CLUSTERED 
(
	[accession_source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[accession_source_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accession_source_map](
	[accession_source_map_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_source_id] [int] NOT NULL,
	[cooperator_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_source_map] PRIMARY KEY CLUSTERED 
(
	[accession_source_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[app_resource]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_resource](
	[app_resource_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[app_name] [nvarchar](100) NOT NULL,
	[form_name] [nvarchar](100) NOT NULL,
	[app_resource_name] [nvarchar](100) NOT NULL,
	[description] [nvarchar](2000) NULL,
	[display_member] [nvarchar](2000) NOT NULL,
	[value_member] [nvarchar](2000) NULL,
	[sort_order] [int] NULL,
	[properties] [nvarchar](2000) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_app_resource] PRIMARY KEY CLUSTERED 
(
	[app_resource_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[app_setting]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_setting](
	[app_setting_id] [int] IDENTITY(1,1) NOT NULL,
	[category_tag] [nvarchar](200) NULL,
	[sort_order] [int] NULL,
	[name] [nvarchar](200) NOT NULL,
	[value] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_app_setting] PRIMARY KEY CLUSTERED 
(
	[app_setting_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[app_user_gui_setting]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_user_gui_setting](
	[app_user_gui_setting_id] [int] IDENTITY(1,1) NOT NULL,
	[cooperator_id] [int] NOT NULL,
	[app_name] [nvarchar](50) NULL,
	[form_name] [nvarchar](100) NULL,
	[resource_name] [nvarchar](100) NOT NULL,
	[resource_key] [nvarchar](100) NOT NULL,
	[resource_value] [nvarchar](2000) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_app_user_gui_setting] PRIMARY KEY CLUSTERED 
(
	[app_user_gui_setting_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[app_user_item_list]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_user_item_list](
	[app_user_item_list_id] [int] IDENTITY(1,1) NOT NULL,
	[cooperator_id] [int] NOT NULL,
	[tab_name] [nvarchar](100) NOT NULL,
	[list_name] [nvarchar](300) NOT NULL,
	[id_number] [int] NULL,
	[id_type] [nvarchar](100) NOT NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](1000) NOT NULL,
	[description] [nvarchar](max) NULL,
	[properties] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_app_user_item_list] PRIMARY KEY CLUSTERED 
(
	[app_user_item_list_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[citation]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[citation](
	[citation_id] [int] IDENTITY(1,1) NOT NULL,
	[literature_id] [int] NULL,
	[citation_title] [nvarchar](400) NULL,
	[author_name] [nvarchar](2000) NULL,
	[citation_year] [int] NULL,
	[reference] [nvarchar](200) NULL,
	[doi_reference] [nvarchar](500) NULL,
	[url] [nvarchar](500) NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[accession_id] [int] NULL,
	[method_id] [int] NULL,
	[taxonomy_species_id] [int] NULL,
	[taxonomy_genus_id] [int] NULL,
	[taxonomy_family_id] [int] NULL,
	[accession_ipr_id] [int] NULL,
	[accession_pedigree_id] [int] NULL,
	[genetic_marker_id] [int] NULL,
	[type_code] [nvarchar](20) NULL,
	[unique_key] [int] NULL,
	[is_accepted_name] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_citation] PRIMARY KEY CLUSTERED 
(
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[code_value]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[code_value](
	[code_value_id] [int] IDENTITY(1,1) NOT NULL,
	[group_name] [nvarchar](100) NOT NULL,
	[value] [nvarchar](20) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_code_value] PRIMARY KEY CLUSTERED 
(
	[code_value_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[code_value_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[code_value_lang](
	[code_value_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[code_value_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_code_value_lang] PRIMARY KEY CLUSTERED 
(
	[code_value_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[cooperator]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cooperator](
	[cooperator_id] [int] IDENTITY(1,1) NOT NULL,
	[current_cooperator_id] [int] NULL,
	[site_id] [int] NULL,
	[last_name] [nvarchar](100) NULL,
	[title] [nvarchar](10) NULL,
	[first_name] [nvarchar](100) NULL,
	[job] [nvarchar](100) NULL,
	[organization] [nvarchar](100) NULL,
	[organization_abbrev] [nvarchar](10) NULL,
	[address_line1] [nvarchar](100) NULL,
	[address_line2] [nvarchar](100) NULL,
	[address_line3] [nvarchar](100) NULL,
	[city] [nvarchar](100) NULL,
	[postal_index] [nvarchar](100) NULL,
	[geography_id] [int] NULL,
	[secondary_organization] [nvarchar](100) NULL,
	[secondary_organization_abbrev] [nvarchar](10) NULL,
	[secondary_address_line1] [nvarchar](100) NULL,
	[secondary_address_line2] [nvarchar](100) NULL,
	[secondary_address_line3] [nvarchar](100) NULL,
	[secondary_city] [nvarchar](100) NULL,
	[secondary_postal_index] [nvarchar](100) NULL,
	[secondary_geography_id] [int] NULL,
	[primary_phone] [nvarchar](30) NULL,
	[secondary_phone] [nvarchar](30) NULL,
	[fax] [nvarchar](30) NULL,
	[email] [nvarchar](100) NULL,
	[secondary_email] [nvarchar](100) NULL,
	[status_code] [nvarchar](20) NOT NULL,
	[category_code] [nvarchar](20) NULL,
	[organization_region_code] [nvarchar](20) NULL,
	[discipline_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[sys_lang_id] [int] NOT NULL,
	[web_cooperator_id] [int] NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_cooperator] PRIMARY KEY CLUSTERED 
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[cooperator_group]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cooperator_group](
	[cooperator_group_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](60) NOT NULL,
	[is_group_active] [nvarchar](1) NOT NULL,
	[site_id] [int] NULL,
	[category_code] [nvarchar](20) NULL,
	[group_tag] [nvarchar](1000) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_cooperator_group] PRIMARY KEY CLUSTERED 
(
	[cooperator_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[cooperator_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cooperator_map](
	[cooperator_map_id] [int] IDENTITY(1,1) NOT NULL,
	[cooperator_id] [int] NOT NULL,
	[cooperator_group_id] [int] NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_cooperator_map] PRIMARY KEY CLUSTERED 
(
	[cooperator_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop](
	[crop_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop] PRIMARY KEY CLUSTERED 
(
	[crop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_attach](
	[crop_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[attach_cooperator_id] [int] NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_attach] PRIMARY KEY CLUSTERED 
(
	[crop_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait](
	[crop_trait_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_id] [int] NOT NULL,
	[coded_name] [nvarchar](30) NOT NULL,
	[is_peer_reviewed] [nvarchar](1) NOT NULL,
	[category_code] [nvarchar](20) NOT NULL,
	[data_type_code] [nvarchar](20) NOT NULL,
	[is_coded] [nvarchar](1) NOT NULL,
	[max_length] [int] NULL,
	[numeric_format] [nvarchar](15) NULL,
	[numeric_maximum] [decimal](18, 5) NULL,
	[numeric_minimum] [decimal](18, 5) NULL,
	[original_value_type_code] [nvarchar](20) NULL,
	[original_value_format] [nvarchar](50) NULL,
	[is_archived] [nvarchar](1) NOT NULL,
	[ontology_url] [nvarchar](300) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait] PRIMARY KEY CLUSTERED 
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_attach](
	[crop_trait_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_trait_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[attach_cooperator_id] [int] NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_attach] PRIMARY KEY CLUSTERED 
(
	[crop_trait_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_code]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_code](
	[crop_trait_code_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_trait_id] [int] NOT NULL,
	[code] [nvarchar](30) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_code] PRIMARY KEY CLUSTERED 
(
	[crop_trait_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_code_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_code_attach](
	[crop_trait_code_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_trait_code_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[attach_cooperator_id] [int] NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_code_attach] PRIMARY KEY CLUSTERED 
(
	[crop_trait_code_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_code_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_code_lang](
	[crop_trait_code_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_trait_code_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_code_lang] PRIMARY KEY CLUSTERED 
(
	[crop_trait_code_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_lang](
	[crop_trait_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_trait_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_lang] PRIMARY KEY CLUSTERED 
(
	[crop_trait_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_observation]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_observation](
	[crop_trait_observation_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[crop_trait_id] [int] NOT NULL,
	[crop_trait_code_id] [int] NULL,
	[numeric_value] [decimal](18, 5) NULL,
	[string_value] [nvarchar](255) NULL,
	[method_id] [int] NOT NULL,
	[is_archived] [nvarchar](1) NOT NULL,
	[data_quality_code] [nvarchar](20) NULL,
	[original_value] [nvarchar](30) NULL,
	[frequency] [decimal](18, 5) NULL,
	[rank] [int] NULL,
	[mean_value] [decimal](18, 5) NULL,
	[maximum_value] [decimal](18, 5) NULL,
	[minimum_value] [decimal](18, 5) NULL,
	[standard_deviation] [decimal](18, 5) NULL,
	[sample_size] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_observation] PRIMARY KEY CLUSTERED 
(
	[crop_trait_observation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[crop_trait_observation_data]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crop_trait_observation_data](
	[crop_trait_observation_data_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_trait_observation_id] [int] NULL,
	[inventory_id] [int] NOT NULL,
	[individual] [int] NOT NULL,
	[crop_trait_id] [int] NULL,
	[crop_trait_code_id] [int] NULL,
	[numeric_value] [decimal](18, 5) NULL,
	[string_value] [nvarchar](255) NULL,
	[method_id] [int] NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_crop_trait_observation_data] PRIMARY KEY CLUSTERED 
(
	[crop_trait_observation_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[email]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[email](
	[email_id] [int] IDENTITY(1,1) NOT NULL,
	[id_type] [nvarchar](100) NULL,
	[id_number] [int] NULL,
	[email_from] [nvarchar](200) NULL,
	[email_to] [nvarchar](1000) NULL,
	[email_cc] [nvarchar](1000) NULL,
	[email_bcc] [nvarchar](1000) NULL,
	[email_reply_to] [nvarchar](200) NULL,
	[subject] [nvarchar](500) NULL,
	[body] [nvarchar](max) NULL,
	[is_html] [nvarchar](1) NOT NULL,
	[to_be_sent_date] [datetime2](7) NOT NULL,
	[sent_date] [datetime2](7) NULL,
	[retry_count] [int] NOT NULL,
	[last_retry_date] [datetime2](7) NULL,
	[last_retry_error_message] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_email] PRIMARY KEY CLUSTERED 
(
	[email_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[email_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[email_attach](
	[email_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[email_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_email_attach] PRIMARY KEY CLUSTERED 
(
	[email_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[exploration]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exploration](
	[exploration_id] [int] IDENTITY(1,1) NOT NULL,
	[exploration_number] [nvarchar](40) NOT NULL,
	[title] [nvarchar](240) NULL,
	[began_date] [datetime2](7) NULL,
	[finished_date] [datetime2](7) NULL,
	[funding_source] [nvarchar](100) NULL,
	[funding_amount] [decimal](10, 2) NULL,
	[target_species] [nvarchar](200) NULL,
	[permits] [nvarchar](240) NULL,
	[restrictions] [nvarchar](60) NULL,
	[fiscal_year] [int] NULL,
	[host_cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_exploration] PRIMARY KEY CLUSTERED 
(
	[exploration_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[exploration_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[exploration_map](
	[exploration_map_id] [int] IDENTITY(1,1) NOT NULL,
	[exploration_id] [int] NOT NULL,
	[cooperator_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_exploration_map] PRIMARY KEY CLUSTERED 
(
	[exploration_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback](
	[feedback_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[start_date] [datetime2](7) NOT NULL,
	[end_date] [datetime2](7) NOT NULL,
	[is_restricted_by_inventory] [nvarchar](1) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback] PRIMARY KEY CLUSTERED 
(
	[feedback_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_attach](
	[feedback_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_attach] PRIMARY KEY CLUSTERED 
(
	[feedback_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_form]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_form](
	[feedback_form_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_form] PRIMARY KEY CLUSTERED 
(
	[feedback_form_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_form_field]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_form_field](
	[feedback_form_field_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_form_id] [int] NOT NULL,
	[title] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[field_type_code] [nvarchar](20) NOT NULL,
	[gui_hint] [nvarchar](100) NULL,
	[foreign_key_dataview_name] [nvarchar](50) NULL,
	[group_name] [nvarchar](100) NULL,
	[sort_order] [int] NULL,
	[is_readonly] [nvarchar](1) NOT NULL,
	[is_required] [nvarchar](1) NOT NULL,
	[references_tag] [nvarchar](50) NULL,
	[default_value] [nvarchar](100) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_form_field] PRIMARY KEY CLUSTERED 
(
	[feedback_form_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_form_trait]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_form_trait](
	[feedback_form_trait_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_form_id] [int] NOT NULL,
	[crop_trait_id] [int] NOT NULL,
	[sort_order] [int] NOT NULL,
	[references_tag] [nvarchar](50) NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](max) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_form_trait] PRIMARY KEY CLUSTERED 
(
	[feedback_form_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_inventory](
	[feedback_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_id] [int] NOT NULL,
	[inventory_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_inventory] PRIMARY KEY CLUSTERED 
(
	[feedback_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_report]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_report](
	[feedback_report_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_id] [int] NOT NULL,
	[feedback_form_id] [int] NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[is_observation_data] [nvarchar](1) NOT NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[due_interval] [int] NOT NULL,
	[interval_length_code] [nvarchar](20) NOT NULL,
	[interval_type_code] [nvarchar](20) NOT NULL,
	[custom_due_date] [datetime2](7) NULL,
	[sort_order] [int] NOT NULL,
	[is_notified_initially] [nvarchar](1) NOT NULL,
	[is_notified_30days_prior] [nvarchar](1) NOT NULL,
	[is_notified_15days_prior] [nvarchar](1) NOT NULL,
	[initial_email_text] [nvarchar](max) NULL,
	[initial_email_subject] [nvarchar](500) NULL,
	[prior30_email_text] [nvarchar](max) NULL,
	[prior30_email_subject] [nvarchar](500) NULL,
	[prior15_email_text] [nvarchar](max) NULL,
	[prior15_email_subject] [nvarchar](500) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_report] PRIMARY KEY CLUSTERED 
(
	[feedback_report_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_report_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_report_attach](
	[feedback_report_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_report_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_report_attach] PRIMARY KEY CLUSTERED 
(
	[feedback_report_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_result]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_result](
	[feedback_result_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_result_group_id] [int] NOT NULL,
	[inventory_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_result] PRIMARY KEY CLUSTERED 
(
	[feedback_result_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_result_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_result_attach](
	[feedback_result_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_result_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_result_attach] PRIMARY KEY CLUSTERED 
(
	[feedback_result_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_result_field]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_result_field](
	[feedback_result_field_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_result_id] [int] NOT NULL,
	[feedback_form_field_id] [int] NOT NULL,
	[string_value] [nvarchar](500) NULL,
	[admin_value] [nvarchar](500) NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_result_field] PRIMARY KEY CLUSTERED 
(
	[feedback_result_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_result_group]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_result_group](
	[feedback_result_group_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_report_id] [int] NOT NULL,
	[participant_cooperator_id] [int] NOT NULL,
	[order_request_id] [int] NOT NULL,
	[started_date] [datetime2](7) NULL,
	[submitted_date] [datetime2](7) NULL,
	[accepted_date] [datetime2](7) NULL,
	[rejected_date] [datetime2](7) NULL,
	[due_date] [datetime2](7) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_result_group] PRIMARY KEY CLUSTERED 
(
	[feedback_result_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedback_result_trait_obs]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedback_result_trait_obs](
	[feedback_result_trait_obs_id] [int] IDENTITY(1,1) NOT NULL,
	[feedback_result_id] [int] NOT NULL,
	[feedback_form_trait_id] [int] NOT NULL,
	[inventory_id] [int] NOT NULL,
	[crop_trait_id] [int] NOT NULL,
	[crop_trait_code_id] [int] NULL,
	[numeric_value] [decimal](18, 5) NULL,
	[string_value] [nvarchar](255) NULL,
	[admin_value] [nvarchar](255) NULL,
	[method_id] [int] NULL,
	[is_archived] [nvarchar](1) NOT NULL,
	[data_quality_code] [nvarchar](20) NULL,
	[original_value] [nvarchar](30) NULL,
	[frequency] [decimal](18, 5) NULL,
	[rank] [int] NULL,
	[mean_value] [decimal](18, 5) NULL,
	[maximum_value] [decimal](18, 5) NULL,
	[minimum_value] [decimal](18, 5) NULL,
	[standard_deviation] [decimal](18, 5) NULL,
	[sample_size] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_feedback_result_trait_obs] PRIMARY KEY CLUSTERED 
(
	[feedback_result_trait_obs_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genetic_annotation]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genetic_annotation](
	[genetic_annotation_id] [int] IDENTITY(1,1) NOT NULL,
	[genetic_marker_id] [int] NOT NULL,
	[method_id] [int] NOT NULL,
	[assay_method] [nvarchar](max) NULL,
	[scoring_method] [nvarchar](max) NULL,
	[control_values] [nvarchar](max) NULL,
	[observation_alleles_count] [int] NULL,
	[max_gob_alleles] [int] NULL,
	[size_alleles] [nvarchar](100) NULL,
	[unusual_alleles] [nvarchar](100) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_genetic_annotation] PRIMARY KEY CLUSTERED 
(
	[genetic_annotation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genetic_marker]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genetic_marker](
	[genetic_marker_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_id] [int] NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[synonyms] [nvarchar](200) NULL,
	[repeat_motif] [nvarchar](100) NULL,
	[primers] [nvarchar](200) NULL,
	[assay_conditions] [nvarchar](4000) NULL,
	[range_products] [nvarchar](60) NULL,
	[genbank_number] [nvarchar](20) NULL,
	[known_standards] [nvarchar](max) NULL,
	[map_location] [nvarchar](100) NULL,
	[position] [nvarchar](1000) NULL,
	[poly_type_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_genetic_marker] PRIMARY KEY CLUSTERED 
(
	[genetic_marker_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genetic_observation]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genetic_observation](
	[genetic_observation_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[genetic_annotation_id] [int] NULL,
	[is_archived] [nvarchar](1) NOT NULL,
	[data_quality_code] [nvarchar](20) NULL,
	[frequency] [decimal](18, 5) NULL,
	[value] [nvarchar](1000) NULL,
	[rank] [int] NULL,
	[mean_value] [decimal](18, 5) NULL,
	[maximum_value] [decimal](18, 5) NULL,
	[minimum_value] [decimal](18, 5) NULL,
	[standard_deviation] [decimal](18, 5) NULL,
	[sample_size] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_genetic_observation] PRIMARY KEY CLUSTERED 
(
	[genetic_observation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genetic_observation_data]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genetic_observation_data](
	[genetic_observation_data_id] [int] IDENTITY(1,1) NOT NULL,
	[genetic_observation_id] [int] NULL,
	[genetic_annotation_id] [int] NOT NULL,
	[inventory_id] [int] NOT NULL,
	[individual] [int] NULL,
	[individual_allele_number] [int] NULL,
	[value] [nvarchar](1000) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_genetic_observation_data] PRIMARY KEY CLUSTERED 
(
	[genetic_observation_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[geneva_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[geneva_site_inventory](
	[geneva_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[restriction_code] [nvarchar](20) NULL,
	[flower_type_code] [nvarchar](20) NULL,
	[trueness_to_type_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_geneva_site_inventory] PRIMARY KEY CLUSTERED 
(
	[geneva_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[geography]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[geography](
	[geography_id] [int] IDENTITY(1,1) NOT NULL,
	[current_geography_id] [int] NULL,
	[country_code] [nvarchar](20) NOT NULL,
	[adm1] [nvarchar](100) NULL,
	[adm1_type_code] [nvarchar](20) NULL,
	[adm1_abbrev] [nvarchar](10) NULL,
	[adm2] [nvarchar](50) NULL,
	[adm2_type_code] [nvarchar](20) NULL,
	[adm2_abbrev] [nvarchar](10) NULL,
	[adm3] [nvarchar](50) NULL,
	[adm3_type_code] [nvarchar](20) NULL,
	[adm3_abbrev] [nvarchar](10) NULL,
	[adm4] [nvarchar](50) NULL,
	[adm4_type_code] [nvarchar](20) NULL,
	[adm4_abbrev] [nvarchar](10) NULL,
	[changed_date] [datetime2](7) NULL,
	[is_valid] [nvarchar](1) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_geography] PRIMARY KEY CLUSTERED 
(
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[geography_region_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[geography_region_map](
	[geography_region_map_id] [int] IDENTITY(1,1) NOT NULL,
	[geography_id] [int] NOT NULL,
	[region_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_geography_region_map] PRIMARY KEY CLUSTERED 
(
	[geography_region_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[gspi_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gspi_site_inventory](
	[gspi_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[seed_quantity] [int] NULL,
	[seed_age] [nvarchar](20) NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[increase_location] [nvarchar](20) NULL,
	[increase_year] [nvarchar](4) NULL,
	[lot_type_code] [nvarchar](20) NULL,
	[pollen_vector_code] [nvarchar](20) NULL,
	[pollen_environment] [nvarchar](20) NULL,
	[pollen_procedure_code] [nvarchar](20) NULL,
	[split_inventory_id] [int] NULL,
	[plot] [nvarchar](30) NULL,
	[note] [nvarchar](240) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_gspi_site_inventory] PRIMARY KEY CLUSTERED 
(
	[gspi_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory](
	[inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_number_part1] [nvarchar](50) NOT NULL,
	[inventory_number_part2] [int] NULL,
	[inventory_number_part3] [nvarchar](50) NULL,
	[form_type_code] [nvarchar](20) NOT NULL,
	[inventory_maint_policy_id] [int] NOT NULL,
	[is_distributable] [nvarchar](1) NOT NULL,
	[storage_location_part1] [nvarchar](20) NULL,
	[storage_location_part2] [nvarchar](20) NULL,
	[storage_location_part3] [nvarchar](20) NULL,
	[storage_location_part4] [nvarchar](20) NULL,
	[latitude] [decimal](18, 8) NULL,
	[longitude] [decimal](18, 8) NULL,
	[is_available] [nvarchar](1) NOT NULL,
	[web_availability_note] [nvarchar](max) NULL,
	[availability_status_code] [nvarchar](20) NOT NULL,
	[availability_status_note] [nvarchar](max) NULL,
	[availability_start_date] [datetime2](7) NULL,
	[availability_end_date] [datetime2](7) NULL,
	[quantity_on_hand] [decimal](18, 5) NULL,
	[quantity_on_hand_unit_code] [nvarchar](20) NULL,
	[is_auto_deducted] [nvarchar](1) NOT NULL,
	[distribution_default_form_code] [nvarchar](20) NULL,
	[distribution_default_quantity] [decimal](18, 5) NULL,
	[distribution_unit_code] [nvarchar](20) NULL,
	[distribution_critical_quantity] [decimal](18, 5) NULL,
	[regeneration_critical_quantity] [decimal](18, 5) NULL,
	[pathogen_status_code] [nvarchar](20) NULL,
	[accession_id] [int] NOT NULL,
	[parent_inventory_id] [int] NULL,
	[backup_inventory_id] [int] NULL,
	[rootstock] [nvarchar](200) NULL,
	[hundred_seed_weight] [decimal](18, 7) NULL,
	[pollination_method_code] [nvarchar](20) NULL,
	[pollination_vector_code] [nvarchar](20) NULL,
	[preservation_method_id] [int] NULL,
	[regeneration_method_id] [int] NULL,
	[plant_sex_code] [nvarchar](20) NULL,
	[propagation_date] [datetime2](7) NULL,
	[propagation_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
	[increment_site] [nvarchar](20) NULL,
	[Increment_year] [int] NULL,
 CONSTRAINT [PK_inventory] PRIMARY KEY CLUSTERED 
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_action]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_action](
	[inventory_action_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[action_name_code] [nvarchar](20) NOT NULL,
	[started_date] [datetime2](7) NULL,
	[started_date_code] [nvarchar](20) NULL,
	[completed_date] [datetime2](7) NULL,
	[completed_date_code] [nvarchar](20) NULL,
	[quantity] [decimal](18, 5) NULL,
	[quantity_unit_code] [nvarchar](20) NULL,
	[form_code] [nvarchar](20) NULL,
	[cooperator_id] [int] NULL,
	[method_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_action] PRIMARY KEY CLUSTERED 
(
	[inventory_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_maint_policy]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_maint_policy](
	[inventory_maint_policy_id] [int] IDENTITY(1,1) NOT NULL,
	[maintenance_name] [nvarchar](50) NOT NULL,
	[form_type_code] [nvarchar](20) NOT NULL,
	[quantity_on_hand_unit_code] [nvarchar](20) NULL,
	[web_availability_note] [nvarchar](max) NULL,
	[is_auto_deducted] [nvarchar](1) NOT NULL,
	[distribution_default_form_code] [nvarchar](20) NOT NULL,
	[distribution_default_quantity] [decimal](18, 5) NULL,
	[distribution_unit_code] [nvarchar](20) NULL,
	[distribution_critical_quantity] [decimal](18, 5) NULL,
	[regeneration_critical_quantity] [decimal](18, 5) NULL,
	[regeneration_method_code] [nvarchar](20) NULL,
	[curator_cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_maint_policy] PRIMARY KEY CLUSTERED 
(
	[inventory_maint_policy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_quality_status]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_quality_status](
	[inventory_quality_status_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[test_type_code] [nvarchar](20) NOT NULL,
	[contaminant_code] [nvarchar](20) NOT NULL,
	[test_result_code] [nvarchar](20) NULL,
	[plant_part_tested_code] [nvarchar](20) NULL,
	[test_results_score] [nvarchar](40) NULL,
	[test_results_score_type_code] [nvarchar](20) NULL,
	[negative_control] [decimal](10, 5) NULL,
	[positive_control] [decimal](10, 5) NULL,
	[replicate] [int] NULL,
	[started_date] [datetime2](7) NULL,
	[started_date_code] [nvarchar](20) NULL,
	[completed_date] [datetime2](7) NULL,
	[completed_date_code] [nvarchar](20) NULL,
	[required_replication_count] [int] NULL,
	[started_count] [int] NULL,
	[completed_count] [int] NULL,
	[plate_or_assay_number] [nvarchar](40) NULL,
	[method_id] [int] NULL,
	[tester_cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_quality_status] PRIMARY KEY CLUSTERED 
(
	[inventory_quality_status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_viability]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_viability](
	[inventory_viability_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[inventory_viability_rule_id] [int] NULL,
	[tested_date] [datetime2](7) NOT NULL,
	[tested_date_code] [nvarchar](20) NULL,
	[percent_normal] [int] NULL,
	[percent_abnormal] [int] NULL,
	[percent_dormant] [int] NULL,
	[percent_viable] [int] NULL,
	[percent_hard] [int] NULL,
	[percent_empty] [int] NULL,
	[percent_infested] [int] NULL,
	[percent_dead] [int] NULL,
	[percent_unknown] [int] NULL,
	[vigor_rating_code] [nvarchar](20) NULL,
	[total_tested_count] [int] NULL,
	[replication_count] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_viability_data]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_viability_data](
	[inventory_viability_data_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_viability_id] [int] NOT NULL,
	[order_request_item_id] [int] NULL,
	[counter_cooperator_id] [int] NULL,
	[replication_number] [int] NOT NULL,
	[count_number] [int] NOT NULL,
	[count_date] [datetime2](7) NOT NULL,
	[normal_count] [int] NOT NULL,
	[abnormal_count] [int] NULL,
	[dormant_count] [int] NULL,
	[hard_count] [int] NULL,
	[empty_count] [int] NULL,
	[infested_count] [int] NULL,
	[dead_count] [int] NULL,
	[unknown_count] [int] NULL,
	[estimated_dormant_count] [int] NULL,
	[treated_dormant_count] [int] NULL,
	[confirmed_dormant_count] [int] NULL,
	[replication_count] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability_data] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_viability_rule]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_viability_rule](
	[inventory_viability_rule_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[substrata] [nvarchar](100) NULL,
	[seeds_per_replicate] [int] NULL,
	[number_of_replicates] [int] NULL,
	[requirements] [nvarchar](max) NULL,
	[category_code] [nvarchar](20) NULL,
	[temperature_range] [nvarchar](100) NULL,
	[count_regime_days] [nvarchar](100) NULL,
	[moisture] [nvarchar](100) NULL,
	[prechill] [nvarchar](100) NULL,
	[lighting] [nvarchar](100) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability_rule] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_rule_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[inventory_viability_rule_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_viability_rule_map](
	[inventory_viability_rule_map_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_viability_rule_id] [int] NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability_rule_map] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_rule_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[literature]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[literature](
	[literature_id] [int] IDENTITY(1,1) NOT NULL,
	[abbreviation] [nvarchar](20) NOT NULL,
	[standard_abbreviation] [nvarchar](2000) NULL,
	[reference_title] [nvarchar](2000) NULL,
	[editor_author_name] [nvarchar](2000) NULL,
	[literature_type_code] [nvarchar](20) NULL,
	[publication_year] [nvarchar](50) NULL,
	[publisher_name] [nvarchar](2000) NULL,
	[publisher_location] [nvarchar](2000) NULL,
	[url] [nvarchar](500) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_literature] PRIMARY KEY CLUSTERED 
(
	[literature_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[method]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[method](
	[method_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[geography_id] [int] NULL,
	[elevation_meters] [int] NULL,
	[latitude] [decimal](18, 8) NULL,
	[longitude] [decimal](18, 8) NULL,
	[uncertainty] [int] NULL,
	[formatted_locality] [nvarchar](max) NULL,
	[georeference_datum] [nvarchar](10) NULL,
	[georeference_protocol_code] [nvarchar](20) NULL,
	[georeference_annotation] [nvarchar](max) NULL,
	[materials_and_methods] [nvarchar](max) NULL,
	[study_reason_code] [nvarchar](20) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_method] PRIMARY KEY CLUSTERED 
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[method_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[method_attach](
	[method_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[method_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_method_attach] PRIMARY KEY CLUSTERED 
(
	[method_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[method_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[method_map](
	[method_map_id] [int] IDENTITY(1,1) NOT NULL,
	[cooperator_id] [int] NOT NULL,
	[method_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_method_map] PRIMARY KEY CLUSTERED 
(
	[method_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[name_group]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[name_group](
	[name_group_id] [int] IDENTITY(1,1) NOT NULL,
	[group_name] [nvarchar](20) NOT NULL,
	[note] [nvarchar](max) NULL,
	[url] [nvarchar](2000) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_name_group] PRIMARY KEY CLUSTERED 
(
	[name_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[nc7_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nc7_site_inventory](
	[nc7_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[pollination_control_code] [nvarchar](20) NULL,
	[farm_field_identifier_code] [nvarchar](20) NULL,
	[location_type_code] [nvarchar](20) NULL,
	[location_low] [nvarchar](10) NULL,
	[location_high] [nvarchar](10) NULL,
	[sublocation_type_code] [nvarchar](20) NULL,
	[sublocation_low] [nvarchar](10) NULL,
	[sublocation_high] [nvarchar](10) NULL,
	[old_inventory_identifier] [nvarchar](30) NULL,
	[inventory_site_note] [nvarchar](max) NULL,
	[inventory_location1_latitude] [decimal](18, 8) NULL,
	[inventory_location1_longitude] [decimal](18, 8) NULL,
	[inventory_location1_precision] [int] NULL,
	[inventory_location2_latitude] [decimal](18, 8) NULL,
	[inventory_location2_longitude] [decimal](18, 8) NULL,
	[inventory_location2_precision] [int] NULL,
	[inventory_datum_code] [nvarchar](20) NULL,
	[coordinates_apply_to_code] [nvarchar](20) NULL,
	[coordinates_comment] [nvarchar](max) NULL,
	[coordinates_voucher_location] [nvarchar](500) NULL,
	[irregular_inventory_location] [nvarchar](500) NULL,
	[is_increase_success_flag] [nvarchar](1) NULL,
	[reason_unsuccessfull1_code] [nvarchar](20) NULL,
	[reason_unsuccessfull2_code] [nvarchar](20) NULL,
	[reason_unsuccessfull3_code] [nvarchar](20) NULL,
	[reason_unsuccessfull_note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_nc7_site_inventory] PRIMARY KEY CLUSTERED 
(
	[nc7_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ne9_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ne9_site_inventory](
	[ne9_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[lot_type_code] [nvarchar](20) NULL,
	[inventory_purpose_code] [nvarchar](20) NULL,
	[pollination_proc_code] [nvarchar](20) NULL,
	[pollination_vector_code] [nvarchar](20) NULL,
	[pollination_env_code] [nvarchar](20) NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[parent_2] [nvarchar](25) NULL,
	[parent_3] [nvarchar](25) NULL,
	[parent_4] [nvarchar](25) NULL,
	[parent_5] [nvarchar](25) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_ne9_site_inventory] PRIMARY KEY CLUSTERED 
(
	[ne9_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[nssl_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nssl_site_inventory](
	[nssl_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[next_test] [nvarchar](10) NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[moisture_1] [decimal](18, 5) NULL,
	[moisture_1_type_code] [nvarchar](20) NULL,
	[moisture_1_date] [datetime2](7) NULL,
	[moisture_2] [decimal](18, 5) NULL,
	[moisture_2_type_code] [nvarchar](20) NULL,
	[moisture_2_date] [datetime2](7) NULL,
	[moisture_3] [decimal](18, 5) NULL,
	[moisture_3_type_code] [nvarchar](20) NULL,
	[moisture_3_date] [datetime2](7) NULL,
	[moisture_4] [decimal](18, 5) NULL,
	[moisture_4_type_code] [nvarchar](20) NULL,
	[moisture_4_date] [datetime2](7) NULL,
	[moisture_5] [decimal](18, 5) NULL,
	[moisture_5_type_code] [nvarchar](20) NULL,
	[moisture_5_date] [datetime2](7) NULL,
	[percent_cleanout] [decimal](18, 5) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_nssl_site_inventory] PRIMARY KEY CLUSTERED 
(
	[nssl_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[opgc_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[opgc_site_inventory](
	[opgc_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[location_code] [nvarchar](20) NULL,
	[location_type_code] [nvarchar](20) NULL,
	[old_inventory_identifier] [nvarchar](30) NULL,
	[farm_field_identifier_code] [nvarchar](20) NULL,
	[cage_greenhouse] [int] NULL,
	[technique_code] [nvarchar](20) NULL,
	[pollination_control_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_opgc_site_inventory] PRIMARY KEY CLUSTERED 
(
	[opgc_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_request]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_request](
	[order_request_id] [int] IDENTITY(1,1) NOT NULL,
	[original_order_request_id] [int] NULL,
	[web_order_request_id] [int] NULL,
	[local_number] [nvarchar](50) NULL,
	[order_type_code] [nvarchar](20) NULL,
	[ordered_date] [datetime2](7) NULL,
	[intended_use_code] [nvarchar](20) NULL,
	[intended_use_note] [nvarchar](max) NULL,
	[completed_date] [datetime2](7) NULL,
	[requestor_cooperator_id] [int] NULL,
	[ship_to_cooperator_id] [int] NULL,
	[final_recipient_cooperator_id] [int] NOT NULL,
	[order_obtained_via] [nvarchar](20) NULL,
	[feedback_id] [int] NULL,
	[special_instruction] [nvarchar](max) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request] PRIMARY KEY CLUSTERED 
(
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_request_action]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_request_action](
	[order_request_action_id] [int] IDENTITY(1,1) NOT NULL,
	[order_request_id] [int] NOT NULL,
	[action_name_code] [nvarchar](20) NOT NULL,
	[started_date] [datetime2](7) NOT NULL,
	[started_date_code] [nvarchar](20) NULL,
	[completed_date] [datetime2](7) NULL,
	[completed_date_code] [nvarchar](20) NULL,
	[action_information] [nvarchar](max) NULL,
	[action_cost] [decimal](18, 5) NULL,
	[cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request_action] PRIMARY KEY CLUSTERED 
(
	[order_request_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_request_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_request_attach](
	[order_request_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[order_request_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request_attach] PRIMARY KEY CLUSTERED 
(
	[order_request_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_request_item]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_request_item](
	[order_request_item_id] [int] IDENTITY(1,1) NOT NULL,
	[order_request_id] [int] NOT NULL,
	[web_order_request_item_id] [int] NULL,
	[sequence_number] [int] NULL,
	[name] [nvarchar](200) NULL,
	[quantity_shipped] [decimal](18, 5) NULL,
	[quantity_shipped_unit_code] [nvarchar](20) NULL,
	[distribution_form_code] [nvarchar](20) NULL,
	[status_code] [nvarchar](20) NOT NULL,
	[status_date] [datetime2](7) NULL,
	[inventory_id] [int] NOT NULL,
	[external_taxonomy] [nvarchar](100) NULL,
	[source_cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[web_user_note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request_item] PRIMARY KEY CLUSTERED 
(
	[order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_request_item_action]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_request_item_action](
	[order_request_item_action_id] [int] IDENTITY(1,1) NOT NULL,
	[order_request_item_id] [int] NOT NULL,
	[action_name_code] [nvarchar](20) NOT NULL,
	[started_date] [datetime2](7) NOT NULL,
	[started_date_code] [nvarchar](20) NULL,
	[completed_date] [datetime2](7) NULL,
	[completed_date_code] [nvarchar](20) NULL,
	[action_information] [nvarchar](max) NULL,
	[action_cost] [decimal](18, 5) NULL,
	[cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request_item_action] PRIMARY KEY CLUSTERED 
(
	[order_request_item_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order_request_phyto_log]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_request_phyto_log](
	[order_request_phyto_log_id] [int] IDENTITY(1,1) NOT NULL,
	[order_request_id] [int] NOT NULL,
	[received_date] [datetime2](7) NOT NULL,
	[opened_date] [datetime2](7) NULL,
	[number_of_packages] [nvarchar](20) NULL,
	[number_of_items] [int] NULL,
	[major_genus] [nvarchar](100) NULL,
	[import_permit_identifier] [nvarchar](60) NULL,
	[setup_inspection_date] [datetime2](7) NULL,
	[inspected_date] [datetime2](7) NULL,
	[inspected_by_cooperator_id] [int] NULL,
	[phyto_certificate_type_code] [nvarchar](20) NULL,
	[certificate_cost] [int] NULL,
	[phyto_certificate_identifier] [nvarchar](60) NULL,
	[shipped_date] [datetime2](7) NULL,
	[shipping_carrier] [nvarchar](60) NULL,
	[tracking_identifier] [nvarchar](100) NULL,
	[passed_inspection] [nvarchar](1) NULL,
	[delivered] [nvarchar](1) NULL,
	[delivered_date] [datetime2](7) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request_phyto_log] PRIMARY KEY CLUSTERED 
(
	[order_request_phyto_log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[parl_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[parl_site_inventory](
	[parl_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[seed_quantity] [int] NULL,
	[seed_quantity_type] [int] NULL,
	[seed_age] [nvarchar](20) NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[increase_location] [nvarchar](20) NULL,
	[increase_year] [int] NULL,
	[lot_type_code] [nvarchar](20) NULL,
	[pollination_vector_code] [nvarchar](20) NULL,
	[pollination_environment] [nvarchar](20) NULL,
	[pollination_procedure_code] [nvarchar](20) NULL,
	[plot] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_parl_site_inventory] PRIMARY KEY CLUSTERED 
(
	[parl_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[region]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[region](
	[region_id] [int] IDENTITY(1,1) NOT NULL,
	[continent] [nvarchar](20) NOT NULL,
	[subcontinent] [nvarchar](30) NULL,
	[sequence_number] [int] NULL,
	[continent_abbreviation] [nvarchar](20) NULL,
	[subcontinent_abbreviation] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_region] PRIMARY KEY CLUSTERED 
(
	[region_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[s9_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s9_site_inventory](
	[s9_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[increase_location_code] [nvarchar](20) NULL,
	[plot_number] [nvarchar](20) NULL,
	[hundred_weight] [decimal](18, 5) NULL,
	[pollination_procedure_code] [nvarchar](20) NULL,
	[virus_status_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_s9_site_inventory] PRIMARY KEY CLUSTERED 
(
	[s9_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[site]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[site](
	[site_id] [int] IDENTITY(1,1) NOT NULL,
	[site_short_name] [nvarchar](20) NOT NULL,
	[site_long_name] [nvarchar](200) NOT NULL,
	[provider_identifier] [nvarchar](6) NULL,
	[organization_abbrev] [nvarchar](20) NULL,
	[is_internal] [nvarchar](1) NOT NULL,
	[is_distribution_site] [nvarchar](1) NOT NULL,
	[type_code] [nvarchar](20) NULL,
	[fao_institute_number] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_site] PRIMARY KEY CLUSTERED 
(
	[site_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[source_desc_observation]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[source_desc_observation](
	[source_desc_observation_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_source_id] [int] NOT NULL,
	[source_descriptor_id] [int] NOT NULL,
	[source_descriptor_code_id] [int] NULL,
	[numeric_value] [decimal](18, 5) NULL,
	[string_value] [nvarchar](255) NULL,
	[original_value] [nvarchar](30) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_source_desc_observation] PRIMARY KEY CLUSTERED 
(
	[source_desc_observation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[source_descriptor]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[source_descriptor](
	[source_descriptor_id] [int] IDENTITY(1,1) NOT NULL,
	[coded_name] [nvarchar](30) NOT NULL,
	[category_code] [nvarchar](20) NOT NULL,
	[data_type_code] [nvarchar](20) NOT NULL,
	[is_coded] [nvarchar](1) NOT NULL,
	[max_length] [int] NULL,
	[numeric_format] [nvarchar](15) NULL,
	[numeric_maximum] [decimal](18, 5) NULL,
	[numeric_minimum] [decimal](18, 5) NULL,
	[original_value_type_code] [nvarchar](20) NULL,
	[original_value_format] [nvarchar](50) NULL,
	[ontology_url] [nvarchar](300) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_source_descriptor] PRIMARY KEY CLUSTERED 
(
	[source_descriptor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[source_descriptor_code]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[source_descriptor_code](
	[source_descriptor_code_id] [int] IDENTITY(1,1) NOT NULL,
	[source_descriptor_id] [int] NOT NULL,
	[code] [nvarchar](30) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_source_descriptor_code] PRIMARY KEY CLUSTERED 
(
	[source_descriptor_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[source_descriptor_code_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[source_descriptor_code_lang](
	[source_descriptor_code_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[source_descriptor_code_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_source_descriptor_code_lang] PRIMARY KEY CLUSTERED 
(
	[source_descriptor_code_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[source_descriptor_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[source_descriptor_lang](
	[source_descriptor_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[source_descriptor_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_source_descriptor_lang] PRIMARY KEY CLUSTERED 
(
	[source_descriptor_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_database]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_database](
	[sys_database_id] [int] IDENTITY(1,1) NOT NULL,
	[migration_number] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_database] PRIMARY KEY CLUSTERED 
(
	[sys_database_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_database_migration]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_database_migration](
	[sys_database_migration_id] [int] IDENTITY(1,1) NOT NULL,
	[migration_number] [int] NOT NULL,
	[sort_order] [int] NOT NULL,
	[action_type] [nvarchar](50) NOT NULL,
	[action_up] [nvarchar](max) NULL,
	[action_down] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_database_migration] PRIMARY KEY CLUSTERED 
(
	[sys_database_migration_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_database_migration_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_database_migration_lang](
	[sys_database_migration_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_database_migration_id] [int] NOT NULL,
	[language_iso_639_3_code] [nvarchar](5) NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_database_migration_lang] PRIMARY KEY CLUSTERED 
(
	[sys_database_migration_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_datatrigger]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_datatrigger](
	[sys_datatrigger_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NULL,
	[sys_table_id] [int] NULL,
	[virtual_file_path] [nvarchar](255) NULL,
	[assembly_name] [nvarchar](255) NOT NULL,
	[fully_qualified_class_name] [nvarchar](255) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[is_system] [nvarchar](1) NOT NULL,
	[sort_order] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_datatrigger] PRIMARY KEY CLUSTERED 
(
	[sys_datatrigger_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_datatrigger_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_datatrigger_lang](
	[sys_datatrigger_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_datatrigger_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_datatrigger_lang] PRIMARY KEY CLUSTERED 
(
	[sys_datatrigger_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dataview]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_dataview](
	[sys_dataview_id] [int] IDENTITY(1,1) NOT NULL,
	[dataview_name] [nvarchar](100) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[is_readonly] [nvarchar](1) NOT NULL,
	[category_code] [nvarchar](100) NULL,
	[database_area_code] [nvarchar](50) NULL,
	[database_area_code_sort_order] [int] NULL,
	[is_transform] [nvarchar](1) NOT NULL,
	[transform_field_for_names] [nvarchar](50) NULL,
	[transform_field_for_captions] [nvarchar](50) NULL,
	[transform_field_for_values] [nvarchar](50) NULL,
	[configuration_options] [nvarchar](500) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_dataview] PRIMARY KEY CLUSTERED 
(
	[sys_dataview_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dataview_field]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_dataview_field](
	[sys_dataview_field_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NOT NULL,
	[field_name] [nvarchar](50) NOT NULL,
	[sys_table_field_id] [int] NULL,
	[is_readonly] [nvarchar](1) NOT NULL,
	[is_primary_key] [nvarchar](1) NOT NULL,
	[is_transform] [nvarchar](1) NOT NULL,
	[sort_order] [int] NOT NULL,
	[gui_hint] [nvarchar](100) NULL,
	[foreign_key_dataview_name] [nvarchar](50) NULL,
	[group_name] [nvarchar](100) NULL,
	[table_alias_name] [nvarchar](100) NULL,
	[is_visible] [nvarchar](1) NOT NULL,
	[configuration_options] [nvarchar](500) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_dataview_field] PRIMARY KEY CLUSTERED 
(
	[sys_dataview_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dataview_field_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_dataview_field_lang](
	[sys_dataview_field_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_field_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_dataview_field_lang] PRIMARY KEY CLUSTERED 
(
	[sys_dataview_field_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dataview_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_dataview_lang](
	[sys_dataview_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_dataview_lang] PRIMARY KEY CLUSTERED 
(
	[sys_dataview_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dataview_param]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_dataview_param](
	[sys_dataview_param_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NOT NULL,
	[param_name] [nvarchar](50) NOT NULL,
	[param_type] [nvarchar](50) NULL,
	[sort_order] [int] NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_dataview_param] PRIMARY KEY CLUSTERED 
(
	[sys_dataview_param_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_dataview_sql]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_dataview_sql](
	[sys_dataview_sql_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NOT NULL,
	[database_engine_tag] [nvarchar](10) NOT NULL,
	[sql_statement] [nvarchar](max) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_dataview_sql] PRIMARY KEY CLUSTERED 
(
	[sys_dataview_sql_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_file]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_file](
	[sys_file_id] [int] IDENTITY(1,1) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[virtual_file_path] [nvarchar](255) NOT NULL,
	[file_name] [nvarchar](255) NULL,
	[file_version] [nvarchar](255) NULL,
	[file_size] [decimal](18, 0) NULL,
	[display_name] [nvarchar](255) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_file] PRIMARY KEY CLUSTERED 
(
	[sys_file_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_file_group]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_file_group](
	[sys_file_group_id] [int] IDENTITY(1,1) NOT NULL,
	[group_name] [nvarchar](100) NOT NULL,
	[version_name] [nvarchar](50) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_file_group] PRIMARY KEY CLUSTERED 
(
	[sys_file_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_file_group_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_file_group_map](
	[sys_file_group_map_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_file_group_id] [int] NOT NULL,
	[sys_file_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_file_group_map] PRIMARY KEY CLUSTERED 
(
	[sys_file_group_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_file_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_file_lang](
	[sys_file_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_file_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_file_lang] PRIMARY KEY CLUSTERED 
(
	[sys_file_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_group]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_group](
	[sys_group_id] [int] IDENTITY(1,1) NOT NULL,
	[group_tag] [nvarchar](1000) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_group] PRIMARY KEY CLUSTERED 
(
	[sys_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_group_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_group_lang](
	[sys_group_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_group_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_group_lang] PRIMARY KEY CLUSTERED 
(
	[sys_group_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_group_permission_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_group_permission_map](
	[sys_group_permission_map_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_group_id] [int] NOT NULL,
	[sys_permission_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_group_permission_map] PRIMARY KEY CLUSTERED 
(
	[sys_group_permission_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_group_user_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_group_user_map](
	[sys_group_user_map_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_group_id] [int] NOT NULL,
	[sys_user_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_group_user_map] PRIMARY KEY CLUSTERED 
(
	[sys_group_user_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_index]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_index](
	[sys_index_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_table_id] [int] NOT NULL,
	[index_name] [nvarchar](50) NOT NULL,
	[is_unique] [nvarchar](1) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_index] PRIMARY KEY CLUSTERED 
(
	[sys_index_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_index_field]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_index_field](
	[sys_index_field_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_index_id] [int] NOT NULL,
	[sys_table_field_id] [int] NOT NULL,
	[sort_order] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_index_field] PRIMARY KEY CLUSTERED 
(
	[sys_index_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_lang](
	[sys_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[iso_639_3_tag] [nvarchar](5) NOT NULL,
	[ietf_tag] [nvarchar](30) NOT NULL,
	[script_direction] [nvarchar](3) NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_lang] PRIMARY KEY CLUSTERED 
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_permission]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_permission](
	[sys_permission_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NULL,
	[sys_table_id] [int] NULL,
	[permission_tag] [nvarchar](1000) NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[create_permission] [nvarchar](1) NOT NULL,
	[read_permission] [nvarchar](1) NOT NULL,
	[update_permission] [nvarchar](1) NOT NULL,
	[delete_permission] [nvarchar](1) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_permission] PRIMARY KEY CLUSTERED 
(
	[sys_permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_permission_field]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_permission_field](
	[sys_permission_field_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_permission_id] [int] NOT NULL,
	[sys_dataview_field_id] [int] NULL,
	[sys_table_field_id] [int] NULL,
	[field_type] [nvarchar](20) NULL,
	[compare_operator] [nvarchar](20) NULL,
	[compare_value] [nvarchar](max) NULL,
	[parent_table_field_id] [int] NULL,
	[parent_field_type] [nvarchar](20) NULL,
	[parent_compare_operator] [nvarchar](20) NULL,
	[parent_compare_value] [nvarchar](max) NULL,
	[compare_mode] [nvarchar](20) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_permission_field] PRIMARY KEY CLUSTERED 
(
	[sys_permission_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_permission_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_permission_lang](
	[sys_permission_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_permission_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_permission_lang] PRIMARY KEY CLUSTERED 
(
	[sys_permission_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_search_autofield]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_search_autofield](
	[sys_search_autofield_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_table_field_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_search_autofield] PRIMARY KEY CLUSTERED 
(
	[sys_search_autofield_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_search_resolver]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_search_resolver](
	[sys_search_resolver_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_dataview_id] [int] NOT NULL,
	[resolved_pkey_name] [nvarchar](100) NOT NULL,
	[sys_table_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_search_resolver] PRIMARY KEY CLUSTERED 
(
	[sys_search_resolver_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_table]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_table](
	[sys_table_id] [int] IDENTITY(1,1) NOT NULL,
	[table_name] [nvarchar](50) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[is_readonly] [nvarchar](1) NOT NULL,
	[audits_created] [nvarchar](1) NOT NULL,
	[audits_modified] [nvarchar](1) NOT NULL,
	[audits_owned] [nvarchar](1) NOT NULL,
	[database_area_code] [nvarchar](100) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_table] PRIMARY KEY CLUSTERED 
(
	[sys_table_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_table_field]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_table_field](
	[sys_table_field_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_table_id] [int] NOT NULL,
	[field_name] [nvarchar](50) NOT NULL,
	[field_ordinal] [int] NULL,
	[field_purpose] [nvarchar](50) NOT NULL,
	[field_type] [nvarchar](50) NOT NULL,
	[default_value] [nvarchar](50) NULL,
	[is_primary_key] [nvarchar](1) NOT NULL,
	[is_foreign_key] [nvarchar](1) NOT NULL,
	[foreign_key_table_field_id] [int] NULL,
	[foreign_key_dataview_name] [nvarchar](100) NULL,
	[is_nullable] [nvarchar](1) NOT NULL,
	[gui_hint] [nvarchar](50) NOT NULL,
	[is_readonly] [nvarchar](1) NOT NULL,
	[min_length] [int] NOT NULL,
	[max_length] [int] NOT NULL,
	[numeric_precision] [int] NOT NULL,
	[numeric_scale] [int] NOT NULL,
	[is_autoincrement] [nvarchar](1) NOT NULL,
	[group_name] [nvarchar](100) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_table_field] PRIMARY KEY CLUSTERED 
(
	[sys_table_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_table_field_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_table_field_lang](
	[sys_table_field_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_table_field_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_table_field_lang] PRIMARY KEY CLUSTERED 
(
	[sys_table_field_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_table_lang]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_table_lang](
	[sys_table_lang_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_table_id] [int] NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[description] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_table_lang] PRIMARY KEY CLUSTERED 
(
	[sys_table_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_table_relationship]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_table_relationship](
	[sys_table_relationship_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_table_field_id] [int] NULL,
	[relationship_type_tag] [nvarchar](20) NOT NULL,
	[other_table_field_id] [int] NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[modified_by] [int] NULL,
	[modified_date] [datetime2](7) NULL,
	[owned_by] [int] NOT NULL,
	[owned_date] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_sys_table_relationship] PRIMARY KEY CLUSTERED 
(
	[sys_table_relationship_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_user]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_user](
	[sys_user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](2000) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[cooperator_id] [int] NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_user] PRIMARY KEY CLUSTERED 
(
	[sys_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sys_user_permission_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_user_permission_map](
	[sys_user_permission_map_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_user_id] [int] NOT NULL,
	[sys_permission_id] [int] NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_sys_user_permission_map] PRIMARY KEY CLUSTERED 
(
	[sys_user_permission_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_alt_family_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_alt_family_map](
	[taxonomy_alt_family_map_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_genus_id] [int] NOT NULL,
	[taxonomy_family_id] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_alt_family_map] PRIMARY KEY CLUSTERED 
(
	[taxonomy_alt_family_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_attach](
	[taxonomy_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_family_id] [int] NULL,
	[taxonomy_genus_id] [int] NULL,
	[taxonomy_species_id] [int] NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_attach] PRIMARY KEY CLUSTERED 
(
	[taxonomy_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_author]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_author](
	[taxonomy_author_id] [int] IDENTITY(1,1) NOT NULL,
	[short_name] [nvarchar](30) NOT NULL,
	[full_name] [nvarchar](100) NOT NULL,
	[short_name_expanded_diacritic] [nvarchar](30) NULL,
	[full_name_expanded_diacritic] [nvarchar](100) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_author] PRIMARY KEY CLUSTERED 
(
	[taxonomy_author_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_common_name]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_common_name](
	[taxonomy_common_name_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_genus_id] [int] NULL,
	[taxonomy_species_id] [int] NULL,
	[language_description] [nvarchar](100) NULL,
	[name] [nvarchar](100) NOT NULL,
	[simplified_name] [nvarchar](100) NULL,
	[alternate_transcription] [nvarchar](100) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_common_name] PRIMARY KEY CLUSTERED 
(
	[taxonomy_common_name_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_crop_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_crop_map](
	[taxonomy_crop_map_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[crop_id] [int] NOT NULL,
	[alternate_crop_name] [nvarchar](100) NOT NULL,
	[common_crop_name] [nvarchar](100) NULL,
	[is_primary_genepool] [nvarchar](1) NOT NULL,
	[is_secondary_genepool] [nvarchar](1) NOT NULL,
	[is_tertiary_genepool] [nvarchar](1) NOT NULL,
	[is_quaternary_genepool] [nvarchar](1) NOT NULL,
	[is_graftstock_genepool] [nvarchar](1) NOT NULL,
	[crop_genepool_reviewers] [nvarchar](500) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_crop_map] PRIMARY KEY CLUSTERED 
(
	[taxonomy_crop_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_cwr]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_cwr](
	[taxonomy_cwr_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[crop_name] [nvarchar](100) NOT NULL,
	[display_name] [nvarchar](100) NULL,
	[crop_common_name] [nvarchar](100) NULL,
	[is_crop] [nvarchar](1) NOT NULL,
	[genepool_code] [nvarchar](20) NULL,
	[is_graftstock_genepool] [nvarchar](1) NOT NULL,
	[trait_class_code] [nvarchar](20) NULL,
	[is_potential] [nvarchar](1) NOT NULL,
	[breeding_type_code] [nvarchar](20) NULL,
	[breeding_usage] [nvarchar](100) NULL,
	[ontology_trait_identifier] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_cwr] PRIMARY KEY CLUSTERED 
(
	[taxonomy_cwr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_cwr_priority]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_cwr_priority](
	[taxonomy_cwr_priority_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[priority_year] [int] NOT NULL,
	[status_code] [nvarchar](20) NOT NULL,
	[in_situ_code] [nvarchar](20) NULL,
	[ex_situ_code] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_cwr_priority] PRIMARY KEY CLUSTERED 
(
	[taxonomy_cwr_priority_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_family]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_family](
	[taxonomy_family_id] [int] IDENTITY(1,1) NOT NULL,
	[current_taxonomy_family_id] [int] NULL,
	[type_taxonomy_genus_id] [int] NULL,
	[suprafamily_rank_code] [nvarchar](20) NULL,
	[suprafamily_rank_name] [nvarchar](100) NULL,
	[family_name] [nvarchar](25) NOT NULL,
	[family_authority] [nvarchar](100) NULL,
	[alternate_name] [nvarchar](25) NULL,
	[subfamily_name] [nvarchar](25) NULL,
	[tribe_name] [nvarchar](25) NULL,
	[subtribe_name] [nvarchar](25) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_family] PRIMARY KEY CLUSTERED 
(
	[taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_genus]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_genus](
	[taxonomy_genus_id] [int] IDENTITY(1,1) NOT NULL,
	[current_taxonomy_genus_id] [int] NULL,
	[taxonomy_family_id] [int] NOT NULL,
	[qualifying_code] [nvarchar](20) NULL,
	[hybrid_code] [nvarchar](20) NULL,
	[genus_name] [nvarchar](30) NOT NULL,
	[genus_authority] [nvarchar](100) NULL,
	[subgenus_name] [nvarchar](30) NULL,
	[section_name] [nvarchar](30) NULL,
	[subsection_name] [nvarchar](30) NULL,
	[series_name] [nvarchar](30) NULL,
	[subseries_name] [nvarchar](30) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_genus] PRIMARY KEY CLUSTERED 
(
	[taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_geography_map]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_geography_map](
	[taxonomy_geography_map_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[geography_id] [int] NULL,
	[geography_status_code] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_geography_map] PRIMARY KEY CLUSTERED 
(
	[taxonomy_geography_map_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_noxious]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_noxious](
	[taxonomy_noxious_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[geography_id] [int] NOT NULL,
	[noxious_type_code] [nvarchar](20) NOT NULL,
	[noxious_level_code] [nvarchar](20) NULL,
	[url] [nvarchar](300) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_noxious] PRIMARY KEY CLUSTERED 
(
	[taxonomy_noxious_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_species]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_species](
	[taxonomy_species_id] [int] IDENTITY(1,1) NOT NULL,
	[current_taxonomy_species_id] [int] NULL,
	[nomen_number] [int] NULL,
	[is_specific_hybrid] [nvarchar](1) NOT NULL,
	[species_name] [nvarchar](30) NOT NULL,
	[species_authority] [nvarchar](100) NULL,
	[is_subspecific_hybrid] [nvarchar](1) NOT NULL,
	[subspecies_name] [nvarchar](30) NULL,
	[subspecies_authority] [nvarchar](100) NULL,
	[is_varietal_hybrid] [nvarchar](1) NOT NULL,
	[variety_name] [nvarchar](30) NULL,
	[variety_authority] [nvarchar](100) NULL,
	[is_subvarietal_hybrid] [nvarchar](1) NOT NULL,
	[subvariety_name] [nvarchar](30) NULL,
	[subvariety_authority] [nvarchar](100) NULL,
	[is_forma_hybrid] [nvarchar](1) NOT NULL,
	[forma_rank_type] [nvarchar](30) NULL,
	[forma_name] [nvarchar](30) NULL,
	[forma_authority] [nvarchar](100) NULL,
	[taxonomy_genus_id] [int] NOT NULL,
	[priority1_site_id] [int] NULL,
	[priority2_site_id] [int] NULL,
	[curator1_cooperator_id] [int] NULL,
	[curator2_cooperator_id] [int] NULL,
	[restriction_code] [nvarchar](20) NULL,
	[life_form_code] [nvarchar](20) NULL,
	[common_fertilization_code] [nvarchar](20) NULL,
	[is_name_pending] [nvarchar](1) NOT NULL,
	[synonym_code] [nvarchar](20) NULL,
	[verifier_cooperator_id] [int] NULL,
	[name_verified_date] [datetime2](7) NULL,
	[name] [nvarchar](100) NOT NULL,
	[name_authority] [nvarchar](100) NULL,
	[protologue] [nvarchar](500) NULL,
	[protologue_virtual_path] [nvarchar](255) NULL,
	[note] [nvarchar](max) NULL,
	[site_note] [nvarchar](max) NULL,
	[alternate_name] [nvarchar](2000) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_species] PRIMARY KEY CLUSTERED 
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taxonomy_use]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taxonomy_use](
	[taxonomy_use_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[economic_usage_code] [nvarchar](20) NOT NULL,
	[usage_type] [nvarchar](100) NULL,
	[plant_part_code] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_use] PRIMARY KEY CLUSTERED 
(
	[taxonomy_use_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[w6_site_inventory]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w6_site_inventory](
	[w6_site_inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[seed_quantity] [int] NULL,
	[seed_age] [nvarchar](20) NULL,
	[hundred_weight] [decimal](18, 7) NULL,
	[increase_location] [nvarchar](20) NULL,
	[increase_year] [nvarchar](4) NULL,
	[lot_type_code] [nvarchar](20) NULL,
	[pollen_vector_code] [nvarchar](20) NULL,
	[pollen_environment] [nvarchar](20) NULL,
	[pollen_procedure_code] [nvarchar](20) NULL,
	[split_inventory_id] [int] NULL,
	[plot] [nvarchar](30) NULL,
	[note] [nvarchar](240) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_w6_site_inventory] PRIMARY KEY CLUSTERED 
(
	[w6_site_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_cooperator]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_cooperator](
	[web_cooperator_id] [int] IDENTITY(1,1) NOT NULL,
	[last_name] [nvarchar](100) NULL,
	[title] [nvarchar](10) NULL,
	[first_name] [nvarchar](100) NULL,
	[job] [nvarchar](100) NULL,
	[organization] [nvarchar](100) NULL,
	[organization_code] [nvarchar](10) NULL,
	[address_line1] [nvarchar](100) NULL,
	[address_line2] [nvarchar](100) NULL,
	[address_line3] [nvarchar](100) NULL,
	[city] [nvarchar](100) NULL,
	[postal_index] [nvarchar](100) NULL,
	[geography_id] [int] NULL,
	[primary_phone] [nvarchar](30) NULL,
	[secondary_phone] [nvarchar](30) NULL,
	[fax] [nvarchar](30) NULL,
	[email] [nvarchar](100) NULL,
	[is_active] [nvarchar](1) NOT NULL,
	[category_code] [nvarchar](4) NULL,
	[organization_region] [nvarchar](20) NULL,
	[discipline] [nvarchar](50) NULL,
	[initials] [nvarchar](10) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_cooperator] PRIMARY KEY CLUSTERED 
(
	[web_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_help]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_help](
	[web_help_id] [int] IDENTITY(1,1) NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[web_help_tag] [nvarchar](100) NULL,
	[virtual_path] [nvarchar](255) NULL,
	[heading] [nvarchar](50) NULL,
	[title] [nvarchar](50) NULL,
	[help_text_html] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_help] PRIMARY KEY CLUSTERED 
(
	[web_help_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_order_request]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_order_request](
	[web_order_request_id] [int] IDENTITY(1,1) NOT NULL,
	[web_cooperator_id] [int] NULL,
	[ordered_date] [datetime2](7) NULL,
	[intended_use_code] [nvarchar](20) NULL,
	[intended_use_note] [nvarchar](max) NULL,
	[status_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[special_instruction] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_order_request] PRIMARY KEY CLUSTERED 
(
	[web_order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_order_request_action]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_order_request_action](
	[web_order_request_action_id] [int] IDENTITY(1,1) NOT NULL,
	[web_order_request_id] [int] NOT NULL,
	[action_code] [nvarchar](20) NOT NULL,
	[acted_date] [datetime2](7) NOT NULL,
	[action_for_id] [nvarchar](max) NULL,
	[note] [nvarchar](max) NULL,
	[web_cooperator_id] [int] NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_order_request_action] PRIMARY KEY CLUSTERED 
(
	[web_order_request_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_order_request_address]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_order_request_address](
	[web_order_request_address_id] [int] IDENTITY(1,1) NOT NULL,
	[web_order_request_id] [int] NOT NULL,
	[address_line1] [nvarchar](100) NULL,
	[address_line2] [nvarchar](100) NULL,
	[address_line3] [nvarchar](100) NULL,
	[city] [nvarchar](100) NULL,
	[postal_index] [nvarchar](100) NULL,
	[geography_id] [int] NULL,
	[carrier] [nvarchar](20) NULL,
	[carrier_account] [nvarchar](50) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_order_request_address] PRIMARY KEY CLUSTERED 
(
	[web_order_request_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_order_request_attach]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_order_request_attach](
	[web_order_request_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[web_cooperator_id] [int] NOT NULL,
	[web_order_request_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[content_type] [nvarchar](100) NULL,
	[title] [nvarchar](500) NULL,
	[status] [nvarchar](50) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_order_request_attach] PRIMARY KEY CLUSTERED 
(
	[web_order_request_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_order_request_item]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_order_request_item](
	[web_order_request_item_id] [int] IDENTITY(1,1) NOT NULL,
	[web_cooperator_id] [int] NOT NULL,
	[web_order_request_id] [int] NOT NULL,
	[sequence_number] [int] NOT NULL,
	[accession_id] [int] NOT NULL,
	[name] [nvarchar](200) NULL,
	[quantity_shipped] [int] NULL,
	[unit_of_shipped_code] [nvarchar](20) NULL,
	[distribution_form_code] [nvarchar](20) NULL,
	[status_code] [nvarchar](20) NULL,
	[curator_note] [nvarchar](max) NULL,
	[user_note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_order_request_item] PRIMARY KEY CLUSTERED 
(
	[web_order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_user]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_user](
	[web_user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[is_enabled] [nvarchar](1) NOT NULL,
	[sys_lang_id] [int] NOT NULL,
	[last_login_date] [datetime2](7) NULL,
	[web_cooperator_id] [int] NULL,
	[created_date] [datetime2](7) NOT NULL,
	[modified_date] [datetime2](7) NULL,
 CONSTRAINT [PK_web_user] PRIMARY KEY CLUSTERED 
(
	[web_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_user_cart]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_user_cart](
	[web_user_cart_id] [int] IDENTITY(1,1) NOT NULL,
	[web_user_id] [int] NOT NULL,
	[cart_type_code] [nvarchar](20) NOT NULL,
	[expiration_date] [datetime2](7) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_user_cart] PRIMARY KEY CLUSTERED 
(
	[web_user_cart_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_user_cart_item]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_user_cart_item](
	[web_user_cart_item_id] [int] IDENTITY(1,1) NOT NULL,
	[web_user_cart_id] [int] NOT NULL,
	[accession_id] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[form_type_code] [nvarchar](20) NOT NULL,
	[usage_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_user_cart_item] PRIMARY KEY CLUSTERED 
(
	[web_user_cart_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_user_preference]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_user_preference](
	[web_user_preference_id] [int] IDENTITY(1,1) NOT NULL,
	[web_user_id] [int] NOT NULL,
	[preference_name] [nvarchar](100) NOT NULL,
	[preference_value] [nvarchar](100) NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_user_preference] PRIMARY KEY CLUSTERED 
(
	[web_user_preference_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[web_user_shipping_address]    Script Date: 9/9/2019 8:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[web_user_shipping_address](
	[web_user_shipping_address_id] [int] IDENTITY(1,1) NOT NULL,
	[web_user_id] [int] NOT NULL,
	[address_name] [nvarchar](50) NOT NULL,
	[address_line1] [nvarchar](100) NOT NULL,
	[address_line2] [nvarchar](100) NULL,
	[address_line3] [nvarchar](100) NULL,
	[city] [nvarchar](100) NOT NULL,
	[postal_index] [nvarchar](100) NOT NULL,
	[geography_id] [int] NOT NULL,
	[is_default] [nvarchar](1) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_web_user_shipping_address] PRIMARY KEY CLUSTERED 
(
	[web_user_shipping_address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_a_part1]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_a_part1] ON [dbo].[accession]
(
	[accession_number_part1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_a_part2]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_a_part2] ON [dbo].[accession]
(
	[accession_number_part2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_a_part3]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_a_part3] ON [dbo].[accession]
(
	[accession_number_part3] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_a_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_a_created] ON [dbo].[accession]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_a_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_a_modified] ON [dbo].[accession]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_a_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_a_owned] ON [dbo].[accession]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_a_t]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_a_t] ON [dbo].[accession]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ac]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ac] ON [dbo].[accession]
(
	[accession_number_part1] ASC,
	[accession_number_part2] ASC,
	[accession_number_part3] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_doi]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_doi] ON [dbo].[accession]
(
	[doi] ASC
)
WHERE ([doi] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aa_a]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aa_a] ON [dbo].[accession_action]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aa_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aa_c] ON [dbo].[accession_action]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aa_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aa_created] ON [dbo].[accession_action]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aa_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aa_m] ON [dbo].[accession_action]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aa_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aa_modified] ON [dbo].[accession_action]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aa_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aa_owned] ON [dbo].[accession_action]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_aa]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aa] ON [dbo].[accession_action]
(
	[accession_id] ASC,
	[action_name_code] ASC,
	[started_date] ASC,
	[completed_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_c] ON [dbo].[accession_inv_annotation]
(
	[annotation_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_created] ON [dbo].[accession_inv_annotation]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_i] ON [dbo].[accession_inv_annotation]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_modified] ON [dbo].[accession_inv_annotation]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_or]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_or] ON [dbo].[accession_inv_annotation]
(
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_owned] ON [dbo].[accession_inv_annotation]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_t_new]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_t_new] ON [dbo].[accession_inv_annotation]
(
	[new_taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aian_t_old]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aian_t_old] ON [dbo].[accession_inv_annotation]
(
	[old_taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_aian]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aian] ON [dbo].[accession_inv_annotation]
(
	[inventory_id] ASC,
	[annotation_type_code] ASC,
	[annotation_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiat_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiat_c] ON [dbo].[accession_inv_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiat_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiat_created] ON [dbo].[accession_inv_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiat_iid]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiat_iid] ON [dbo].[accession_inv_attach]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiat_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiat_modified] ON [dbo].[accession_inv_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiat_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiat_owned] ON [dbo].[accession_inv_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_aiat]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aiat] ON [dbo].[accession_inv_attach]
(
	[inventory_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aig_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aig_created] ON [dbo].[accession_inv_group]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aig_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aig_m] ON [dbo].[accession_inv_group]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aig_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aig_modified] ON [dbo].[accession_inv_group]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aig_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aig_owned] ON [dbo].[accession_inv_group]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_aig]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aig] ON [dbo].[accession_inv_group]
(
	[group_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigat_aig]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_aig] ON [dbo].[accession_inv_group_attach]
(
	[accession_inv_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigat_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_c] ON [dbo].[accession_inv_group_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigat_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_created] ON [dbo].[accession_inv_group_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigat_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_modified] ON [dbo].[accession_inv_group_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigat_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_owned] ON [dbo].[accession_inv_group_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_aigat]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aigat] ON [dbo].[accession_inv_group_attach]
(
	[accession_inv_group_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigm_ag]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigm_ag] ON [dbo].[accession_inv_group_map]
(
	[accession_inv_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigm_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigm_created] ON [dbo].[accession_inv_group_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigm_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigm_i] ON [dbo].[accession_inv_group_map]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigm_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigm_modified] ON [dbo].[accession_inv_group_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aigm_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigm_owned] ON [dbo].[accession_inv_group_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_aigm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aigm] ON [dbo].[accession_inv_group_map]
(
	[inventory_id] ASC,
	[accession_inv_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ain_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ain_c] ON [dbo].[accession_inv_name]
(
	[name_source_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ain_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ain_created] ON [dbo].[accession_inv_name]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ain_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ain_i] ON [dbo].[accession_inv_name]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ain_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ain_modified] ON [dbo].[accession_inv_name]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ain_ng]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ain_ng] ON [dbo].[accession_inv_name]
(
	[name_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ain_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ain_owned] ON [dbo].[accession_inv_name]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ain]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ain] ON [dbo].[accession_inv_name]
(
	[inventory_id] ASC,
	[plant_name] ASC,
	[name_group_id] ASC,
	[category_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiv_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiv_c] ON [dbo].[accession_inv_voucher]
(
	[collector_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiv_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiv_created] ON [dbo].[accession_inv_voucher]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiv_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiv_i] ON [dbo].[accession_inv_voucher]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiv_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiv_modified] ON [dbo].[accession_inv_voucher]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aiv_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aiv_owned] ON [dbo].[accession_inv_voucher]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_aiv]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aiv] ON [dbo].[accession_inv_voucher]
(
	[inventory_id] ASC,
	[voucher_location] ASC,
	[vouchered_date] ASC,
	[collector_voucher_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ar_a]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ar_a] ON [dbo].[accession_ipr]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ar_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ar_c] ON [dbo].[accession_ipr]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ar_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ar_created] ON [dbo].[accession_ipr]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ar_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ar_modified] ON [dbo].[accession_ipr]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ar_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ar_owned] ON [dbo].[accession_ipr]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_ipr_crop]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ipr_crop] ON [dbo].[accession_ipr]
(
	[ipr_crop_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_ipr_number]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ipr_number] ON [dbo].[accession_ipr]
(
	[ipr_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ipr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ipr] ON [dbo].[accession_ipr]
(
	[accession_id] ASC,
	[type_code] ASC,
	[ipr_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ap_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ap_created] ON [dbo].[accession_pedigree]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ap_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ap_modified] ON [dbo].[accession_pedigree]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ap_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ap_owned] ON [dbo].[accession_pedigree]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_pd]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_pd] ON [dbo].[accession_pedigree]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aq_a]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aq_a] ON [dbo].[accession_quarantine]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aq_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aq_c] ON [dbo].[accession_quarantine]
(
	[custodial_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aq_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aq_created] ON [dbo].[accession_quarantine]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aq_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aq_modified] ON [dbo].[accession_quarantine]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_aq_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aq_owned] ON [dbo].[accession_quarantine]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_qr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_qr] ON [dbo].[accession_quarantine]
(
	[accession_id] ASC,
	[quarantine_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_as_a]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_as_a] ON [dbo].[accession_source]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_as_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_as_created] ON [dbo].[accession_source]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_as_g]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_as_g] ON [dbo].[accession_source]
(
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_as_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_as_modified] ON [dbo].[accession_source]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_as_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_as_owned] ON [dbo].[accession_source]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sr] ON [dbo].[accession_source]
(
	[accession_id] ASC,
	[source_type_code] ASC,
	[source_date] ASC,
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_asm_as]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_asm_as] ON [dbo].[accession_source_map]
(
	[accession_source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_asm_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_asm_c] ON [dbo].[accession_source_map]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_asm_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_asm_created] ON [dbo].[accession_source_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_asm_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_asm_modified] ON [dbo].[accession_source_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_asm_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_asm_owned] ON [dbo].[accession_source_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sm] ON [dbo].[accession_source_map]
(
	[accession_source_id] ASC,
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_are_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_are_created] ON [dbo].[app_resource]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_are_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_are_modified] ON [dbo].[app_resource]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_are_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_are_owned] ON [dbo].[app_resource]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_are_sl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_are_sl] ON [dbo].[app_resource]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ar]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ar] ON [dbo].[app_resource]
(
	[sys_lang_id] ASC,
	[app_name] ASC,
	[form_name] ASC,
	[app_resource_name] ASC,
	[sort_order] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_as]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_as] ON [dbo].[app_setting]
(
	[category_tag] ASC,
	[sort_order] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sugs_co]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sugs_co] ON [dbo].[app_user_gui_setting]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sugs_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sugs_created] ON [dbo].[app_user_gui_setting]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sugs_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sugs_modified] ON [dbo].[app_user_gui_setting]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sugs_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sugs_owned] ON [dbo].[app_user_gui_setting]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sugs]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sugs] ON [dbo].[app_user_gui_setting]
(
	[cooperator_id] ASC,
	[app_name] ASC,
	[form_name] ASC,
	[resource_name] ASC,
	[resource_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_auil_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_auil_c] ON [dbo].[app_user_item_list]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_auil_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_auil_created] ON [dbo].[app_user_item_list]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_auil_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_auil_modified] ON [dbo].[app_user_item_list]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_auil_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_auil_owned] ON [dbo].[app_user_item_list]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uil_group]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_uil_group] ON [dbo].[app_user_item_list]
(
	[cooperator_id] ASC,
	[list_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uil_tab]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_uil_tab] ON [dbo].[app_user_item_list]
(
	[cooperator_id] ASC,
	[tab_name] ASC,
	[list_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_a]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_a] ON [dbo].[citation]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_aipr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_aipr] ON [dbo].[citation]
(
	[accession_ipr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_ap]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_ap] ON [dbo].[citation]
(
	[accession_pedigree_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_created] ON [dbo].[citation]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_gm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_gm] ON [dbo].[citation]
(
	[genetic_marker_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_l]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_l] ON [dbo].[citation]
(
	[literature_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_m] ON [dbo].[citation]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_modified] ON [dbo].[citation]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_owned] ON [dbo].[citation]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_tf]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_tf] ON [dbo].[citation]
(
	[taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_tg]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_tg] ON [dbo].[citation]
(
	[taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ci_ts]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ci_ts] ON [dbo].[citation]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_cit]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cit] ON [dbo].[citation]
(
	[literature_id] ASC,
	[citation_title] ASC,
	[accession_id] ASC,
	[method_id] ASC,
	[taxonomy_species_id] ASC,
	[taxonomy_genus_id] ASC,
	[taxonomy_family_id] ASC,
	[accession_ipr_id] ASC,
	[accession_pedigree_id] ASC,
	[genetic_marker_id] ASC,
	[type_code] ASC,
	[unique_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_fk_cdval_cdgrp]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cdval_cdgrp] ON [dbo].[code_value]
(
	[group_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cdval_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cdval_created] ON [dbo].[code_value]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cdval_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cdval_modified] ON [dbo].[code_value]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cdval_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cdval_owned] ON [dbo].[code_value]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_cv]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cv] ON [dbo].[code_value]
(
	[group_name] ASC,
	[value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cvl_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cvl_created] ON [dbo].[code_value_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cvl_cv]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cvl_cv] ON [dbo].[code_value_lang]
(
	[code_value_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cvl_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cvl_modified] ON [dbo].[code_value_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cvl_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cvl_owned] ON [dbo].[code_value_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cvl_sl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cvl_sl] ON [dbo].[code_value_lang]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_cvl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cvl] ON [dbo].[code_value_lang]
(
	[code_value_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_co_full_name]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_co_full_name] ON [dbo].[cooperator]
(
	[last_name] ASC,
	[first_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_co_org_code]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_co_org_code] ON [dbo].[cooperator]
(
	[organization_abbrev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c_created] ON [dbo].[cooperator]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c_cur_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c_cur_c] ON [dbo].[cooperator]
(
	[current_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c_modified] ON [dbo].[cooperator]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c_owned] ON [dbo].[cooperator]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c_sl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c_sl] ON [dbo].[cooperator]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c_wc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c_wc] ON [dbo].[cooperator]
(
	[web_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_co]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_co] ON [dbo].[cooperator]
(
	[last_name] ASC,
	[first_name] ASC,
	[organization] ASC,
	[geography_id] ASC,
	[address_line1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cg_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cg_created] ON [dbo].[cooperator_group]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cg_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cg_modified] ON [dbo].[cooperator_group]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cg_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cg_owned] ON [dbo].[cooperator_group]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_cg_name]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cg_name] ON [dbo].[cooperator_group]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cm_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cm_c] ON [dbo].[cooperator_map]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cm_cg]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cm_cg] ON [dbo].[cooperator_map]
(
	[cooperator_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cm_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cm_created] ON [dbo].[cooperator_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cm_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cm_modified] ON [dbo].[cooperator_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cm_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cm_owned] ON [dbo].[cooperator_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_cm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cm] ON [dbo].[cooperator_map]
(
	[cooperator_id] ASC,
	[cooperator_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cr_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cr_created] ON [dbo].[crop]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cr_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cr_modified] ON [dbo].[crop]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cr_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cr_owned] ON [dbo].[crop]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_crop]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_crop] ON [dbo].[crop]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_c] ON [dbo].[crop_attach]
(
	[crop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ca_co]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ca_co] ON [dbo].[crop_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_created] ON [dbo].[crop_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_modified] ON [dbo].[crop_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_owned] ON [dbo].[crop_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ca]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ca] ON [dbo].[crop_attach]
(
	[crop_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ct_cr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ct_cr] ON [dbo].[crop_trait]
(
	[crop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ct_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ct_created] ON [dbo].[crop_trait]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ct_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ct_modified] ON [dbo].[crop_trait]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ct_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ct_owned] ON [dbo].[crop_trait]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ct]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ct] ON [dbo].[crop_trait]
(
	[coded_name] ASC,
	[crop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cta_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cta_c] ON [dbo].[crop_trait_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cta_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cta_created] ON [dbo].[crop_trait_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cta_ct]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cta_ct] ON [dbo].[crop_trait_attach]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cta_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cta_modified] ON [dbo].[crop_trait_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cta_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cta_owned] ON [dbo].[crop_trait_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_cta]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cta] ON [dbo].[crop_trait_attach]
(
	[crop_trait_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tct_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tct_created] ON [dbo].[crop_trait_code]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tct_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tct_modified] ON [dbo].[crop_trait_code]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tct_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tct_owned] ON [dbo].[crop_trait_code]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tct_tr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tct_tr] ON [dbo].[crop_trait_code]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ctc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ctc] ON [dbo].[crop_trait_code]
(
	[crop_trait_id] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctca_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctca_c] ON [dbo].[crop_trait_code_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctca_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctca_created] ON [dbo].[crop_trait_code_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctca_ctc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctca_ctc] ON [dbo].[crop_trait_code_attach]
(
	[crop_trait_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctca_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctca_modified] ON [dbo].[crop_trait_code_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctca_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctca_owned] ON [dbo].[crop_trait_code_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ctca]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ctca] ON [dbo].[crop_trait_code_attach]
(
	[crop_trait_code_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctcl_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctcl_created] ON [dbo].[crop_trait_code_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctcl_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctcl_modified] ON [dbo].[crop_trait_code_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctcl_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctcl_owned] ON [dbo].[crop_trait_code_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctcl_sl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctcl_sl] ON [dbo].[crop_trait_code_lang]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctcl_tc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctcl_tc] ON [dbo].[crop_trait_code_lang]
(
	[crop_trait_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_ctcl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ctcl] ON [dbo].[crop_trait_code_lang]
(
	[crop_trait_code_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctl_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctl_created] ON [dbo].[crop_trait_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctl_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctl_modified] ON [dbo].[crop_trait_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctl_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctl_owned] ON [dbo].[crop_trait_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctl_sl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctl_sl] ON [dbo].[crop_trait_lang]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctl_t]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctl_t] ON [dbo].[crop_trait_lang]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_ctl]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ctl] ON [dbo].[crop_trait_lang]
(
	[crop_trait_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_created] ON [dbo].[crop_trait_observation]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_ct]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_ct] ON [dbo].[crop_trait_observation]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_ctc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_ctc] ON [dbo].[crop_trait_observation]
(
	[crop_trait_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_i] ON [dbo].[crop_trait_observation]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_m] ON [dbo].[crop_trait_observation]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_modified] ON [dbo].[crop_trait_observation]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_cto_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_cto_owned] ON [dbo].[crop_trait_observation]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_cto]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_cto] ON [dbo].[crop_trait_observation]
(
	[inventory_id] ASC,
	[crop_trait_id] ASC,
	[crop_trait_code_id] ASC,
	[numeric_value] ASC,
	[string_value] ASC,
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_created] ON [dbo].[crop_trait_observation_data]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_ct]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_ct] ON [dbo].[crop_trait_observation_data]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_ctc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_ctc] ON [dbo].[crop_trait_observation_data]
(
	[crop_trait_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_cto]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_cto] ON [dbo].[crop_trait_observation_data]
(
	[crop_trait_observation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_i] ON [dbo].[crop_trait_observation_data]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_m] ON [dbo].[crop_trait_observation_data]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_modified] ON [dbo].[crop_trait_observation_data]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ctod_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ctod_owned] ON [dbo].[crop_trait_observation_data]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ctod]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ctod] ON [dbo].[crop_trait_observation_data]
(
	[crop_trait_observation_id] ASC,
	[inventory_id] ASC,
	[crop_trait_id] ASC,
	[crop_trait_code_id] ASC,
	[numeric_value] ASC,
	[string_value] ASC,
	[method_id] ASC,
	[individual] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_e_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_e_created] ON [dbo].[email]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_e_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_e_modified] ON [dbo].[email]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_e_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_e_owned] ON [dbo].[email]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ea_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ea_created] ON [dbo].[email_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ea_e]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ea_e] ON [dbo].[email_attach]
(
	[email_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ea_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ea_modified] ON [dbo].[email_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ea_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ea_owned] ON [dbo].[email_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ea]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ea] ON [dbo].[email_attach]
(
	[email_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ex_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ex_c] ON [dbo].[exploration]
(
	[host_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ex_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ex_created] ON [dbo].[exploration]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ex_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ex_modified] ON [dbo].[exploration]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ex_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ex_owned] ON [dbo].[exploration]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ex]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ex] ON [dbo].[exploration]
(
	[exploration_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_exm_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_exm_c] ON [dbo].[exploration_map]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_exm_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_exm_created] ON [dbo].[exploration_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_exm_ex]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_exm_ex] ON [dbo].[exploration_map]
(
	[exploration_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_exm_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_exm_modified] ON [dbo].[exploration_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_exm_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_exm_owned] ON [dbo].[exploration_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_exm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_exm] ON [dbo].[exploration_map]
(
	[exploration_id] ASC,
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_f_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_f_created] ON [dbo].[feedback]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_f_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_f_modified] ON [dbo].[feedback]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_f_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_f_owned] ON [dbo].[feedback]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_f]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_f] ON [dbo].[feedback]
(
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ff_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ff_created] ON [dbo].[feedback_form]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ff_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ff_modified] ON [dbo].[feedback_form]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ff_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ff_owned] ON [dbo].[feedback_form]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ff]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ff] ON [dbo].[feedback_form]
(
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fff_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fff_created] ON [dbo].[feedback_form_field]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fff_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fff_modified] ON [dbo].[feedback_form_field]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fff_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fff_owned] ON [dbo].[feedback_form_field]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_fff]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fff] ON [dbo].[feedback_form_field]
(
	[feedback_form_id] ASC,
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fft_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fft_created] ON [dbo].[feedback_form_trait]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fft_ct]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fft_ct] ON [dbo].[feedback_form_trait]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fft_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fft_modified] ON [dbo].[feedback_form_trait]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fft_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fft_owned] ON [dbo].[feedback_form_trait]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_fft]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fft] ON [dbo].[feedback_form_trait]
(
	[feedback_form_id] ASC,
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fi_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fi_created] ON [dbo].[feedback_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fi_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fi_modified] ON [dbo].[feedback_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fi_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fi_owned] ON [dbo].[feedback_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_fi]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fi] ON [dbo].[feedback_inventory]
(
	[feedback_id] ASC,
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fr_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fr_created] ON [dbo].[feedback_report]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fr_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fr_modified] ON [dbo].[feedback_report]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fr_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fr_owned] ON [dbo].[feedback_report]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_fr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fr] ON [dbo].[feedback_report]
(
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fa_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fa_created] ON [dbo].[feedback_report_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fa_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fa_modified] ON [dbo].[feedback_report_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fa_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fa_owned] ON [dbo].[feedback_report_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_frepa]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_frepa] ON [dbo].[feedback_report_attach]
(
	[feedback_report_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_fa]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fa] ON [dbo].[feedback_report_attach]
(
	[feedback_report_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndf_fk_fres_fresg]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndf_fk_fres_fresg] ON [dbo].[feedback_result]
(
	[feedback_result_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fres_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fres_created] ON [dbo].[feedback_result]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fres_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fres_i] ON [dbo].[feedback_result]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fres_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fres_modified] ON [dbo].[feedback_result]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fres_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fres_owned] ON [dbo].[feedback_result]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresa]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresa] ON [dbo].[feedback_result_attach]
(
	[feedback_result_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresa_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresa_created] ON [dbo].[feedback_result_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresa_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresa_modified] ON [dbo].[feedback_result_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresa_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresa_owned] ON [dbo].[feedback_result_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_fresa]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fresa] ON [dbo].[feedback_result_attach]
(
	[feedback_result_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_fresf_fff]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [fk_fresf_fff] ON [dbo].[feedback_result_field]
(
	[feedback_form_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresf_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresf_created] ON [dbo].[feedback_result_field]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresf_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresf_modified] ON [dbo].[feedback_result_field]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresf_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresf_owned] ON [dbo].[feedback_result_field]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_fresf]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fresf] ON [dbo].[feedback_result_field]
(
	[feedback_result_id] ASC,
	[feedback_form_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_frg_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_frg_created] ON [dbo].[feedback_result_group]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_frg_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_frg_modified] ON [dbo].[feedback_result_group]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_frg_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_frg_owned] ON [dbo].[feedback_result_group]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_frg_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_frg_c] ON [dbo].[feedback_result_group]
(
	[participant_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_fres]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fres] ON [dbo].[feedback_result_group]
(
	[feedback_report_id] ASC,
	[participant_cooperator_id] ASC,
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_created] ON [dbo].[feedback_result_trait_obs]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_ct]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_ct] ON [dbo].[feedback_result_trait_obs]
(
	[crop_trait_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_ctc]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_ctc] ON [dbo].[feedback_result_trait_obs]
(
	[crop_trait_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_i] ON [dbo].[feedback_result_trait_obs]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_m] ON [dbo].[feedback_result_trait_obs]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_modified] ON [dbo].[feedback_result_trait_obs]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_fresto_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_fresto_owned] ON [dbo].[feedback_result_trait_obs]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_fresto]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_fresto] ON [dbo].[feedback_result_trait_obs]
(
	[feedback_result_id] ASC,
	[inventory_id] ASC,
	[crop_trait_id] ASC,
	[crop_trait_code_id] ASC,
	[numeric_value] ASC,
	[string_value] ASC,
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ga_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ga_created] ON [dbo].[genetic_annotation]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ga_gm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ga_gm] ON [dbo].[genetic_annotation]
(
	[genetic_marker_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ga_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ga_m] ON [dbo].[genetic_annotation]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ga_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ga_modified] ON [dbo].[genetic_annotation]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ga_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ga_owned] ON [dbo].[genetic_annotation]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_ga]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ga] ON [dbo].[genetic_annotation]
(
	[genetic_marker_id] ASC,
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gm_cr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gm_cr] ON [dbo].[genetic_marker]
(
	[crop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gm_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gm_created] ON [dbo].[genetic_marker]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gm_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gm_modified] ON [dbo].[genetic_marker]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gm_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gm_owned] ON [dbo].[genetic_marker]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_gm_crop]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_gm_crop] ON [dbo].[genetic_marker]
(
	[crop_id] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_go_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_go_created] ON [dbo].[genetic_observation]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_go_ga]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_go_ga] ON [dbo].[genetic_observation]
(
	[genetic_annotation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_go_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_go_modified] ON [dbo].[genetic_observation]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_go_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_go_owned] ON [dbo].[genetic_observation]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_go]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_go] ON [dbo].[genetic_observation]
(
	[inventory_id] ASC,
	[genetic_annotation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_god_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_god_created] ON [dbo].[genetic_observation_data]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_god_ga]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_god_ga] ON [dbo].[genetic_observation_data]
(
	[genetic_annotation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_god_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_god_i] ON [dbo].[genetic_observation_data]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_god_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_god_modified] ON [dbo].[genetic_observation_data]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_god_ob]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_god_ob] ON [dbo].[genetic_observation_data]
(
	[genetic_observation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_god_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_god_owned] ON [dbo].[genetic_observation_data]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_god]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_god] ON [dbo].[genetic_observation_data]
(
	[genetic_annotation_id] ASC,
	[inventory_id] ASC,
	[individual] ASC,
	[individual_allele_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gsi_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gsi_created] ON [dbo].[geneva_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gsi_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_gsi_i] ON [dbo].[geneva_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gsi_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gsi_modified] ON [dbo].[geneva_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gsi_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gsi_owned] ON [dbo].[geneva_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_g_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_g_created] ON [dbo].[geography]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_g_cur_g]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_g_cur_g] ON [dbo].[geography]
(
	[current_geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_g_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_g_modified] ON [dbo].[geography]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_g_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_g_owned] ON [dbo].[geography]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_g_adm1]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_g_adm1] ON [dbo].[geography]
(
	[adm1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_g_adm2]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_g_adm2] ON [dbo].[geography]
(
	[adm2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_g_adm3]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_g_adm3] ON [dbo].[geography]
(
	[adm3] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_g_adm4]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_g_adm4] ON [dbo].[geography]
(
	[adm4] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_g_country_code]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_g_country_code] ON [dbo].[geography]
(
	[country_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_geo]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_geo] ON [dbo].[geography]
(
	[country_code] ASC,
	[adm1] ASC,
	[adm1_type_code] ASC,
	[adm2] ASC,
	[adm3] ASC,
	[adm4] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_grm_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_grm_created] ON [dbo].[geography_region_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_grm_g]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_grm_g] ON [dbo].[geography_region_map]
(
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_grm_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_grm_modified] ON [dbo].[geography_region_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_grm_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_grm_owned] ON [dbo].[geography_region_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_grm_r]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_grm_r] ON [dbo].[geography_region_map]
(
	[region_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_grm]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_grm] ON [dbo].[geography_region_map]
(
	[geography_id] ASC,
	[region_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gspiivsi_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gspiivsi_i] ON [dbo].[gspi_site_inventory]
(
	[split_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gspisi_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gspisi_created] ON [dbo].[gspi_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gspisi_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_gspisi_i] ON [dbo].[gspi_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gspisi_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gspisi_modified] ON [dbo].[gspi_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_gspisi_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_gspisi_owned] ON [dbo].[gspi_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_a]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_a] ON [dbo].[inventory]
(
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_backup_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_backup_i] ON [dbo].[inventory]
(
	[backup_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_created] ON [dbo].[inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_im]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_im] ON [dbo].[inventory]
(
	[inventory_maint_policy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_m1]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_m1] ON [dbo].[inventory]
(
	[preservation_method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_m2]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_m2] ON [dbo].[inventory]
(
	[regeneration_method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_modified] ON [dbo].[inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_owned] ON [dbo].[inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_i_parent_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_i_parent_i] ON [dbo].[inventory]
(
	[parent_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_inv_number]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_inv_number] ON [dbo].[inventory]
(
	[inventory_number_part2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_inv_part3]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_inv_part3] ON [dbo].[inventory]
(
	[inventory_number_part3] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_inv_prefix]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_inv_prefix] ON [dbo].[inventory]
(
	[inventory_number_part1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_inv]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_inv] ON [dbo].[inventory]
(
	[inventory_number_part1] ASC,
	[inventory_number_part2] ASC,
	[inventory_number_part3] ASC,
	[form_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ia_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ia_c] ON [dbo].[inventory_action]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ia_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ia_created] ON [dbo].[inventory_action]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ia_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ia_i] ON [dbo].[inventory_action]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ia_m]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ia_m] ON [dbo].[inventory_action]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ia_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ia_modified] ON [dbo].[inventory_action]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ia_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ia_owned] ON [dbo].[inventory_action]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ia]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ia] ON [dbo].[inventory_action]
(
	[inventory_id] ASC,
	[action_name_code] ASC,
	[started_date] ASC,
	[completed_date] ASC,
	[form_code] ASC,
	[quantity] ASC,
	[method_id] ASC,
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_im_co]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_im_co] ON [dbo].[inventory_maint_policy]
(
	[curator_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_im_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_im_created] ON [dbo].[inventory_maint_policy]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_im_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_im_modified] ON [dbo].[inventory_maint_policy]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_im_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_im_owned] ON [dbo].[inventory_maint_policy]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_im]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_im] ON [dbo].[inventory_maint_policy]
(
	[maintenance_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iqs_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iqs_created] ON [dbo].[inventory_quality_status]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iqs_cur]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iqs_cur] ON [dbo].[inventory_quality_status]
(
	[tester_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iqs_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iqs_i] ON [dbo].[inventory_quality_status]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iqs_me]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iqs_me] ON [dbo].[inventory_quality_status]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iqs_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iqs_modified] ON [dbo].[inventory_quality_status]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iqs_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iqs_owned] ON [dbo].[inventory_quality_status]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_iqs_test]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_iqs_test] ON [dbo].[inventory_quality_status]
(
	[test_type_code] ASC,
	[contaminant_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_iqs]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_iqs] ON [dbo].[inventory_quality_status]
(
	[inventory_id] ASC,
	[test_type_code] ASC,
	[contaminant_code] ASC,
	[started_date] ASC,
	[plant_part_tested_code] ASC,
	[test_results_score_type_code] ASC,
	[replicate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iv_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iv_created] ON [dbo].[inventory_viability]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iv_i]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iv_i] ON [dbo].[inventory_viability]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iv_ivr]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iv_ivr] ON [dbo].[inventory_viability]
(
	[inventory_viability_rule_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iv_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iv_modified] ON [dbo].[inventory_viability]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_iv_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_iv_owned] ON [dbo].[inventory_viability]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_iv]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_iv] ON [dbo].[inventory_viability]
(
	[inventory_id] ASC,
	[inventory_viability_rule_id] ASC,
	[tested_date] ASC,
	[tested_date_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivd_c]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_c] ON [dbo].[inventory_viability_data]
(
	[counter_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivd_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_created] ON [dbo].[inventory_viability_data]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivd_iv]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_iv] ON [dbo].[inventory_viability_data]
(
	[inventory_viability_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivd_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_modified] ON [dbo].[inventory_viability_data]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivd_ori]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_ori] ON [dbo].[inventory_viability_data]
(
	[order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivd_owned]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivd_owned] ON [dbo].[inventory_viability_data]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_ivd]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ivd] ON [dbo].[inventory_viability_data]
(
	[inventory_viability_id] ASC,
	[replication_number] ASC,
	[count_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivr_created]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivr_created] ON [dbo].[inventory_viability_rule]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivr_modified]    Script Date: 9/9/2019 8:46:19 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivr_modified] ON [dbo].[inventory_viability_rule]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivr_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivr_owned] ON [dbo].[inventory_viability_rule]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ivr]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ivr] ON [dbo].[inventory_viability_rule]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivrm_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivrm_created] ON [dbo].[inventory_viability_rule_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivrm_ivr]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivrm_ivr] ON [dbo].[inventory_viability_rule_map]
(
	[inventory_viability_rule_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivrm_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivrm_modified] ON [dbo].[inventory_viability_rule_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivrm_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivrm_owned] ON [dbo].[inventory_viability_rule_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ivrm_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ivrm_ts] ON [dbo].[inventory_viability_rule_map]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_ivrm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ivrm] ON [dbo].[inventory_viability_rule_map]
(
	[taxonomy_species_id] ASC,
	[inventory_viability_rule_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_l_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_l_created] ON [dbo].[literature]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_l_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_l_modified] ON [dbo].[literature]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_l_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_l_owned] ON [dbo].[literature]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_l]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_l] ON [dbo].[literature]
(
	[abbreviation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_m_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_m_created] ON [dbo].[method]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_m_g]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_m_g] ON [dbo].[method]
(
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_m_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_m_modified] ON [dbo].[method]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_m_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_m_owned] ON [dbo].[method]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_m]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_m] ON [dbo].[method]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mat_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_c] ON [dbo].[method_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mat_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_created] ON [dbo].[method_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mat_m]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_m] ON [dbo].[method_attach]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mat_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_modified] ON [dbo].[method_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mat_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_owned] ON [dbo].[method_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_mat]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_mat] ON [dbo].[method_attach]
(
	[method_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mm_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mm_c] ON [dbo].[method_map]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mm_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mm_created] ON [dbo].[method_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mm_m]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mm_m] ON [dbo].[method_map]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mm_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mm_modified] ON [dbo].[method_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_mm_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mm_owned] ON [dbo].[method_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_mm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_mm] ON [dbo].[method_map]
(
	[cooperator_id] ASC,
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ng_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ng_created] ON [dbo].[name_group]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ng_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ng_modified] ON [dbo].[name_group]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ng_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ng_owned] ON [dbo].[name_group]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ng]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ng] ON [dbo].[name_group]
(
	[group_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nc7si_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_nc7si_created] ON [dbo].[nc7_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nc7si_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_nc7si_i] ON [dbo].[nc7_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nc7si_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_nc7si_modified] ON [dbo].[nc7_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nc7si_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_nc7si_owned] ON [dbo].[nc7_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ne9si_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ne9si_created] ON [dbo].[ne9_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ne9si_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_ne9si_i] ON [dbo].[ne9_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ne9si_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ne9si_modified] ON [dbo].[ne9_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ne9si_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ne9si_owned] ON [dbo].[ne9_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nsslsi_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_nsslsi_created] ON [dbo].[nssl_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nsslsi_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_nsslsi_i] ON [dbo].[nssl_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nsslsi_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_nsslsi_modified] ON [dbo].[nssl_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_nsslsi_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_nsslsi_owned] ON [dbo].[nssl_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_opgcsi_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_opgcsi_created] ON [dbo].[opgc_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_opgcsi_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_opgcsi_i] ON [dbo].[opgc_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_opgcsi_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_opgcsi_modified] ON [dbo].[opgc_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_opgcsi_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_opgcsi_owned] ON [dbo].[opgc_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_created] ON [dbo].[order_request]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_final_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_final_c] ON [dbo].[order_request]
(
	[final_recipient_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_modified] ON [dbo].[order_request]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_original_or]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_original_or] ON [dbo].[order_request]
(
	[original_order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_owned] ON [dbo].[order_request]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_requestor_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_requestor_c] ON [dbo].[order_request]
(
	[requestor_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_or_ship_to_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_or_ship_to_c] ON [dbo].[order_request]
(
	[ship_to_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oreq_f]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oreq_f] ON [dbo].[order_request]
(
	[feedback_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_or_obtained]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_or_obtained] ON [dbo].[order_request]
(
	[order_obtained_via] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ora_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ora_c] ON [dbo].[order_request_action]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ora_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ora_created] ON [dbo].[order_request_action]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ora_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ora_modified] ON [dbo].[order_request_action]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ora_or]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ora_or] ON [dbo].[order_request_action]
(
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ora_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ora_owned] ON [dbo].[order_request_action]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ora]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ora] ON [dbo].[order_request_action]
(
	[order_request_id] ASC,
	[action_name_code] ASC,
	[started_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oat_or]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oat_or] ON [dbo].[order_request_attach]
(
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orat_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orat_c] ON [dbo].[order_request_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orat_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orat_created] ON [dbo].[order_request_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orat_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orat_modified] ON [dbo].[order_request_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orat_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orat_owned] ON [dbo].[order_request_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_orat]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_orat] ON [dbo].[order_request_attach]
(
	[order_request_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_created] ON [dbo].[order_request_item]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_i] ON [dbo].[order_request_item]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_modified] ON [dbo].[order_request_item]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_or]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_or] ON [dbo].[order_request_item]
(
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_owned] ON [dbo].[order_request_item]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_sc]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_sc] ON [dbo].[order_request_item]
(
	[source_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ori_wori]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ori_wori] ON [dbo].[order_request_item]
(
	[web_order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_ori_item]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ori_item] ON [dbo].[order_request_item]
(
	[order_request_id] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oria_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_c] ON [dbo].[order_request_item_action]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oria_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_created] ON [dbo].[order_request_item_action]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oria_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_modified] ON [dbo].[order_request_item_action]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oria_ori]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_ori] ON [dbo].[order_request_item_action]
(
	[order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_oria_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_owned] ON [dbo].[order_request_item_action]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_oria]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_oria] ON [dbo].[order_request_item_action]
(
	[order_request_item_id] ASC,
	[action_name_code] ASC,
	[started_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orpl_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orpl_created] ON [dbo].[order_request_phyto_log]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orpl_inspected_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orpl_inspected_c] ON [dbo].[order_request_phyto_log]
(
	[inspected_by_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orpl_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orpl_modified] ON [dbo].[order_request_phyto_log]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orpl_or]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orpl_or] ON [dbo].[order_request_phyto_log]
(
	[order_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_orpl_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_orpl_owned] ON [dbo].[order_request_phyto_log]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_orpl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_orpl] ON [dbo].[order_request_phyto_log]
(
	[order_request_id] ASC,
	[phyto_certificate_type_code] ASC,
	[phyto_certificate_identifier] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_parlsi_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_parlsi_created] ON [dbo].[parl_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_parlsi_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_parlsi_i] ON [dbo].[parl_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_parlsi_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_parlsi_modified] ON [dbo].[parl_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_parlsi_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_parlsi_owned] ON [dbo].[parl_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_r_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_r_created] ON [dbo].[region]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_r_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_r_modified] ON [dbo].[region]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_r_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_r_owned] ON [dbo].[region]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_re]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_re] ON [dbo].[region]
(
	[continent] ASC,
	[subcontinent] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s9si_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_s9si_created] ON [dbo].[s9_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s9si_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_s9si_i] ON [dbo].[s9_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s9si_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_s9si_modified] ON [dbo].[s9_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s9si_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_s9si_owned] ON [dbo].[s9_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_s_created] ON [dbo].[site]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_s_modified] ON [dbo].[site]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_s_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_s_owned] ON [dbo].[site]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_s]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_s] ON [dbo].[site]
(
	[site_short_name] ASC,
	[site_long_name] ASC,
	[organization_abbrev] ASC,
	[fao_institute_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodo_as]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodo_as] ON [dbo].[source_desc_observation]
(
	[accession_source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodo_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodo_created] ON [dbo].[source_desc_observation]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodo_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodo_modified] ON [dbo].[source_desc_observation]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodo_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodo_owned] ON [dbo].[source_desc_observation]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodo_sd]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodo_sd] ON [dbo].[source_desc_observation]
(
	[source_descriptor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodo_sodc]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodo_sodc] ON [dbo].[source_desc_observation]
(
	[source_descriptor_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sodo]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sodo] ON [dbo].[source_desc_observation]
(
	[accession_source_id] ASC,
	[source_descriptor_id] ASC,
	[source_descriptor_code_id] ASC,
	[numeric_value] ASC,
	[string_value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sd_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sd_created] ON [dbo].[source_descriptor]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sd_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sd_modified] ON [dbo].[source_descriptor]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sd_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sd_owned] ON [dbo].[source_descriptor]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sd]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sd] ON [dbo].[source_descriptor]
(
	[coded_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodc_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodc_created] ON [dbo].[source_descriptor_code]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodc_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodc_modified] ON [dbo].[source_descriptor_code]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodc_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodc_owned] ON [dbo].[source_descriptor_code]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodc_sd]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodc_sd] ON [dbo].[source_descriptor_code]
(
	[source_descriptor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sodc]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sodc] ON [dbo].[source_descriptor_code]
(
	[source_descriptor_id] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodcl_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodcl_created] ON [dbo].[source_descriptor_code_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodcl_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodcl_modified] ON [dbo].[source_descriptor_code_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodcl_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodcl_owned] ON [dbo].[source_descriptor_code_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodcl_sl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodcl_sl] ON [dbo].[source_descriptor_code_lang]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodcl_sodc]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodcl_sodc] ON [dbo].[source_descriptor_code_lang]
(
	[source_descriptor_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sodcl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sodcl] ON [dbo].[source_descriptor_code_lang]
(
	[source_descriptor_code_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodl_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodl_created] ON [dbo].[source_descriptor_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodl_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodl_modified] ON [dbo].[source_descriptor_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodl_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodl_owned] ON [dbo].[source_descriptor_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodl_sd]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodl_sd] ON [dbo].[source_descriptor_lang]
(
	[source_descriptor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sodl_sl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sodl_sl] ON [dbo].[source_descriptor_lang]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sodl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sodl] ON [dbo].[source_descriptor_lang]
(
	[source_descriptor_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sdb]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdb] ON [dbo].[sys_database]
(
	[migration_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sdbm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdbm] ON [dbo].[sys_database_migration]
(
	[migration_number] ASC,
	[sort_order] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sdbml]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdbml] ON [dbo].[sys_database_migration_lang]
(
	[sys_database_migration_id] ASC,
	[language_iso_639_3_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sdt]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdt] ON [dbo].[sys_datatrigger]
(
	[sys_dataview_id] ASC,
	[sys_table_id] ASC,
	[fully_qualified_class_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sdtl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdtl] ON [dbo].[sys_datatrigger_lang]
(
	[sys_datatrigger_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sr_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sr_created] ON [dbo].[sys_dataview]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sr_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sr_modified] ON [dbo].[sys_dataview]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sr_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sr_owned] ON [dbo].[sys_dataview]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_dataview]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_dataview] ON [dbo].[sys_dataview]
(
	[dataview_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srf_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srf_created] ON [dbo].[sys_dataview_field]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srf_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srf_modified] ON [dbo].[sys_dataview_field]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srf_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srf_owned] ON [dbo].[sys_dataview_field]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srf_sr]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srf_sr] ON [dbo].[sys_dataview_field]
(
	[sys_dataview_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srf_stf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srf_stf] ON [dbo].[sys_dataview_field]
(
	[sys_table_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sdf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdf] ON [dbo].[sys_dataview_field]
(
	[sys_dataview_id] ASC,
	[field_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srfl_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srfl_created] ON [dbo].[sys_dataview_field_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srfl_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srfl_modified] ON [dbo].[sys_dataview_field_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srfl_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srfl_owned] ON [dbo].[sys_dataview_field_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srfl_sl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srfl_sl] ON [dbo].[sys_dataview_field_lang]
(
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srfl_srf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srfl_srf] ON [dbo].[sys_dataview_field_lang]
(
	[sys_dataview_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sdfl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdfl] ON [dbo].[sys_dataview_field_lang]
(
	[sys_dataview_field_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sdl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdl] ON [dbo].[sys_dataview_lang]
(
	[sys_dataview_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srp_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srp_created] ON [dbo].[sys_dataview_param]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srp_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srp_modified] ON [dbo].[sys_dataview_param]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srp_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srp_owned] ON [dbo].[sys_dataview_param]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_srp_sr]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_srp_sr] ON [dbo].[sys_dataview_param]
(
	[sys_dataview_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sdp]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sdp] ON [dbo].[sys_dataview_param]
(
	[sys_dataview_id] ASC,
	[param_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sds]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sds] ON [dbo].[sys_dataview_sql]
(
	[sys_dataview_id] ASC,
	[database_engine_tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sf] ON [dbo].[sys_file]
(
	[virtual_file_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sfg]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sfg] ON [dbo].[sys_file_group]
(
	[group_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_sfgm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_sfgm] ON [dbo].[sys_file_group_map]
(
	[sys_file_group_id] ASC,
	[sys_file_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sfl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sfl] ON [dbo].[sys_file_lang]
(
	[sys_file_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sgl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sgl] ON [dbo].[sys_group_lang]
(
	[sys_group_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sgpm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sgpm] ON [dbo].[sys_group_permission_map]
(
	[sys_group_id] ASC,
	[sys_permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sgum]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sgum] ON [dbo].[sys_group_user_map]
(
	[sys_group_id] ASC,
	[sys_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_si_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_si_created] ON [dbo].[sys_index]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_si_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_si_modified] ON [dbo].[sys_index]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_si_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_si_owned] ON [dbo].[sys_index]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_si_st]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_si_st] ON [dbo].[sys_index]
(
	[sys_table_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_si]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_si] ON [dbo].[sys_index]
(
	[index_name] ASC,
	[sys_table_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sif_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sif_created] ON [dbo].[sys_index_field]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sif_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sif_modified] ON [dbo].[sys_index_field]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sif_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sif_owned] ON [dbo].[sys_index_field]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sif_si]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sif_si] ON [dbo].[sys_index_field]
(
	[sys_index_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sif_stf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sif_stf] ON [dbo].[sys_index_field]
(
	[sys_table_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sif]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sif] ON [dbo].[sys_index_field]
(
	[sys_index_id] ASC,
	[sys_index_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sl_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sl_created] ON [dbo].[sys_lang]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sl_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sl_modified] ON [dbo].[sys_lang]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sl_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sl_owned] ON [dbo].[sys_lang]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_sl_tag]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sl_tag] ON [dbo].[sys_lang]
(
	[ietf_tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sp_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sp_created] ON [dbo].[sys_permission]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sp_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sp_modified] ON [dbo].[sys_permission]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sp_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sp_owned] ON [dbo].[sys_permission]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_spf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_spf] ON [dbo].[sys_permission_field]
(
	[sys_permission_id] ASC,
	[sys_dataview_field_id] ASC,
	[sys_table_field_id] ASC,
	[compare_mode] ASC,
	[compare_operator] ASC,
	[parent_compare_operator] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_spl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_spl] ON [dbo].[sys_permission_lang]
(
	[sys_permission_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_st_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_st_created] ON [dbo].[sys_table]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_st_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_st_modified] ON [dbo].[sys_table]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_st_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_st_owned] ON [dbo].[sys_table]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_st]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_st] ON [dbo].[sys_table]
(
	[table_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_fk_stf_cdgrp]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_stf_cdgrp] ON [dbo].[sys_table_field]
(
	[group_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_stf_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_stf_created] ON [dbo].[sys_table_field]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_stf_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_stf_modified] ON [dbo].[sys_table_field]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_stf_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_stf_owned] ON [dbo].[sys_table_field]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_stf_st]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_stf_st] ON [dbo].[sys_table_field]
(
	[sys_table_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_stf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_stf] ON [dbo].[sys_table_field]
(
	[sys_table_id] ASC,
	[field_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndk_uniq_stfl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndk_uniq_stfl] ON [dbo].[sys_table_field_lang]
(
	[sys_table_field_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_stl]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_stl] ON [dbo].[sys_table_lang]
(
	[sys_table_id] ASC,
	[sys_lang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_str]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_str] ON [dbo].[sys_table_relationship]
(
	[sys_table_field_id] ASC,
	[relationship_type_tag] ASC,
	[other_table_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_su_co]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_su_co] ON [dbo].[sys_user]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_su_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_su_created] ON [dbo].[sys_user]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_su_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_su_modified] ON [dbo].[sys_user]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_su_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_su_owned] ON [dbo].[sys_user]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_su_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_su_name] ON [dbo].[sys_user]
(
	[user_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sup_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sup_created] ON [dbo].[sys_user_permission_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sup_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sup_modified] ON [dbo].[sys_user_permission_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sup_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sup_owned] ON [dbo].[sys_user_permission_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sup_sp]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sup_sp] ON [dbo].[sys_user_permission_map]
(
	[sys_permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_sup_su]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_sup_su] ON [dbo].[sys_user_permission_map]
(
	[sys_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_sup]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_sup] ON [dbo].[sys_user_permission_map]
(
	[sys_permission_id] ASC,
	[sys_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tafm_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tafm_created] ON [dbo].[taxonomy_alt_family_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tafm_f]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tafm_f] ON [dbo].[taxonomy_alt_family_map]
(
	[taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tafm_g]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tafm_g] ON [dbo].[taxonomy_alt_family_map]
(
	[taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tafm_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tafm_modified] ON [dbo].[taxonomy_alt_family_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tafm_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tafm_owned] ON [dbo].[taxonomy_alt_family_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_tafm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tafm] ON [dbo].[taxonomy_alt_family_map]
(
	[taxonomy_genus_id] ASC,
	[taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tat_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tat_created] ON [dbo].[taxonomy_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tat_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tat_modified] ON [dbo].[taxonomy_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tat_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tat_owned] ON [dbo].[taxonomy_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tat_tf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tat_tf] ON [dbo].[taxonomy_attach]
(
	[taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tat_tg]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tat_tg] ON [dbo].[taxonomy_attach]
(
	[taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tat_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tat_ts] ON [dbo].[taxonomy_attach]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tat]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tat] ON [dbo].[taxonomy_attach]
(
	[taxonomy_family_id] ASC,
	[taxonomy_genus_id] ASC,
	[taxonomy_species_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ta_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ta_created] ON [dbo].[taxonomy_author]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ta_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ta_modified] ON [dbo].[taxonomy_author]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ta_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ta_owned] ON [dbo].[taxonomy_author]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_ta_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ta_name] ON [dbo].[taxonomy_author]
(
	[short_name_expanded_diacritic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ta]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ta] ON [dbo].[taxonomy_author]
(
	[short_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_cn_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_cn_name] ON [dbo].[taxonomy_common_name]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_cn_simplified_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_cn_simplified_name] ON [dbo].[taxonomy_common_name]
(
	[simplified_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcn_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcn_created] ON [dbo].[taxonomy_common_name]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcn_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcn_modified] ON [dbo].[taxonomy_common_name]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcn_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcn_owned] ON [dbo].[taxonomy_common_name]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcn_tg]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcn_tg] ON [dbo].[taxonomy_common_name]
(
	[taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcn_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcn_ts] ON [dbo].[taxonomy_common_name]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tcn]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcn] ON [dbo].[taxonomy_common_name]
(
	[taxonomy_genus_id] ASC,
	[taxonomy_species_id] ASC,
	[language_description] ASC,
	[name] ASC,
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcm_cr]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcm_cr] ON [dbo].[taxonomy_crop_map]
(
	[crop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcm_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcm_created] ON [dbo].[taxonomy_crop_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcm_modfied]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcm_modfied] ON [dbo].[taxonomy_crop_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcm_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcm_owned] ON [dbo].[taxonomy_crop_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcm_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcm_ts] ON [dbo].[taxonomy_crop_map]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tcm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcm] ON [dbo].[taxonomy_crop_map]
(
	[taxonomy_species_id] ASC,
	[crop_id] ASC,
	[alternate_crop_name] ASC,
	[common_crop_name] ASC,
	[is_primary_genepool] ASC,
	[is_secondary_genepool] ASC,
	[is_tertiary_genepool] ASC,
	[is_quaternary_genepool] ASC,
	[is_graftstock_genepool] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwr_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_c] ON [dbo].[taxonomy_cwr]
(
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwr_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_created] ON [dbo].[taxonomy_cwr]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwr_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_modified] ON [dbo].[taxonomy_cwr]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwr_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_owned] ON [dbo].[taxonomy_cwr]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwr_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_ts] ON [dbo].[taxonomy_cwr]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tcwr]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcwr] ON [dbo].[taxonomy_cwr]
(
	[taxonomy_species_id] ASC,
	[crop_name] ASC,
	[crop_common_name] ASC,
	[genepool_code] ASC,
	[is_graftstock_genepool] ASC,
	[ontology_trait_identifier] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwrp_cit]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_cit] ON [dbo].[taxonomy_cwr_priority]
(
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwrp_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_created] ON [dbo].[taxonomy_cwr_priority]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwrp_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_modified] ON [dbo].[taxonomy_cwr_priority]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwrp_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_owned] ON [dbo].[taxonomy_cwr_priority]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tcwrp_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_ts] ON [dbo].[taxonomy_cwr_priority]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tcwrp]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcwrp] ON [dbo].[taxonomy_cwr_priority]
(
	[taxonomy_species_id] ASC,
	[priority_year] ASC,
	[status_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tf_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tf_created] ON [dbo].[taxonomy_family]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tf_cur_tf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tf_cur_tf] ON [dbo].[taxonomy_family]
(
	[current_taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tf_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tf_modified] ON [dbo].[taxonomy_family]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tf_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tf_owned] ON [dbo].[taxonomy_family]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_tf_alt_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_tf_alt_name] ON [dbo].[taxonomy_family]
(
	[alternate_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_tf_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_tf_name] ON [dbo].[taxonomy_family]
(
	[family_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tf] ON [dbo].[taxonomy_family]
(
	[family_name] ASC,
	[family_authority] ASC,
	[subfamily_name] ASC,
	[tribe_name] ASC,
	[subtribe_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_tg_cur_tg]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [fk_tg_cur_tg] ON [dbo].[taxonomy_genus]
(
	[current_taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tg_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tg_created] ON [dbo].[taxonomy_genus]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tg_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tg_modified] ON [dbo].[taxonomy_genus]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tg_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tg_owned] ON [dbo].[taxonomy_genus]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tg_tf]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tg_tf] ON [dbo].[taxonomy_genus]
(
	[taxonomy_family_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_tg_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_tg_name] ON [dbo].[taxonomy_genus]
(
	[genus_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tg]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tg] ON [dbo].[taxonomy_genus]
(
	[taxonomy_family_id] ASC,
	[genus_name] ASC,
	[genus_authority] ASC,
	[subgenus_name] ASC,
	[section_name] ASC,
	[subsection_name] ASC,
	[series_name] ASC,
	[subseries_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tgm_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tgm_created] ON [dbo].[taxonomy_geography_map]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tgm_g]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tgm_g] ON [dbo].[taxonomy_geography_map]
(
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tgm_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tgm_modified] ON [dbo].[taxonomy_geography_map]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tgm_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tgm_owned] ON [dbo].[taxonomy_geography_map]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tgm_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tgm_ts] ON [dbo].[taxonomy_geography_map]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tgm]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tgm] ON [dbo].[taxonomy_geography_map]
(
	[taxonomy_species_id] ASC,
	[geography_id] ASC,
	[geography_status_code] ASC,
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tn_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tn_created] ON [dbo].[taxonomy_noxious]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tn_g]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tn_g] ON [dbo].[taxonomy_noxious]
(
	[geography_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tn_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tn_modified] ON [dbo].[taxonomy_noxious]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tn_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tn_owned] ON [dbo].[taxonomy_noxious]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tn_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tn_ts] ON [dbo].[taxonomy_noxious]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tn]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tn] ON [dbo].[taxonomy_noxious]
(
	[taxonomy_species_id] ASC,
	[geography_id] ASC,
	[noxious_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_c]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_c] ON [dbo].[taxonomy_species]
(
	[verifier_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_created] ON [dbo].[taxonomy_species]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_cur_t]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_cur_t] ON [dbo].[taxonomy_species]
(
	[current_taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_modified] ON [dbo].[taxonomy_species]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_owned] ON [dbo].[taxonomy_species]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_s]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_s] ON [dbo].[taxonomy_species]
(
	[priority1_site_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_s2]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_s2] ON [dbo].[taxonomy_species]
(
	[priority2_site_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_ts_tg]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_ts_tg] ON [dbo].[taxonomy_species]
(
	[taxonomy_genus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_ts_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ts_name] ON [dbo].[taxonomy_species]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_ts_nomen]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ts_nomen] ON [dbo].[taxonomy_species]
(
	[nomen_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_ts_s_name]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_ts_s_name] ON [dbo].[taxonomy_species]
(
	[species_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_ts] ON [dbo].[taxonomy_species]
(
	[name] ASC,
	[name_authority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tus_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tus_created] ON [dbo].[taxonomy_use]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tus_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tus_modified] ON [dbo].[taxonomy_use]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tus_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tus_owned] ON [dbo].[taxonomy_use]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_tus_ts]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tus_ts] ON [dbo].[taxonomy_use]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_tu_usage]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_tu_usage] ON [dbo].[taxonomy_use]
(
	[economic_usage_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_tu]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tu] ON [dbo].[taxonomy_use]
(
	[taxonomy_species_id] ASC,
	[economic_usage_code] ASC,
	[usage_type] ASC,
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_w6ivsi_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_w6ivsi_i] ON [dbo].[w6_site_inventory]
(
	[split_inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_w6si_created]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_w6si_created] ON [dbo].[w6_site_inventory]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_w6si_i]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_fk_w6si_i] ON [dbo].[w6_site_inventory]
(
	[inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_w6si_modified]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_w6si_modified] ON [dbo].[w6_site_inventory]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_fk_w6si_owned]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_w6si_owned] ON [dbo].[w6_site_inventory]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_wor]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wor] ON [dbo].[web_order_request]
(
	[web_cooperator_id] ASC,
	[ordered_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_wora]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wora] ON [dbo].[web_order_request_action]
(
	[web_order_request_id] ASC,
	[web_cooperator_id] ASC,
	[acted_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_worat]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_worat] ON [dbo].[web_order_request_attach]
(
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_wori]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wori] ON [dbo].[web_order_request_item]
(
	[web_order_request_id] ASC,
	[web_cooperator_id] ASC,
	[sequence_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_wu]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wu] ON [dbo].[web_user]
(
	[user_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_wuc]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wuc] ON [dbo].[web_user_cart]
(
	[web_user_id] ASC,
	[cart_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ndx_uniq_wuci]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wuci] ON [dbo].[web_user_cart_item]
(
	[web_user_cart_id] ASC,
	[accession_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_wup]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wup] ON [dbo].[web_user_preference]
(
	[web_user_id] ASC,
	[preference_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ndx_uniq_wusa]    Script Date: 9/9/2019 8:46:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_wusa] ON [dbo].[web_user_shipping_address]
(
	[web_user_id] ASC,
	[address_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[accession] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[accession_inv_attach] ADD  DEFAULT ('IMAGE') FOR [category_code]
GO
ALTER TABLE [dbo].[accession_inv_attach] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[accession_inv_group] ADD  DEFAULT ('N') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[accession_inv_name] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[accession_source] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[crop_attach] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[crop_trait_attach] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[crop_trait_code_attach] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[order_request_attach] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[taxonomy_attach] ADD  DEFAULT ('Y') FOR [is_web_visible]
GO
ALTER TABLE [dbo].[taxonomy_crop_map] ADD  DEFAULT ('N') FOR [is_graftstock_genepool]
GO
ALTER TABLE [dbo].[taxonomy_cwr] ADD  DEFAULT ('N') FOR [is_graftstock_genepool]
GO
ALTER TABLE [dbo].[accession]  WITH CHECK ADD  CONSTRAINT [fk_a_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession] CHECK CONSTRAINT [fk_a_created]
GO
ALTER TABLE [dbo].[accession]  WITH CHECK ADD  CONSTRAINT [fk_a_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession] CHECK CONSTRAINT [fk_a_modified]
GO
ALTER TABLE [dbo].[accession]  WITH CHECK ADD  CONSTRAINT [fk_a_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession] CHECK CONSTRAINT [fk_a_owned]
GO
ALTER TABLE [dbo].[accession]  WITH CHECK ADD  CONSTRAINT [fk_a_s1] FOREIGN KEY([backup_location1_site_id])
REFERENCES [dbo].[site] ([site_id])
GO
ALTER TABLE [dbo].[accession] CHECK CONSTRAINT [fk_a_s1]
GO
ALTER TABLE [dbo].[accession]  WITH CHECK ADD  CONSTRAINT [fk_a_s2] FOREIGN KEY([backup_location2_site_id])
REFERENCES [dbo].[site] ([site_id])
GO
ALTER TABLE [dbo].[accession] CHECK CONSTRAINT [fk_a_s2]
GO
ALTER TABLE [dbo].[accession]  WITH CHECK ADD  CONSTRAINT [fk_a_t] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[accession] CHECK CONSTRAINT [fk_a_t]
GO
ALTER TABLE [dbo].[accession_action]  WITH CHECK ADD  CONSTRAINT [fk_aa_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_action] CHECK CONSTRAINT [fk_aa_a]
GO
ALTER TABLE [dbo].[accession_action]  WITH CHECK ADD  CONSTRAINT [fk_aa_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_action] CHECK CONSTRAINT [fk_aa_c]
GO
ALTER TABLE [dbo].[accession_action]  WITH CHECK ADD  CONSTRAINT [fk_aa_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_action] CHECK CONSTRAINT [fk_aa_created]
GO
ALTER TABLE [dbo].[accession_action]  WITH CHECK ADD  CONSTRAINT [fk_aa_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[accession_action] CHECK CONSTRAINT [fk_aa_m]
GO
ALTER TABLE [dbo].[accession_action]  WITH CHECK ADD  CONSTRAINT [fk_aa_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_action] CHECK CONSTRAINT [fk_aa_modified]
GO
ALTER TABLE [dbo].[accession_action]  WITH CHECK ADD  CONSTRAINT [fk_aa_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_action] CHECK CONSTRAINT [fk_aa_owned]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_c] FOREIGN KEY([annotation_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_c]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_created]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_i]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_modified]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_or] FOREIGN KEY([order_request_id])
REFERENCES [dbo].[order_request] ([order_request_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_or]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_owned]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_t_new] FOREIGN KEY([new_taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_t_new]
GO
ALTER TABLE [dbo].[accession_inv_annotation]  WITH CHECK ADD  CONSTRAINT [fk_aian_t_old] FOREIGN KEY([old_taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[accession_inv_annotation] CHECK CONSTRAINT [fk_aian_t_old]
GO
ALTER TABLE [dbo].[accession_inv_attach]  WITH CHECK ADD  CONSTRAINT [fk_aiat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_attach] CHECK CONSTRAINT [fk_aiat_c]
GO
ALTER TABLE [dbo].[accession_inv_attach]  WITH CHECK ADD  CONSTRAINT [fk_aiat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_attach] CHECK CONSTRAINT [fk_aiat_created]
GO
ALTER TABLE [dbo].[accession_inv_attach]  WITH CHECK ADD  CONSTRAINT [fk_aiat_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[accession_inv_attach] CHECK CONSTRAINT [fk_aiat_i]
GO
ALTER TABLE [dbo].[accession_inv_attach]  WITH CHECK ADD  CONSTRAINT [fk_aiat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_attach] CHECK CONSTRAINT [fk_aiat_modified]
GO
ALTER TABLE [dbo].[accession_inv_attach]  WITH CHECK ADD  CONSTRAINT [fk_aiat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_attach] CHECK CONSTRAINT [fk_aiat_owned]
GO
ALTER TABLE [dbo].[accession_inv_group]  WITH CHECK ADD  CONSTRAINT [fk_aig_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group] CHECK CONSTRAINT [fk_aig_created]
GO
ALTER TABLE [dbo].[accession_inv_group]  WITH CHECK ADD  CONSTRAINT [fk_aig_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[accession_inv_group] CHECK CONSTRAINT [fk_aig_m]
GO
ALTER TABLE [dbo].[accession_inv_group]  WITH CHECK ADD  CONSTRAINT [fk_aig_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group] CHECK CONSTRAINT [fk_aig_modified]
GO
ALTER TABLE [dbo].[accession_inv_group]  WITH CHECK ADD  CONSTRAINT [fk_aig_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group] CHECK CONSTRAINT [fk_aig_owned]
GO
ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_aig] FOREIGN KEY([accession_inv_group_id])
REFERENCES [dbo].[accession_inv_group] ([accession_inv_group_id])
GO
ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_aig]
GO
ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_c]
GO
ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_created]
GO
ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_modified]
GO
ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_owned]
GO
ALTER TABLE [dbo].[accession_inv_group_map]  WITH CHECK ADD  CONSTRAINT [fk_aigm_aig] FOREIGN KEY([accession_inv_group_id])
REFERENCES [dbo].[accession_inv_group] ([accession_inv_group_id])
GO
ALTER TABLE [dbo].[accession_inv_group_map] CHECK CONSTRAINT [fk_aigm_aig]
GO
ALTER TABLE [dbo].[accession_inv_group_map]  WITH CHECK ADD  CONSTRAINT [fk_aigm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_map] CHECK CONSTRAINT [fk_aigm_created]
GO
ALTER TABLE [dbo].[accession_inv_group_map]  WITH CHECK ADD  CONSTRAINT [fk_aigm_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[accession_inv_group_map] CHECK CONSTRAINT [fk_aigm_i]
GO
ALTER TABLE [dbo].[accession_inv_group_map]  WITH CHECK ADD  CONSTRAINT [fk_aigm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_map] CHECK CONSTRAINT [fk_aigm_modified]
GO
ALTER TABLE [dbo].[accession_inv_group_map]  WITH CHECK ADD  CONSTRAINT [fk_aigm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_group_map] CHECK CONSTRAINT [fk_aigm_owned]
GO
ALTER TABLE [dbo].[accession_inv_name]  WITH CHECK ADD  CONSTRAINT [fk_ain_c] FOREIGN KEY([name_source_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_name] CHECK CONSTRAINT [fk_ain_c]
GO
ALTER TABLE [dbo].[accession_inv_name]  WITH CHECK ADD  CONSTRAINT [fk_ain_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_name] CHECK CONSTRAINT [fk_ain_created]
GO
ALTER TABLE [dbo].[accession_inv_name]  WITH CHECK ADD  CONSTRAINT [fk_ain_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[accession_inv_name] CHECK CONSTRAINT [fk_ain_i]
GO
ALTER TABLE [dbo].[accession_inv_name]  WITH CHECK ADD  CONSTRAINT [fk_ain_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_name] CHECK CONSTRAINT [fk_ain_modified]
GO
ALTER TABLE [dbo].[accession_inv_name]  WITH CHECK ADD  CONSTRAINT [fk_ain_ng] FOREIGN KEY([name_group_id])
REFERENCES [dbo].[name_group] ([name_group_id])
GO
ALTER TABLE [dbo].[accession_inv_name] CHECK CONSTRAINT [fk_ain_ng]
GO
ALTER TABLE [dbo].[accession_inv_name]  WITH CHECK ADD  CONSTRAINT [fk_ain_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_name] CHECK CONSTRAINT [fk_ain_owned]
GO
ALTER TABLE [dbo].[accession_inv_voucher]  WITH CHECK ADD  CONSTRAINT [fk_aiv_c] FOREIGN KEY([collector_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_voucher] CHECK CONSTRAINT [fk_aiv_c]
GO
ALTER TABLE [dbo].[accession_inv_voucher]  WITH CHECK ADD  CONSTRAINT [fk_aiv_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_voucher] CHECK CONSTRAINT [fk_aiv_created]
GO
ALTER TABLE [dbo].[accession_inv_voucher]  WITH CHECK ADD  CONSTRAINT [fk_aiv_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[accession_inv_voucher] CHECK CONSTRAINT [fk_aiv_i]
GO
ALTER TABLE [dbo].[accession_inv_voucher]  WITH CHECK ADD  CONSTRAINT [fk_aiv_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_voucher] CHECK CONSTRAINT [fk_aiv_modified]
GO
ALTER TABLE [dbo].[accession_inv_voucher]  WITH CHECK ADD  CONSTRAINT [fk_aiv_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_inv_voucher] CHECK CONSTRAINT [fk_aiv_owned]
GO
ALTER TABLE [dbo].[accession_ipr]  WITH CHECK ADD  CONSTRAINT [fk_ar_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_ipr] CHECK CONSTRAINT [fk_ar_a]
GO
ALTER TABLE [dbo].[accession_ipr]  WITH CHECK ADD  CONSTRAINT [fk_ar_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_ipr] CHECK CONSTRAINT [fk_ar_c]
GO
ALTER TABLE [dbo].[accession_ipr]  WITH CHECK ADD  CONSTRAINT [fk_ar_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_ipr] CHECK CONSTRAINT [fk_ar_created]
GO
ALTER TABLE [dbo].[accession_ipr]  WITH CHECK ADD  CONSTRAINT [fk_ar_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_ipr] CHECK CONSTRAINT [fk_ar_modified]
GO
ALTER TABLE [dbo].[accession_ipr]  WITH CHECK ADD  CONSTRAINT [fk_ar_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_ipr] CHECK CONSTRAINT [fk_ar_owned]
GO
ALTER TABLE [dbo].[accession_pedigree]  WITH CHECK ADD  CONSTRAINT [fk_ap_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_pedigree] CHECK CONSTRAINT [fk_ap_a]
GO
ALTER TABLE [dbo].[accession_pedigree]  WITH CHECK ADD  CONSTRAINT [fk_ap_a_female] FOREIGN KEY([female_accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_pedigree] CHECK CONSTRAINT [fk_ap_a_female]
GO
ALTER TABLE [dbo].[accession_pedigree]  WITH CHECK ADD  CONSTRAINT [fk_ap_a_male] FOREIGN KEY([male_accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_pedigree] CHECK CONSTRAINT [fk_ap_a_male]
GO
ALTER TABLE [dbo].[accession_pedigree]  WITH CHECK ADD  CONSTRAINT [fk_ap_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_pedigree] CHECK CONSTRAINT [fk_ap_created]
GO
ALTER TABLE [dbo].[accession_pedigree]  WITH CHECK ADD  CONSTRAINT [fk_ap_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_pedigree] CHECK CONSTRAINT [fk_ap_modified]
GO
ALTER TABLE [dbo].[accession_pedigree]  WITH CHECK ADD  CONSTRAINT [fk_ap_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_pedigree] CHECK CONSTRAINT [fk_ap_owned]
GO
ALTER TABLE [dbo].[accession_quarantine]  WITH CHECK ADD  CONSTRAINT [fk_aq_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_quarantine] CHECK CONSTRAINT [fk_aq_a]
GO
ALTER TABLE [dbo].[accession_quarantine]  WITH CHECK ADD  CONSTRAINT [fk_aq_c] FOREIGN KEY([custodial_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_quarantine] CHECK CONSTRAINT [fk_aq_c]
GO
ALTER TABLE [dbo].[accession_quarantine]  WITH CHECK ADD  CONSTRAINT [fk_aq_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_quarantine] CHECK CONSTRAINT [fk_aq_created]
GO
ALTER TABLE [dbo].[accession_quarantine]  WITH CHECK ADD  CONSTRAINT [fk_aq_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_quarantine] CHECK CONSTRAINT [fk_aq_modified]
GO
ALTER TABLE [dbo].[accession_quarantine]  WITH CHECK ADD  CONSTRAINT [fk_aq_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_quarantine] CHECK CONSTRAINT [fk_aq_owned]
GO
ALTER TABLE [dbo].[accession_source]  WITH CHECK ADD  CONSTRAINT [fk_as_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[accession_source] CHECK CONSTRAINT [fk_as_a]
GO
ALTER TABLE [dbo].[accession_source]  WITH CHECK ADD  CONSTRAINT [fk_as_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source] CHECK CONSTRAINT [fk_as_created]
GO
ALTER TABLE [dbo].[accession_source]  WITH CHECK ADD  CONSTRAINT [fk_as_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[accession_source] CHECK CONSTRAINT [fk_as_g]
GO
ALTER TABLE [dbo].[accession_source]  WITH CHECK ADD  CONSTRAINT [fk_as_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source] CHECK CONSTRAINT [fk_as_modified]
GO
ALTER TABLE [dbo].[accession_source]  WITH CHECK ADD  CONSTRAINT [fk_as_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source] CHECK CONSTRAINT [fk_as_owned]
GO
ALTER TABLE [dbo].[accession_source_map]  WITH CHECK ADD  CONSTRAINT [fk_asm_as] FOREIGN KEY([accession_source_id])
REFERENCES [dbo].[accession_source] ([accession_source_id])
GO
ALTER TABLE [dbo].[accession_source_map] CHECK CONSTRAINT [fk_asm_as]
GO
ALTER TABLE [dbo].[accession_source_map]  WITH CHECK ADD  CONSTRAINT [fk_asm_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source_map] CHECK CONSTRAINT [fk_asm_c]
GO
ALTER TABLE [dbo].[accession_source_map]  WITH CHECK ADD  CONSTRAINT [fk_asm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source_map] CHECK CONSTRAINT [fk_asm_created]
GO
ALTER TABLE [dbo].[accession_source_map]  WITH CHECK ADD  CONSTRAINT [fk_asm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source_map] CHECK CONSTRAINT [fk_asm_modified]
GO
ALTER TABLE [dbo].[accession_source_map]  WITH CHECK ADD  CONSTRAINT [fk_asm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[accession_source_map] CHECK CONSTRAINT [fk_asm_owned]
GO
ALTER TABLE [dbo].[app_resource]  WITH CHECK ADD  CONSTRAINT [fk_are_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_resource] CHECK CONSTRAINT [fk_are_created]
GO
ALTER TABLE [dbo].[app_resource]  WITH CHECK ADD  CONSTRAINT [fk_are_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_resource] CHECK CONSTRAINT [fk_are_modified]
GO
ALTER TABLE [dbo].[app_resource]  WITH CHECK ADD  CONSTRAINT [fk_are_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_resource] CHECK CONSTRAINT [fk_are_owned]
GO
ALTER TABLE [dbo].[app_resource]  WITH CHECK ADD  CONSTRAINT [fk_are_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[app_resource] CHECK CONSTRAINT [fk_are_sl]
GO
ALTER TABLE [dbo].[app_setting]  WITH CHECK ADD  CONSTRAINT [ndx_fk_aset_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_setting] CHECK CONSTRAINT [ndx_fk_aset_created]
GO
ALTER TABLE [dbo].[app_setting]  WITH CHECK ADD  CONSTRAINT [ndx_fk_aset_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_setting] CHECK CONSTRAINT [ndx_fk_aset_modified]
GO
ALTER TABLE [dbo].[app_setting]  WITH CHECK ADD  CONSTRAINT [ndx_fk_aset_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_setting] CHECK CONSTRAINT [ndx_fk_aset_owned]
GO
ALTER TABLE [dbo].[app_user_gui_setting]  WITH CHECK ADD  CONSTRAINT [fk_sugs_co] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_gui_setting] CHECK CONSTRAINT [fk_sugs_co]
GO
ALTER TABLE [dbo].[app_user_gui_setting]  WITH CHECK ADD  CONSTRAINT [fk_sugs_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_gui_setting] CHECK CONSTRAINT [fk_sugs_created]
GO
ALTER TABLE [dbo].[app_user_gui_setting]  WITH CHECK ADD  CONSTRAINT [fk_sugs_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_gui_setting] CHECK CONSTRAINT [fk_sugs_modified]
GO
ALTER TABLE [dbo].[app_user_gui_setting]  WITH CHECK ADD  CONSTRAINT [fk_sugs_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_gui_setting] CHECK CONSTRAINT [fk_sugs_owned]
GO
ALTER TABLE [dbo].[app_user_item_list]  WITH CHECK ADD  CONSTRAINT [fk_auil_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_item_list] CHECK CONSTRAINT [fk_auil_c]
GO
ALTER TABLE [dbo].[app_user_item_list]  WITH CHECK ADD  CONSTRAINT [ndx_fk_auil_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_item_list] CHECK CONSTRAINT [ndx_fk_auil_created]
GO
ALTER TABLE [dbo].[app_user_item_list]  WITH CHECK ADD  CONSTRAINT [ndx_fk_auil_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_item_list] CHECK CONSTRAINT [ndx_fk_auil_modified]
GO
ALTER TABLE [dbo].[app_user_item_list]  WITH CHECK ADD  CONSTRAINT [ndx_fk_auil_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[app_user_item_list] CHECK CONSTRAINT [ndx_fk_auil_owned]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_a]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_aipr] FOREIGN KEY([accession_ipr_id])
REFERENCES [dbo].[accession_ipr] ([accession_ipr_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_aipr]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_ap] FOREIGN KEY([accession_pedigree_id])
REFERENCES [dbo].[accession_pedigree] ([accession_pedigree_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_ap]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_created]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_gm] FOREIGN KEY([genetic_marker_id])
REFERENCES [dbo].[genetic_marker] ([genetic_marker_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_gm]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_l] FOREIGN KEY([literature_id])
REFERENCES [dbo].[literature] ([literature_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_l]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_m]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_modified]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_owned]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_tf] FOREIGN KEY([taxonomy_family_id])
REFERENCES [dbo].[taxonomy_family] ([taxonomy_family_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_tf]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_tg] FOREIGN KEY([taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_tg]
GO
ALTER TABLE [dbo].[citation]  WITH CHECK ADD  CONSTRAINT [fk_ci_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[citation] CHECK CONSTRAINT [fk_ci_ts]
GO
ALTER TABLE [dbo].[code_value]  WITH CHECK ADD  CONSTRAINT [fk_cdval_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[code_value] CHECK CONSTRAINT [fk_cdval_created]
GO
ALTER TABLE [dbo].[code_value]  WITH CHECK ADD  CONSTRAINT [fk_cdval_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[code_value] CHECK CONSTRAINT [fk_cdval_modified]
GO
ALTER TABLE [dbo].[code_value]  WITH CHECK ADD  CONSTRAINT [fk_cdval_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[code_value] CHECK CONSTRAINT [fk_cdval_owned]
GO
ALTER TABLE [dbo].[code_value_lang]  WITH CHECK ADD  CONSTRAINT [fk_cvl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[code_value_lang] CHECK CONSTRAINT [fk_cvl_created]
GO
ALTER TABLE [dbo].[code_value_lang]  WITH CHECK ADD  CONSTRAINT [fk_cvl_cv] FOREIGN KEY([code_value_id])
REFERENCES [dbo].[code_value] ([code_value_id])
GO
ALTER TABLE [dbo].[code_value_lang] CHECK CONSTRAINT [fk_cvl_cv]
GO
ALTER TABLE [dbo].[code_value_lang]  WITH CHECK ADD  CONSTRAINT [fk_cvl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[code_value_lang] CHECK CONSTRAINT [fk_cvl_modified]
GO
ALTER TABLE [dbo].[code_value_lang]  WITH CHECK ADD  CONSTRAINT [fk_cvl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[code_value_lang] CHECK CONSTRAINT [fk_cvl_owned]
GO
ALTER TABLE [dbo].[code_value_lang]  WITH CHECK ADD  CONSTRAINT [fk_cvl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[code_value_lang] CHECK CONSTRAINT [fk_cvl_sl]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_created]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_cur] FOREIGN KEY([current_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_cur]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_g]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_g2] FOREIGN KEY([secondary_geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_g2]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_modified]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_owned]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_s] FOREIGN KEY([site_id])
REFERENCES [dbo].[site] ([site_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_s]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_sl]
GO
ALTER TABLE [dbo].[cooperator]  WITH CHECK ADD  CONSTRAINT [fk_c_wc] FOREIGN KEY([web_cooperator_id])
REFERENCES [dbo].[web_cooperator] ([web_cooperator_id])
GO
ALTER TABLE [dbo].[cooperator] CHECK CONSTRAINT [fk_c_wc]
GO
ALTER TABLE [dbo].[cooperator_group]  WITH CHECK ADD  CONSTRAINT [fk_cg_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_group] CHECK CONSTRAINT [fk_cg_created]
GO
ALTER TABLE [dbo].[cooperator_group]  WITH CHECK ADD  CONSTRAINT [fk_cg_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_group] CHECK CONSTRAINT [fk_cg_modified]
GO
ALTER TABLE [dbo].[cooperator_group]  WITH CHECK ADD  CONSTRAINT [fk_cg_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_group] CHECK CONSTRAINT [fk_cg_owned]
GO
ALTER TABLE [dbo].[cooperator_group]  WITH CHECK ADD  CONSTRAINT [fk_cg_s] FOREIGN KEY([site_id])
REFERENCES [dbo].[site] ([site_id])
GO
ALTER TABLE [dbo].[cooperator_group] CHECK CONSTRAINT [fk_cg_s]
GO
ALTER TABLE [dbo].[cooperator_map]  WITH CHECK ADD  CONSTRAINT [fk_cm_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_map] CHECK CONSTRAINT [fk_cm_c]
GO
ALTER TABLE [dbo].[cooperator_map]  WITH CHECK ADD  CONSTRAINT [fk_cm_cg] FOREIGN KEY([cooperator_group_id])
REFERENCES [dbo].[cooperator_group] ([cooperator_group_id])
GO
ALTER TABLE [dbo].[cooperator_map] CHECK CONSTRAINT [fk_cm_cg]
GO
ALTER TABLE [dbo].[cooperator_map]  WITH CHECK ADD  CONSTRAINT [fk_cm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_map] CHECK CONSTRAINT [fk_cm_created]
GO
ALTER TABLE [dbo].[cooperator_map]  WITH CHECK ADD  CONSTRAINT [fk_cm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_map] CHECK CONSTRAINT [fk_cm_modified]
GO
ALTER TABLE [dbo].[cooperator_map]  WITH CHECK ADD  CONSTRAINT [fk_cm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[cooperator_map] CHECK CONSTRAINT [fk_cm_owned]
GO
ALTER TABLE [dbo].[crop]  WITH CHECK ADD  CONSTRAINT [fk_cr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop] CHECK CONSTRAINT [fk_cr_created]
GO
ALTER TABLE [dbo].[crop]  WITH CHECK ADD  CONSTRAINT [fk_cr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop] CHECK CONSTRAINT [fk_cr_modified]
GO
ALTER TABLE [dbo].[crop]  WITH CHECK ADD  CONSTRAINT [fk_cr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop] CHECK CONSTRAINT [fk_cr_owned]
GO
ALTER TABLE [dbo].[crop_attach]  WITH CHECK ADD  CONSTRAINT [fk_ca_c] FOREIGN KEY([crop_id])
REFERENCES [dbo].[crop] ([crop_id])
GO
ALTER TABLE [dbo].[crop_attach] CHECK CONSTRAINT [fk_ca_c]
GO
ALTER TABLE [dbo].[crop_attach]  WITH CHECK ADD  CONSTRAINT [fk_ca_co] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_attach] CHECK CONSTRAINT [fk_ca_co]
GO
ALTER TABLE [dbo].[crop_attach]  WITH CHECK ADD  CONSTRAINT [fk_ca_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_attach] CHECK CONSTRAINT [fk_ca_created]
GO
ALTER TABLE [dbo].[crop_attach]  WITH CHECK ADD  CONSTRAINT [fk_ca_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_attach] CHECK CONSTRAINT [fk_ca_modified]
GO
ALTER TABLE [dbo].[crop_attach]  WITH CHECK ADD  CONSTRAINT [fk_ca_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_attach] CHECK CONSTRAINT [fk_ca_owned]
GO
ALTER TABLE [dbo].[crop_trait]  WITH CHECK ADD  CONSTRAINT [fk_ct_cr] FOREIGN KEY([crop_id])
REFERENCES [dbo].[crop] ([crop_id])
GO
ALTER TABLE [dbo].[crop_trait] CHECK CONSTRAINT [fk_ct_cr]
GO
ALTER TABLE [dbo].[crop_trait]  WITH CHECK ADD  CONSTRAINT [fk_ct_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait] CHECK CONSTRAINT [fk_ct_created]
GO
ALTER TABLE [dbo].[crop_trait]  WITH CHECK ADD  CONSTRAINT [fk_ct_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait] CHECK CONSTRAINT [fk_ct_modified]
GO
ALTER TABLE [dbo].[crop_trait]  WITH CHECK ADD  CONSTRAINT [fk_ct_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait] CHECK CONSTRAINT [fk_ct_owned]
GO
ALTER TABLE [dbo].[crop_trait_attach]  WITH CHECK ADD  CONSTRAINT [fk_cta_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_attach] CHECK CONSTRAINT [fk_cta_c]
GO
ALTER TABLE [dbo].[crop_trait_attach]  WITH CHECK ADD  CONSTRAINT [fk_cta_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_attach] CHECK CONSTRAINT [fk_cta_created]
GO
ALTER TABLE [dbo].[crop_trait_attach]  WITH CHECK ADD  CONSTRAINT [fk_cta_ct] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[crop_trait_attach] CHECK CONSTRAINT [fk_cta_ct]
GO
ALTER TABLE [dbo].[crop_trait_attach]  WITH CHECK ADD  CONSTRAINT [fk_cta_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_attach] CHECK CONSTRAINT [fk_cta_modified]
GO
ALTER TABLE [dbo].[crop_trait_attach]  WITH CHECK ADD  CONSTRAINT [fk_cta_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_attach] CHECK CONSTRAINT [fk_cta_owned]
GO
ALTER TABLE [dbo].[crop_trait_code]  WITH CHECK ADD  CONSTRAINT [fk_tct_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code] CHECK CONSTRAINT [fk_tct_created]
GO
ALTER TABLE [dbo].[crop_trait_code]  WITH CHECK ADD  CONSTRAINT [fk_tct_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code] CHECK CONSTRAINT [fk_tct_modified]
GO
ALTER TABLE [dbo].[crop_trait_code]  WITH CHECK ADD  CONSTRAINT [fk_tct_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code] CHECK CONSTRAINT [fk_tct_owned]
GO
ALTER TABLE [dbo].[crop_trait_code]  WITH CHECK ADD  CONSTRAINT [fk_tct_tr] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[crop_trait_code] CHECK CONSTRAINT [fk_tct_tr]
GO
ALTER TABLE [dbo].[crop_trait_code_attach]  WITH CHECK ADD  CONSTRAINT [fk_ctca_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_attach] CHECK CONSTRAINT [fk_ctca_c]
GO
ALTER TABLE [dbo].[crop_trait_code_attach]  WITH CHECK ADD  CONSTRAINT [fk_ctca_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_attach] CHECK CONSTRAINT [fk_ctca_created]
GO
ALTER TABLE [dbo].[crop_trait_code_attach]  WITH CHECK ADD  CONSTRAINT [fk_ctca_ctc] FOREIGN KEY([crop_trait_code_id])
REFERENCES [dbo].[crop_trait_code] ([crop_trait_code_id])
GO
ALTER TABLE [dbo].[crop_trait_code_attach] CHECK CONSTRAINT [fk_ctca_ctc]
GO
ALTER TABLE [dbo].[crop_trait_code_attach]  WITH CHECK ADD  CONSTRAINT [fk_ctca_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_attach] CHECK CONSTRAINT [fk_ctca_modified]
GO
ALTER TABLE [dbo].[crop_trait_code_attach]  WITH CHECK ADD  CONSTRAINT [fk_ctca_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_attach] CHECK CONSTRAINT [fk_ctca_owned]
GO
ALTER TABLE [dbo].[crop_trait_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctcl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_lang] CHECK CONSTRAINT [fk_ctcl_created]
GO
ALTER TABLE [dbo].[crop_trait_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctcl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_lang] CHECK CONSTRAINT [fk_ctcl_modified]
GO
ALTER TABLE [dbo].[crop_trait_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctcl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_code_lang] CHECK CONSTRAINT [fk_ctcl_owned]
GO
ALTER TABLE [dbo].[crop_trait_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctcl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[crop_trait_code_lang] CHECK CONSTRAINT [fk_ctcl_sl]
GO
ALTER TABLE [dbo].[crop_trait_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctcl_tc] FOREIGN KEY([crop_trait_code_id])
REFERENCES [dbo].[crop_trait_code] ([crop_trait_code_id])
GO
ALTER TABLE [dbo].[crop_trait_code_lang] CHECK CONSTRAINT [fk_ctcl_tc]
GO
ALTER TABLE [dbo].[crop_trait_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_lang] CHECK CONSTRAINT [fk_ctl_created]
GO
ALTER TABLE [dbo].[crop_trait_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_lang] CHECK CONSTRAINT [fk_ctl_modified]
GO
ALTER TABLE [dbo].[crop_trait_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_lang] CHECK CONSTRAINT [fk_ctl_owned]
GO
ALTER TABLE [dbo].[crop_trait_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[crop_trait_lang] CHECK CONSTRAINT [fk_ctl_sl]
GO
ALTER TABLE [dbo].[crop_trait_lang]  WITH CHECK ADD  CONSTRAINT [fk_ctl_t] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[crop_trait_lang] CHECK CONSTRAINT [fk_ctl_t]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_created]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_ct] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_ct]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_ctc] FOREIGN KEY([crop_trait_code_id])
REFERENCES [dbo].[crop_trait_code] ([crop_trait_code_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_ctc]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_i]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_m]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_modified]
GO
ALTER TABLE [dbo].[crop_trait_observation]  WITH CHECK ADD  CONSTRAINT [fk_cto_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_observation] CHECK CONSTRAINT [fk_cto_owned]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_created]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_ct] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_ct]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_ctc] FOREIGN KEY([crop_trait_code_id])
REFERENCES [dbo].[crop_trait_code] ([crop_trait_code_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_ctc]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_cto] FOREIGN KEY([crop_trait_observation_id])
REFERENCES [dbo].[crop_trait_observation] ([crop_trait_observation_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_cto]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_i]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_m]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_modified]
GO
ALTER TABLE [dbo].[crop_trait_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_ctod_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[crop_trait_observation_data] CHECK CONSTRAINT [fk_ctod_owned]
GO
ALTER TABLE [dbo].[email]  WITH CHECK ADD  CONSTRAINT [fk_e_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[email] CHECK CONSTRAINT [fk_e_created]
GO
ALTER TABLE [dbo].[email]  WITH CHECK ADD  CONSTRAINT [fk_e_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[email] CHECK CONSTRAINT [fk_e_modified]
GO
ALTER TABLE [dbo].[email]  WITH CHECK ADD  CONSTRAINT [fk_e_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[email] CHECK CONSTRAINT [fk_e_owned]
GO
ALTER TABLE [dbo].[email_attach]  WITH CHECK ADD  CONSTRAINT [fk_ea_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[email_attach] CHECK CONSTRAINT [fk_ea_created]
GO
ALTER TABLE [dbo].[email_attach]  WITH CHECK ADD  CONSTRAINT [fk_ea_e] FOREIGN KEY([email_id])
REFERENCES [dbo].[email] ([email_id])
GO
ALTER TABLE [dbo].[email_attach] CHECK CONSTRAINT [fk_ea_e]
GO
ALTER TABLE [dbo].[email_attach]  WITH CHECK ADD  CONSTRAINT [fk_ea_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[email_attach] CHECK CONSTRAINT [fk_ea_modified]
GO
ALTER TABLE [dbo].[email_attach]  WITH CHECK ADD  CONSTRAINT [fk_ea_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[email_attach] CHECK CONSTRAINT [fk_ea_owned]
GO
ALTER TABLE [dbo].[exploration]  WITH CHECK ADD  CONSTRAINT [fk_ex_c] FOREIGN KEY([host_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration] CHECK CONSTRAINT [fk_ex_c]
GO
ALTER TABLE [dbo].[exploration]  WITH CHECK ADD  CONSTRAINT [fk_ex_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration] CHECK CONSTRAINT [fk_ex_created]
GO
ALTER TABLE [dbo].[exploration]  WITH CHECK ADD  CONSTRAINT [fk_ex_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration] CHECK CONSTRAINT [fk_ex_modified]
GO
ALTER TABLE [dbo].[exploration]  WITH CHECK ADD  CONSTRAINT [fk_ex_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration] CHECK CONSTRAINT [fk_ex_owned]
GO
ALTER TABLE [dbo].[exploration_map]  WITH CHECK ADD  CONSTRAINT [fk_exm_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration_map] CHECK CONSTRAINT [fk_exm_c]
GO
ALTER TABLE [dbo].[exploration_map]  WITH CHECK ADD  CONSTRAINT [fk_exm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration_map] CHECK CONSTRAINT [fk_exm_created]
GO
ALTER TABLE [dbo].[exploration_map]  WITH CHECK ADD  CONSTRAINT [fk_exm_ex] FOREIGN KEY([exploration_id])
REFERENCES [dbo].[exploration] ([exploration_id])
GO
ALTER TABLE [dbo].[exploration_map] CHECK CONSTRAINT [fk_exm_ex]
GO
ALTER TABLE [dbo].[exploration_map]  WITH CHECK ADD  CONSTRAINT [fk_exm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration_map] CHECK CONSTRAINT [fk_exm_modified]
GO
ALTER TABLE [dbo].[exploration_map]  WITH CHECK ADD  CONSTRAINT [fk_exm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[exploration_map] CHECK CONSTRAINT [fk_exm_owned]
GO
ALTER TABLE [dbo].[feedback]  WITH CHECK ADD  CONSTRAINT [fk_f_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback] CHECK CONSTRAINT [fk_f_created]
GO
ALTER TABLE [dbo].[feedback]  WITH CHECK ADD  CONSTRAINT [fk_f_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback] CHECK CONSTRAINT [fk_f_modified]
GO
ALTER TABLE [dbo].[feedback]  WITH CHECK ADD  CONSTRAINT [fk_f_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback] CHECK CONSTRAINT [fk_f_owned]
GO
ALTER TABLE [dbo].[feedback_attach]  WITH CHECK ADD  CONSTRAINT [fk_fa_f] FOREIGN KEY([feedback_id])
REFERENCES [dbo].[feedback] ([feedback_id])
GO
ALTER TABLE [dbo].[feedback_attach] CHECK CONSTRAINT [fk_fa_f]
GO
ALTER TABLE [dbo].[feedback_form]  WITH CHECK ADD  CONSTRAINT [fk_ff_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form] CHECK CONSTRAINT [fk_ff_created]
GO
ALTER TABLE [dbo].[feedback_form]  WITH CHECK ADD  CONSTRAINT [fk_ff_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form] CHECK CONSTRAINT [fk_ff_modified]
GO
ALTER TABLE [dbo].[feedback_form]  WITH CHECK ADD  CONSTRAINT [fk_ff_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form] CHECK CONSTRAINT [fk_ff_owned]
GO
ALTER TABLE [dbo].[feedback_form_field]  WITH CHECK ADD  CONSTRAINT [fk_fff_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form_field] CHECK CONSTRAINT [fk_fff_created]
GO
ALTER TABLE [dbo].[feedback_form_field]  WITH CHECK ADD  CONSTRAINT [fk_fff_ff] FOREIGN KEY([feedback_form_id])
REFERENCES [dbo].[feedback_form] ([feedback_form_id])
GO
ALTER TABLE [dbo].[feedback_form_field] CHECK CONSTRAINT [fk_fff_ff]
GO
ALTER TABLE [dbo].[feedback_form_field]  WITH CHECK ADD  CONSTRAINT [fk_fff_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form_field] CHECK CONSTRAINT [fk_fff_modified]
GO
ALTER TABLE [dbo].[feedback_form_field]  WITH CHECK ADD  CONSTRAINT [fk_fff_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form_field] CHECK CONSTRAINT [fk_fff_owned]
GO
ALTER TABLE [dbo].[feedback_form_trait]  WITH CHECK ADD  CONSTRAINT [fk_fft_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form_trait] CHECK CONSTRAINT [fk_fft_created]
GO
ALTER TABLE [dbo].[feedback_form_trait]  WITH CHECK ADD  CONSTRAINT [fk_fft_ct] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[feedback_form_trait] CHECK CONSTRAINT [fk_fft_ct]
GO
ALTER TABLE [dbo].[feedback_form_trait]  WITH CHECK ADD  CONSTRAINT [fk_fft_ff] FOREIGN KEY([feedback_form_id])
REFERENCES [dbo].[feedback_form] ([feedback_form_id])
GO
ALTER TABLE [dbo].[feedback_form_trait] CHECK CONSTRAINT [fk_fft_ff]
GO
ALTER TABLE [dbo].[feedback_form_trait]  WITH CHECK ADD  CONSTRAINT [fk_fft_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form_trait] CHECK CONSTRAINT [fk_fft_modified]
GO
ALTER TABLE [dbo].[feedback_form_trait]  WITH CHECK ADD  CONSTRAINT [fk_fft_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_form_trait] CHECK CONSTRAINT [fk_fft_owned]
GO
ALTER TABLE [dbo].[feedback_inventory]  WITH CHECK ADD  CONSTRAINT [fk_fi_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_inventory] CHECK CONSTRAINT [fk_fi_created]
GO
ALTER TABLE [dbo].[feedback_inventory]  WITH CHECK ADD  CONSTRAINT [fk_fi_f] FOREIGN KEY([feedback_id])
REFERENCES [dbo].[feedback] ([feedback_id])
GO
ALTER TABLE [dbo].[feedback_inventory] CHECK CONSTRAINT [fk_fi_f]
GO
ALTER TABLE [dbo].[feedback_inventory]  WITH CHECK ADD  CONSTRAINT [fk_fi_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[feedback_inventory] CHECK CONSTRAINT [fk_fi_i]
GO
ALTER TABLE [dbo].[feedback_inventory]  WITH CHECK ADD  CONSTRAINT [fk_fi_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_inventory] CHECK CONSTRAINT [fk_fi_modified]
GO
ALTER TABLE [dbo].[feedback_inventory]  WITH CHECK ADD  CONSTRAINT [fk_fi_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_inventory] CHECK CONSTRAINT [fk_fi_owned]
GO
ALTER TABLE [dbo].[feedback_report]  WITH CHECK ADD  CONSTRAINT [fk_fr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_report] CHECK CONSTRAINT [fk_fr_created]
GO
ALTER TABLE [dbo].[feedback_report]  WITH CHECK ADD  CONSTRAINT [fk_fr_f] FOREIGN KEY([feedback_id])
REFERENCES [dbo].[feedback] ([feedback_id])
GO
ALTER TABLE [dbo].[feedback_report] CHECK CONSTRAINT [fk_fr_f]
GO
ALTER TABLE [dbo].[feedback_report]  WITH CHECK ADD  CONSTRAINT [fk_fr_ff] FOREIGN KEY([feedback_form_id])
REFERENCES [dbo].[feedback_form] ([feedback_form_id])
GO
ALTER TABLE [dbo].[feedback_report] CHECK CONSTRAINT [fk_fr_ff]
GO
ALTER TABLE [dbo].[feedback_report]  WITH CHECK ADD  CONSTRAINT [fk_fr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_report] CHECK CONSTRAINT [fk_fr_modified]
GO
ALTER TABLE [dbo].[feedback_report]  WITH CHECK ADD  CONSTRAINT [fk_fr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_report] CHECK CONSTRAINT [fk_fr_owned]
GO
ALTER TABLE [dbo].[feedback_report_attach]  WITH CHECK ADD  CONSTRAINT [fk_fa_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_report_attach] CHECK CONSTRAINT [fk_fa_created]
GO
ALTER TABLE [dbo].[feedback_report_attach]  WITH CHECK ADD  CONSTRAINT [fk_fa_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_report_attach] CHECK CONSTRAINT [fk_fa_modified]
GO
ALTER TABLE [dbo].[feedback_report_attach]  WITH CHECK ADD  CONSTRAINT [fk_fa_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_report_attach] CHECK CONSTRAINT [fk_fa_owned]
GO
ALTER TABLE [dbo].[feedback_report_attach]  WITH CHECK ADD  CONSTRAINT [fk_frepa_frep] FOREIGN KEY([feedback_report_id])
REFERENCES [dbo].[feedback_report] ([feedback_report_id])
GO
ALTER TABLE [dbo].[feedback_report_attach] CHECK CONSTRAINT [fk_frepa_frep]
GO
ALTER TABLE [dbo].[feedback_result]  WITH CHECK ADD  CONSTRAINT [fk_fres_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result] CHECK CONSTRAINT [fk_fres_created]
GO
ALTER TABLE [dbo].[feedback_result]  WITH CHECK ADD  CONSTRAINT [fk_fres_fresg] FOREIGN KEY([feedback_result_group_id])
REFERENCES [dbo].[feedback_result_group] ([feedback_result_group_id])
GO
ALTER TABLE [dbo].[feedback_result] CHECK CONSTRAINT [fk_fres_fresg]
GO
ALTER TABLE [dbo].[feedback_result]  WITH CHECK ADD  CONSTRAINT [fk_fres_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[feedback_result] CHECK CONSTRAINT [fk_fres_i]
GO
ALTER TABLE [dbo].[feedback_result]  WITH CHECK ADD  CONSTRAINT [fk_fres_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result] CHECK CONSTRAINT [fk_fres_modified]
GO
ALTER TABLE [dbo].[feedback_result]  WITH CHECK ADD  CONSTRAINT [fk_fres_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result] CHECK CONSTRAINT [fk_fres_owned]
GO
ALTER TABLE [dbo].[feedback_result_attach]  WITH CHECK ADD  CONSTRAINT [fk_fresa_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_attach] CHECK CONSTRAINT [fk_fresa_created]
GO
ALTER TABLE [dbo].[feedback_result_attach]  WITH CHECK ADD  CONSTRAINT [fk_fresa_fres] FOREIGN KEY([feedback_result_id])
REFERENCES [dbo].[feedback_result] ([feedback_result_id])
GO
ALTER TABLE [dbo].[feedback_result_attach] CHECK CONSTRAINT [fk_fresa_fres]
GO
ALTER TABLE [dbo].[feedback_result_attach]  WITH CHECK ADD  CONSTRAINT [fk_fresa_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_attach] CHECK CONSTRAINT [fk_fresa_modified]
GO
ALTER TABLE [dbo].[feedback_result_attach]  WITH CHECK ADD  CONSTRAINT [fk_fresa_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_attach] CHECK CONSTRAINT [fk_fresa_owned]
GO
ALTER TABLE [dbo].[feedback_result_field]  WITH CHECK ADD  CONSTRAINT [fk_fresf_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_field] CHECK CONSTRAINT [fk_fresf_created]
GO
ALTER TABLE [dbo].[feedback_result_field]  WITH CHECK ADD  CONSTRAINT [fk_fresf_fff] FOREIGN KEY([feedback_form_field_id])
REFERENCES [dbo].[feedback_form_field] ([feedback_form_field_id])
GO
ALTER TABLE [dbo].[feedback_result_field] CHECK CONSTRAINT [fk_fresf_fff]
GO
ALTER TABLE [dbo].[feedback_result_field]  WITH CHECK ADD  CONSTRAINT [fk_fresf_fres] FOREIGN KEY([feedback_result_id])
REFERENCES [dbo].[feedback_result] ([feedback_result_id])
GO
ALTER TABLE [dbo].[feedback_result_field] CHECK CONSTRAINT [fk_fresf_fres]
GO
ALTER TABLE [dbo].[feedback_result_field]  WITH CHECK ADD  CONSTRAINT [fk_fresf_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_field] CHECK CONSTRAINT [fk_fresf_modified]
GO
ALTER TABLE [dbo].[feedback_result_field]  WITH CHECK ADD  CONSTRAINT [fk_fresf_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_field] CHECK CONSTRAINT [fk_fresf_owned]
GO
ALTER TABLE [dbo].[feedback_result_group]  WITH CHECK ADD  CONSTRAINT [fk_frg_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_group] CHECK CONSTRAINT [fk_frg_created]
GO
ALTER TABLE [dbo].[feedback_result_group]  WITH CHECK ADD  CONSTRAINT [fk_frg_fr] FOREIGN KEY([feedback_report_id])
REFERENCES [dbo].[feedback_report] ([feedback_report_id])
GO
ALTER TABLE [dbo].[feedback_result_group] CHECK CONSTRAINT [fk_frg_fr]
GO
ALTER TABLE [dbo].[feedback_result_group]  WITH CHECK ADD  CONSTRAINT [fk_frg_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_group] CHECK CONSTRAINT [fk_frg_modified]
GO
ALTER TABLE [dbo].[feedback_result_group]  WITH CHECK ADD  CONSTRAINT [fk_frg_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_group] CHECK CONSTRAINT [fk_frg_owned]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_created]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_ct] FOREIGN KEY([crop_trait_id])
REFERENCES [dbo].[crop_trait] ([crop_trait_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_ct]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_ctc] FOREIGN KEY([crop_trait_code_id])
REFERENCES [dbo].[crop_trait_code] ([crop_trait_code_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_ctc]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_fr] FOREIGN KEY([feedback_result_id])
REFERENCES [dbo].[feedback_result] ([feedback_result_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_fr]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_i]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_m]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_modified]
GO
ALTER TABLE [dbo].[feedback_result_trait_obs]  WITH CHECK ADD  CONSTRAINT [fk_fresto_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[feedback_result_trait_obs] CHECK CONSTRAINT [fk_fresto_owned]
GO
ALTER TABLE [dbo].[genetic_annotation]  WITH CHECK ADD  CONSTRAINT [fk_ga_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_annotation] CHECK CONSTRAINT [fk_ga_created]
GO
ALTER TABLE [dbo].[genetic_annotation]  WITH CHECK ADD  CONSTRAINT [fk_ga_gm] FOREIGN KEY([genetic_marker_id])
REFERENCES [dbo].[genetic_marker] ([genetic_marker_id])
GO
ALTER TABLE [dbo].[genetic_annotation] CHECK CONSTRAINT [fk_ga_gm]
GO
ALTER TABLE [dbo].[genetic_annotation]  WITH CHECK ADD  CONSTRAINT [fk_ga_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[genetic_annotation] CHECK CONSTRAINT [fk_ga_m]
GO
ALTER TABLE [dbo].[genetic_annotation]  WITH CHECK ADD  CONSTRAINT [fk_ga_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_annotation] CHECK CONSTRAINT [fk_ga_modified]
GO
ALTER TABLE [dbo].[genetic_annotation]  WITH CHECK ADD  CONSTRAINT [fk_ga_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_annotation] CHECK CONSTRAINT [fk_ga_owned]
GO
ALTER TABLE [dbo].[genetic_marker]  WITH CHECK ADD  CONSTRAINT [fk_gm_cr] FOREIGN KEY([crop_id])
REFERENCES [dbo].[crop] ([crop_id])
GO
ALTER TABLE [dbo].[genetic_marker] CHECK CONSTRAINT [fk_gm_cr]
GO
ALTER TABLE [dbo].[genetic_marker]  WITH CHECK ADD  CONSTRAINT [fk_gm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_marker] CHECK CONSTRAINT [fk_gm_created]
GO
ALTER TABLE [dbo].[genetic_marker]  WITH CHECK ADD  CONSTRAINT [fk_gm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_marker] CHECK CONSTRAINT [fk_gm_modified]
GO
ALTER TABLE [dbo].[genetic_marker]  WITH CHECK ADD  CONSTRAINT [fk_gm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_marker] CHECK CONSTRAINT [fk_gm_owned]
GO
ALTER TABLE [dbo].[genetic_observation]  WITH CHECK ADD  CONSTRAINT [fk_go_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_observation] CHECK CONSTRAINT [fk_go_created]
GO
ALTER TABLE [dbo].[genetic_observation]  WITH CHECK ADD  CONSTRAINT [fk_go_ga] FOREIGN KEY([genetic_annotation_id])
REFERENCES [dbo].[genetic_annotation] ([genetic_annotation_id])
GO
ALTER TABLE [dbo].[genetic_observation] CHECK CONSTRAINT [fk_go_ga]
GO
ALTER TABLE [dbo].[genetic_observation]  WITH CHECK ADD  CONSTRAINT [fk_go_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[genetic_observation] CHECK CONSTRAINT [fk_go_i]
GO
ALTER TABLE [dbo].[genetic_observation]  WITH CHECK ADD  CONSTRAINT [fk_go_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_observation] CHECK CONSTRAINT [fk_go_modified]
GO
ALTER TABLE [dbo].[genetic_observation]  WITH CHECK ADD  CONSTRAINT [fk_go_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_observation] CHECK CONSTRAINT [fk_go_owned]
GO
ALTER TABLE [dbo].[genetic_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_god_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_observation_data] CHECK CONSTRAINT [fk_god_created]
GO
ALTER TABLE [dbo].[genetic_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_god_ga] FOREIGN KEY([genetic_annotation_id])
REFERENCES [dbo].[genetic_annotation] ([genetic_annotation_id])
GO
ALTER TABLE [dbo].[genetic_observation_data] CHECK CONSTRAINT [fk_god_ga]
GO
ALTER TABLE [dbo].[genetic_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_god_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[genetic_observation_data] CHECK CONSTRAINT [fk_god_i]
GO
ALTER TABLE [dbo].[genetic_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_god_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_observation_data] CHECK CONSTRAINT [fk_god_modified]
GO
ALTER TABLE [dbo].[genetic_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_god_ob] FOREIGN KEY([genetic_observation_id])
REFERENCES [dbo].[genetic_observation] ([genetic_observation_id])
GO
ALTER TABLE [dbo].[genetic_observation_data] CHECK CONSTRAINT [fk_god_ob]
GO
ALTER TABLE [dbo].[genetic_observation_data]  WITH CHECK ADD  CONSTRAINT [fk_god_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[genetic_observation_data] CHECK CONSTRAINT [fk_god_owned]
GO
ALTER TABLE [dbo].[geneva_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gsi_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geneva_site_inventory] CHECK CONSTRAINT [fk_gsi_created]
GO
ALTER TABLE [dbo].[geneva_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gsi_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[geneva_site_inventory] CHECK CONSTRAINT [fk_gsi_i]
GO
ALTER TABLE [dbo].[geneva_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gsi_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geneva_site_inventory] CHECK CONSTRAINT [fk_gsi_modified]
GO
ALTER TABLE [dbo].[geneva_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gsi_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geneva_site_inventory] CHECK CONSTRAINT [fk_gsi_owned]
GO
ALTER TABLE [dbo].[geography]  WITH CHECK ADD  CONSTRAINT [fk_g_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geography] CHECK CONSTRAINT [fk_g_created]
GO
ALTER TABLE [dbo].[geography]  WITH CHECK ADD  CONSTRAINT [fk_g_cur_g] FOREIGN KEY([current_geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[geography] CHECK CONSTRAINT [fk_g_cur_g]
GO
ALTER TABLE [dbo].[geography]  WITH CHECK ADD  CONSTRAINT [fk_g_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geography] CHECK CONSTRAINT [fk_g_modified]
GO
ALTER TABLE [dbo].[geography]  WITH CHECK ADD  CONSTRAINT [fk_g_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geography] CHECK CONSTRAINT [fk_g_owned]
GO
ALTER TABLE [dbo].[geography_region_map]  WITH CHECK ADD  CONSTRAINT [fk_grm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geography_region_map] CHECK CONSTRAINT [fk_grm_created]
GO
ALTER TABLE [dbo].[geography_region_map]  WITH CHECK ADD  CONSTRAINT [fk_grm_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[geography_region_map] CHECK CONSTRAINT [fk_grm_g]
GO
ALTER TABLE [dbo].[geography_region_map]  WITH CHECK ADD  CONSTRAINT [fk_grm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geography_region_map] CHECK CONSTRAINT [fk_grm_modified]
GO
ALTER TABLE [dbo].[geography_region_map]  WITH CHECK ADD  CONSTRAINT [fk_grm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[geography_region_map] CHECK CONSTRAINT [fk_grm_owned]
GO
ALTER TABLE [dbo].[geography_region_map]  WITH CHECK ADD  CONSTRAINT [fo_grm_r] FOREIGN KEY([region_id])
REFERENCES [dbo].[region] ([region_id])
GO
ALTER TABLE [dbo].[geography_region_map] CHECK CONSTRAINT [fo_grm_r]
GO
ALTER TABLE [dbo].[gspi_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gspiivsi_i] FOREIGN KEY([split_inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[gspi_site_inventory] CHECK CONSTRAINT [fk_gspiivsi_i]
GO
ALTER TABLE [dbo].[gspi_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gspisi_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[gspi_site_inventory] CHECK CONSTRAINT [fk_gspisi_created]
GO
ALTER TABLE [dbo].[gspi_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gspisi_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[gspi_site_inventory] CHECK CONSTRAINT [fk_gspisi_i]
GO
ALTER TABLE [dbo].[gspi_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gspisi_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[gspi_site_inventory] CHECK CONSTRAINT [fk_gspisi_modified]
GO
ALTER TABLE [dbo].[gspi_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_gspisi_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[gspi_site_inventory] CHECK CONSTRAINT [fk_gspisi_owned]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_a]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_backup_i] FOREIGN KEY([backup_inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_backup_i]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_created]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_im] FOREIGN KEY([inventory_maint_policy_id])
REFERENCES [dbo].[inventory_maint_policy] ([inventory_maint_policy_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_im]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_m1] FOREIGN KEY([preservation_method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_m1]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_m2] FOREIGN KEY([regeneration_method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_m2]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_modified]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_owned]
GO
ALTER TABLE [dbo].[inventory]  WITH CHECK ADD  CONSTRAINT [fk_i_parent_i] FOREIGN KEY([parent_inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[inventory] CHECK CONSTRAINT [fk_i_parent_i]
GO
ALTER TABLE [dbo].[inventory_action]  WITH CHECK ADD  CONSTRAINT [fk_ia_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_action] CHECK CONSTRAINT [fk_ia_c]
GO
ALTER TABLE [dbo].[inventory_action]  WITH CHECK ADD  CONSTRAINT [fk_ia_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_action] CHECK CONSTRAINT [fk_ia_created]
GO
ALTER TABLE [dbo].[inventory_action]  WITH CHECK ADD  CONSTRAINT [fk_ia_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[inventory_action] CHECK CONSTRAINT [fk_ia_i]
GO
ALTER TABLE [dbo].[inventory_action]  WITH CHECK ADD  CONSTRAINT [fk_ia_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[inventory_action] CHECK CONSTRAINT [fk_ia_m]
GO
ALTER TABLE [dbo].[inventory_action]  WITH CHECK ADD  CONSTRAINT [fk_ia_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_action] CHECK CONSTRAINT [fk_ia_modified]
GO
ALTER TABLE [dbo].[inventory_action]  WITH CHECK ADD  CONSTRAINT [fk_ia_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_action] CHECK CONSTRAINT [fk_ia_owned]
GO
ALTER TABLE [dbo].[inventory_maint_policy]  WITH CHECK ADD  CONSTRAINT [fk_im_co] FOREIGN KEY([curator_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_maint_policy] CHECK CONSTRAINT [fk_im_co]
GO
ALTER TABLE [dbo].[inventory_maint_policy]  WITH CHECK ADD  CONSTRAINT [fk_im_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_maint_policy] CHECK CONSTRAINT [fk_im_created]
GO
ALTER TABLE [dbo].[inventory_maint_policy]  WITH CHECK ADD  CONSTRAINT [fk_im_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_maint_policy] CHECK CONSTRAINT [fk_im_modified]
GO
ALTER TABLE [dbo].[inventory_maint_policy]  WITH CHECK ADD  CONSTRAINT [fk_im_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_maint_policy] CHECK CONSTRAINT [fk_im_owned]
GO
ALTER TABLE [dbo].[inventory_quality_status]  WITH CHECK ADD  CONSTRAINT [fk_iqs_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_quality_status] CHECK CONSTRAINT [fk_iqs_created]
GO
ALTER TABLE [dbo].[inventory_quality_status]  WITH CHECK ADD  CONSTRAINT [fk_iqs_cur] FOREIGN KEY([tester_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_quality_status] CHECK CONSTRAINT [fk_iqs_cur]
GO
ALTER TABLE [dbo].[inventory_quality_status]  WITH CHECK ADD  CONSTRAINT [fk_iqs_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[inventory_quality_status] CHECK CONSTRAINT [fk_iqs_i]
GO
ALTER TABLE [dbo].[inventory_quality_status]  WITH CHECK ADD  CONSTRAINT [fk_iqs_me] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[inventory_quality_status] CHECK CONSTRAINT [fk_iqs_me]
GO
ALTER TABLE [dbo].[inventory_quality_status]  WITH CHECK ADD  CONSTRAINT [fk_iqs_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_quality_status] CHECK CONSTRAINT [fk_iqs_modified]
GO
ALTER TABLE [dbo].[inventory_quality_status]  WITH CHECK ADD  CONSTRAINT [fk_iqs_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_quality_status] CHECK CONSTRAINT [fk_iqs_owned]
GO
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_created]
GO
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_i]
GO
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_ivr] FOREIGN KEY([inventory_viability_rule_id])
REFERENCES [dbo].[inventory_viability_rule] ([inventory_viability_rule_id])
GO
ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_ivr]
GO
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_modified]
GO
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_owned]
GO
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_c] FOREIGN KEY([counter_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_c]
GO
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_created]
GO
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_iv] FOREIGN KEY([inventory_viability_id])
REFERENCES [dbo].[inventory_viability] ([inventory_viability_id])
GO
ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_iv]
GO
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_modified]
GO
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_ori] FOREIGN KEY([order_request_item_id])
REFERENCES [dbo].[order_request_item] ([order_request_item_id])
GO
ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_ori]
GO
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_owned]
GO
ALTER TABLE [dbo].[inventory_viability_rule]  WITH CHECK ADD  CONSTRAINT [fk_ivr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule] CHECK CONSTRAINT [fk_ivr_created]
GO
ALTER TABLE [dbo].[inventory_viability_rule]  WITH CHECK ADD  CONSTRAINT [fk_ivr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule] CHECK CONSTRAINT [fk_ivr_modified]
GO
ALTER TABLE [dbo].[inventory_viability_rule]  WITH CHECK ADD  CONSTRAINT [fk_ivr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule] CHECK CONSTRAINT [fk_ivr_owned]
GO
ALTER TABLE [dbo].[inventory_viability_rule_map]  WITH CHECK ADD  CONSTRAINT [fk_ivrm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule_map] CHECK CONSTRAINT [fk_ivrm_created]
GO
ALTER TABLE [dbo].[inventory_viability_rule_map]  WITH CHECK ADD  CONSTRAINT [fk_ivrm_ivr] FOREIGN KEY([inventory_viability_rule_id])
REFERENCES [dbo].[inventory_viability_rule] ([inventory_viability_rule_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule_map] CHECK CONSTRAINT [fk_ivrm_ivr]
GO
ALTER TABLE [dbo].[inventory_viability_rule_map]  WITH CHECK ADD  CONSTRAINT [fk_ivrm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule_map] CHECK CONSTRAINT [fk_ivrm_modified]
GO
ALTER TABLE [dbo].[inventory_viability_rule_map]  WITH CHECK ADD  CONSTRAINT [fk_ivrm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule_map] CHECK CONSTRAINT [fk_ivrm_owned]
GO
ALTER TABLE [dbo].[inventory_viability_rule_map]  WITH CHECK ADD  CONSTRAINT [fk_ivrm_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[inventory_viability_rule_map] CHECK CONSTRAINT [fk_ivrm_ts]
GO
ALTER TABLE [dbo].[literature]  WITH CHECK ADD  CONSTRAINT [fk_l_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[literature] CHECK CONSTRAINT [fk_l_created]
GO
ALTER TABLE [dbo].[literature]  WITH CHECK ADD  CONSTRAINT [fk_l_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[literature] CHECK CONSTRAINT [fk_l_modified]
GO
ALTER TABLE [dbo].[literature]  WITH CHECK ADD  CONSTRAINT [fk_l_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[literature] CHECK CONSTRAINT [fk_l_owned]
GO
ALTER TABLE [dbo].[method]  WITH CHECK ADD  CONSTRAINT [fk_m_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method] CHECK CONSTRAINT [fk_m_created]
GO
ALTER TABLE [dbo].[method]  WITH CHECK ADD  CONSTRAINT [fk_m_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[method] CHECK CONSTRAINT [fk_m_g]
GO
ALTER TABLE [dbo].[method]  WITH CHECK ADD  CONSTRAINT [fk_m_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method] CHECK CONSTRAINT [fk_m_modified]
GO
ALTER TABLE [dbo].[method]  WITH CHECK ADD  CONSTRAINT [fk_m_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method] CHECK CONSTRAINT [fk_m_owned]
GO
ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_c]
GO
ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_created]
GO
ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_m]
GO
ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_modified]
GO
ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_owned]
GO
ALTER TABLE [dbo].[method_map]  WITH CHECK ADD  CONSTRAINT [fk_mm_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_map] CHECK CONSTRAINT [fk_mm_c]
GO
ALTER TABLE [dbo].[method_map]  WITH CHECK ADD  CONSTRAINT [fk_mm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_map] CHECK CONSTRAINT [fk_mm_created]
GO
ALTER TABLE [dbo].[method_map]  WITH CHECK ADD  CONSTRAINT [fk_mm_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO
ALTER TABLE [dbo].[method_map] CHECK CONSTRAINT [fk_mm_m]
GO
ALTER TABLE [dbo].[method_map]  WITH CHECK ADD  CONSTRAINT [fk_mm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_map] CHECK CONSTRAINT [fk_mm_modified]
GO
ALTER TABLE [dbo].[method_map]  WITH CHECK ADD  CONSTRAINT [fk_mm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[method_map] CHECK CONSTRAINT [fk_mm_owned]
GO
ALTER TABLE [dbo].[name_group]  WITH CHECK ADD  CONSTRAINT [fk_ng_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[name_group] CHECK CONSTRAINT [fk_ng_created]
GO
ALTER TABLE [dbo].[name_group]  WITH CHECK ADD  CONSTRAINT [fk_ng_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[name_group] CHECK CONSTRAINT [fk_ng_modified]
GO
ALTER TABLE [dbo].[name_group]  WITH CHECK ADD  CONSTRAINT [fk_ng_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[name_group] CHECK CONSTRAINT [fk_ng_owned]
GO
ALTER TABLE [dbo].[nc7_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nc7si_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[nc7_site_inventory] CHECK CONSTRAINT [fk_nc7si_created]
GO
ALTER TABLE [dbo].[nc7_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nc7si_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[nc7_site_inventory] CHECK CONSTRAINT [fk_nc7si_i]
GO
ALTER TABLE [dbo].[nc7_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nc7si_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[nc7_site_inventory] CHECK CONSTRAINT [fk_nc7si_modified]
GO
ALTER TABLE [dbo].[nc7_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nc7si_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[nc7_site_inventory] CHECK CONSTRAINT [fk_nc7si_owned]
GO
ALTER TABLE [dbo].[ne9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_ne9si_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[ne9_site_inventory] CHECK CONSTRAINT [fk_ne9si_created]
GO
ALTER TABLE [dbo].[ne9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_ne9si_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[ne9_site_inventory] CHECK CONSTRAINT [fk_ne9si_i]
GO
ALTER TABLE [dbo].[ne9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_ne9si_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[ne9_site_inventory] CHECK CONSTRAINT [fk_ne9si_modified]
GO
ALTER TABLE [dbo].[ne9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_ne9si_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[ne9_site_inventory] CHECK CONSTRAINT [fk_ne9si_owned]
GO
ALTER TABLE [dbo].[nssl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nsslsi_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[nssl_site_inventory] CHECK CONSTRAINT [fk_nsslsi_created]
GO
ALTER TABLE [dbo].[nssl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nsslsi_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[nssl_site_inventory] CHECK CONSTRAINT [fk_nsslsi_i]
GO
ALTER TABLE [dbo].[nssl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nsslsi_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[nssl_site_inventory] CHECK CONSTRAINT [fk_nsslsi_modified]
GO
ALTER TABLE [dbo].[nssl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_nsslsi_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[nssl_site_inventory] CHECK CONSTRAINT [fk_nsslsi_owned]
GO
ALTER TABLE [dbo].[opgc_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_opgcsi_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[opgc_site_inventory] CHECK CONSTRAINT [fk_opgcsi_created]
GO
ALTER TABLE [dbo].[opgc_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_opgcsi_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[opgc_site_inventory] CHECK CONSTRAINT [fk_opgcsi_i]
GO
ALTER TABLE [dbo].[opgc_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_opgcsi_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[opgc_site_inventory] CHECK CONSTRAINT [fk_opgcsi_modified]
GO
ALTER TABLE [dbo].[opgc_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_opgcsi_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[opgc_site_inventory] CHECK CONSTRAINT [fk_opgcsi_owned]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_created]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_final_c] FOREIGN KEY([final_recipient_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_final_c]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_modified]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_original_or] FOREIGN KEY([original_order_request_id])
REFERENCES [dbo].[order_request] ([order_request_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_original_or]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_owned]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_requestor_c] FOREIGN KEY([requestor_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_requestor_c]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_ship_to_c] FOREIGN KEY([ship_to_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_ship_to_c]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_or_wor] FOREIGN KEY([web_order_request_id])
REFERENCES [dbo].[web_order_request] ([web_order_request_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_or_wor]
GO
ALTER TABLE [dbo].[order_request]  WITH CHECK ADD  CONSTRAINT [fk_oreq_f] FOREIGN KEY([feedback_id])
REFERENCES [dbo].[feedback] ([feedback_id])
GO
ALTER TABLE [dbo].[order_request] CHECK CONSTRAINT [fk_oreq_f]
GO
ALTER TABLE [dbo].[order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_ora_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_action] CHECK CONSTRAINT [fk_ora_c]
GO
ALTER TABLE [dbo].[order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_ora_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_action] CHECK CONSTRAINT [fk_ora_created]
GO
ALTER TABLE [dbo].[order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_ora_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_action] CHECK CONSTRAINT [fk_ora_modified]
GO
ALTER TABLE [dbo].[order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_ora_or] FOREIGN KEY([order_request_id])
REFERENCES [dbo].[order_request] ([order_request_id])
GO
ALTER TABLE [dbo].[order_request_action] CHECK CONSTRAINT [fk_ora_or]
GO
ALTER TABLE [dbo].[order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_ora_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_action] CHECK CONSTRAINT [fk_ora_owned]
GO
ALTER TABLE [dbo].[order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_orat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_attach] CHECK CONSTRAINT [fk_orat_c]
GO
ALTER TABLE [dbo].[order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_orat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_attach] CHECK CONSTRAINT [fk_orat_created]
GO
ALTER TABLE [dbo].[order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_orat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_attach] CHECK CONSTRAINT [fk_orat_modified]
GO
ALTER TABLE [dbo].[order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_orat_or] FOREIGN KEY([order_request_id])
REFERENCES [dbo].[order_request] ([order_request_id])
GO
ALTER TABLE [dbo].[order_request_attach] CHECK CONSTRAINT [fk_orat_or]
GO
ALTER TABLE [dbo].[order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_orat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_attach] CHECK CONSTRAINT [fk_orat_owned]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_created]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_i]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_modified]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_or] FOREIGN KEY([order_request_id])
REFERENCES [dbo].[order_request] ([order_request_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_or]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_owned]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_sc] FOREIGN KEY([source_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_sc]
GO
ALTER TABLE [dbo].[order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_ori_wori] FOREIGN KEY([web_order_request_item_id])
REFERENCES [dbo].[web_order_request_item] ([web_order_request_item_id])
GO
ALTER TABLE [dbo].[order_request_item] CHECK CONSTRAINT [fk_ori_wori]
GO
ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_c]
GO
ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_created]
GO
ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_modified]
GO
ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_ori] FOREIGN KEY([order_request_item_id])
REFERENCES [dbo].[order_request_item] ([order_request_item_id])
GO
ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_ori]
GO
ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_owned]
GO
ALTER TABLE [dbo].[order_request_phyto_log]  WITH CHECK ADD  CONSTRAINT [fk_orpl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_phyto_log] CHECK CONSTRAINT [fk_orpl_created]
GO
ALTER TABLE [dbo].[order_request_phyto_log]  WITH CHECK ADD  CONSTRAINT [fk_orpl_inspected_c] FOREIGN KEY([inspected_by_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_phyto_log] CHECK CONSTRAINT [fk_orpl_inspected_c]
GO
ALTER TABLE [dbo].[order_request_phyto_log]  WITH CHECK ADD  CONSTRAINT [fk_orpl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_phyto_log] CHECK CONSTRAINT [fk_orpl_modified]
GO
ALTER TABLE [dbo].[order_request_phyto_log]  WITH CHECK ADD  CONSTRAINT [fk_orpl_or] FOREIGN KEY([order_request_id])
REFERENCES [dbo].[order_request] ([order_request_id])
GO
ALTER TABLE [dbo].[order_request_phyto_log] CHECK CONSTRAINT [fk_orpl_or]
GO
ALTER TABLE [dbo].[order_request_phyto_log]  WITH CHECK ADD  CONSTRAINT [fk_orpl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[order_request_phyto_log] CHECK CONSTRAINT [fk_orpl_owned]
GO
ALTER TABLE [dbo].[parl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_parlsi_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[parl_site_inventory] CHECK CONSTRAINT [fk_parlsi_created]
GO
ALTER TABLE [dbo].[parl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_parlsi_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[parl_site_inventory] CHECK CONSTRAINT [fk_parlsi_i]
GO
ALTER TABLE [dbo].[parl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_parlsi_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[parl_site_inventory] CHECK CONSTRAINT [fk_parlsi_modified]
GO
ALTER TABLE [dbo].[parl_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_parlsi_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[parl_site_inventory] CHECK CONSTRAINT [fk_parlsi_owned]
GO
ALTER TABLE [dbo].[region]  WITH CHECK ADD  CONSTRAINT [fk_r_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[region] CHECK CONSTRAINT [fk_r_created]
GO
ALTER TABLE [dbo].[region]  WITH CHECK ADD  CONSTRAINT [fk_r_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[region] CHECK CONSTRAINT [fk_r_modified]
GO
ALTER TABLE [dbo].[region]  WITH CHECK ADD  CONSTRAINT [fk_r_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[region] CHECK CONSTRAINT [fk_r_owned]
GO
ALTER TABLE [dbo].[s9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_s9si_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[s9_site_inventory] CHECK CONSTRAINT [fk_s9si_created]
GO
ALTER TABLE [dbo].[s9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_s9si_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[s9_site_inventory] CHECK CONSTRAINT [fk_s9si_i]
GO
ALTER TABLE [dbo].[s9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_s9si_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[s9_site_inventory] CHECK CONSTRAINT [fk_s9si_modified]
GO
ALTER TABLE [dbo].[s9_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_s9si_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[s9_site_inventory] CHECK CONSTRAINT [fk_s9si_owned]
GO
ALTER TABLE [dbo].[site]  WITH CHECK ADD  CONSTRAINT [fk_s_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[site] CHECK CONSTRAINT [fk_s_created]
GO
ALTER TABLE [dbo].[site]  WITH CHECK ADD  CONSTRAINT [fk_s_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[site] CHECK CONSTRAINT [fk_s_modified]
GO
ALTER TABLE [dbo].[site]  WITH CHECK ADD  CONSTRAINT [fk_s_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[site] CHECK CONSTRAINT [fk_s_owned]
GO
ALTER TABLE [dbo].[source_desc_observation]  WITH CHECK ADD  CONSTRAINT [fk_sodo_as] FOREIGN KEY([accession_source_id])
REFERENCES [dbo].[accession_source] ([accession_source_id])
GO
ALTER TABLE [dbo].[source_desc_observation] CHECK CONSTRAINT [fk_sodo_as]
GO
ALTER TABLE [dbo].[source_desc_observation]  WITH CHECK ADD  CONSTRAINT [fk_sodo_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_desc_observation] CHECK CONSTRAINT [fk_sodo_created]
GO
ALTER TABLE [dbo].[source_desc_observation]  WITH CHECK ADD  CONSTRAINT [fk_sodo_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_desc_observation] CHECK CONSTRAINT [fk_sodo_modified]
GO
ALTER TABLE [dbo].[source_desc_observation]  WITH CHECK ADD  CONSTRAINT [fk_sodo_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_desc_observation] CHECK CONSTRAINT [fk_sodo_owned]
GO
ALTER TABLE [dbo].[source_desc_observation]  WITH CHECK ADD  CONSTRAINT [fk_sodo_sd] FOREIGN KEY([source_descriptor_id])
REFERENCES [dbo].[source_descriptor] ([source_descriptor_id])
GO
ALTER TABLE [dbo].[source_desc_observation] CHECK CONSTRAINT [fk_sodo_sd]
GO
ALTER TABLE [dbo].[source_desc_observation]  WITH CHECK ADD  CONSTRAINT [fk_sodo_sodc] FOREIGN KEY([source_descriptor_code_id])
REFERENCES [dbo].[source_descriptor_code] ([source_descriptor_code_id])
GO
ALTER TABLE [dbo].[source_desc_observation] CHECK CONSTRAINT [fk_sodo_sodc]
GO
ALTER TABLE [dbo].[source_descriptor]  WITH CHECK ADD  CONSTRAINT [fk_sd_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor] CHECK CONSTRAINT [fk_sd_created]
GO
ALTER TABLE [dbo].[source_descriptor]  WITH CHECK ADD  CONSTRAINT [fk_sd_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor] CHECK CONSTRAINT [fk_sd_modified]
GO
ALTER TABLE [dbo].[source_descriptor]  WITH CHECK ADD  CONSTRAINT [fk_sd_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor] CHECK CONSTRAINT [fk_sd_owned]
GO
ALTER TABLE [dbo].[source_descriptor_code]  WITH CHECK ADD  CONSTRAINT [fk_sodc_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_code] CHECK CONSTRAINT [fk_sodc_created]
GO
ALTER TABLE [dbo].[source_descriptor_code]  WITH CHECK ADD  CONSTRAINT [fk_sodc_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_code] CHECK CONSTRAINT [fk_sodc_modified]
GO
ALTER TABLE [dbo].[source_descriptor_code]  WITH CHECK ADD  CONSTRAINT [fk_sodc_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_code] CHECK CONSTRAINT [fk_sodc_owned]
GO
ALTER TABLE [dbo].[source_descriptor_code]  WITH CHECK ADD  CONSTRAINT [fk_sodc_sd] FOREIGN KEY([source_descriptor_id])
REFERENCES [dbo].[source_descriptor] ([source_descriptor_id])
GO
ALTER TABLE [dbo].[source_descriptor_code] CHECK CONSTRAINT [fk_sodc_sd]
GO
ALTER TABLE [dbo].[source_descriptor_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodcl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_code_lang] CHECK CONSTRAINT [fk_sodcl_created]
GO
ALTER TABLE [dbo].[source_descriptor_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodcl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_code_lang] CHECK CONSTRAINT [fk_sodcl_modified]
GO
ALTER TABLE [dbo].[source_descriptor_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodcl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_code_lang] CHECK CONSTRAINT [fk_sodcl_owned]
GO
ALTER TABLE [dbo].[source_descriptor_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodcl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[source_descriptor_code_lang] CHECK CONSTRAINT [fk_sodcl_sl]
GO
ALTER TABLE [dbo].[source_descriptor_code_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodcl_sodc] FOREIGN KEY([source_descriptor_code_id])
REFERENCES [dbo].[source_descriptor_code] ([source_descriptor_code_id])
GO
ALTER TABLE [dbo].[source_descriptor_code_lang] CHECK CONSTRAINT [fk_sodcl_sodc]
GO
ALTER TABLE [dbo].[source_descriptor_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_lang] CHECK CONSTRAINT [fk_sodl_created]
GO
ALTER TABLE [dbo].[source_descriptor_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_lang] CHECK CONSTRAINT [fk_sodl_modified]
GO
ALTER TABLE [dbo].[source_descriptor_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[source_descriptor_lang] CHECK CONSTRAINT [fk_sodl_owned]
GO
ALTER TABLE [dbo].[source_descriptor_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodl_sd] FOREIGN KEY([source_descriptor_id])
REFERENCES [dbo].[source_descriptor] ([source_descriptor_id])
GO
ALTER TABLE [dbo].[source_descriptor_lang] CHECK CONSTRAINT [fk_sodl_sd]
GO
ALTER TABLE [dbo].[source_descriptor_lang]  WITH CHECK ADD  CONSTRAINT [fk_sodl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[source_descriptor_lang] CHECK CONSTRAINT [fk_sodl_sl]
GO
ALTER TABLE [dbo].[sys_database]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdb_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database] CHECK CONSTRAINT [ndx_fk_sdb_created]
GO
ALTER TABLE [dbo].[sys_database]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdb_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database] CHECK CONSTRAINT [ndx_fk_sdb_modified]
GO
ALTER TABLE [dbo].[sys_database]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdb_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database] CHECK CONSTRAINT [ndx_fk_sdb_owned]
GO
ALTER TABLE [dbo].[sys_database_migration]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdbm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database_migration] CHECK CONSTRAINT [ndx_fk_sdbm_created]
GO
ALTER TABLE [dbo].[sys_database_migration]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdbm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database_migration] CHECK CONSTRAINT [ndx_fk_sdbm_modified]
GO
ALTER TABLE [dbo].[sys_database_migration]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdbm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database_migration] CHECK CONSTRAINT [ndx_fk_sdbm_owned]
GO
ALTER TABLE [dbo].[sys_database_migration_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdbml_sdbm] FOREIGN KEY([sys_database_migration_id])
REFERENCES [dbo].[sys_database_migration] ([sys_database_migration_id])
GO
ALTER TABLE [dbo].[sys_database_migration_lang] CHECK CONSTRAINT [fk_sdbml_sdbm]
GO
ALTER TABLE [dbo].[sys_database_migration_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdbml_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database_migration_lang] CHECK CONSTRAINT [ndx_fk_sdbml_created]
GO
ALTER TABLE [dbo].[sys_database_migration_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdbml_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database_migration_lang] CHECK CONSTRAINT [ndx_fk_sdbml_modified]
GO
ALTER TABLE [dbo].[sys_database_migration_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sdbml_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_database_migration_lang] CHECK CONSTRAINT [ndx_fk_sdbml_owned]
GO
ALTER TABLE [dbo].[sys_datatrigger]  WITH CHECK ADD  CONSTRAINT [fk_sdt_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_datatrigger] CHECK CONSTRAINT [fk_sdt_created]
GO
ALTER TABLE [dbo].[sys_datatrigger]  WITH CHECK ADD  CONSTRAINT [fk_sdt_dv] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_datatrigger] CHECK CONSTRAINT [fk_sdt_dv]
GO
ALTER TABLE [dbo].[sys_datatrigger]  WITH CHECK ADD  CONSTRAINT [fk_sdt_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_datatrigger] CHECK CONSTRAINT [fk_sdt_modified]
GO
ALTER TABLE [dbo].[sys_datatrigger]  WITH CHECK ADD  CONSTRAINT [fk_sdt_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_datatrigger] CHECK CONSTRAINT [fk_sdt_owned]
GO
ALTER TABLE [dbo].[sys_datatrigger]  WITH CHECK ADD  CONSTRAINT [fk_sdt_st] FOREIGN KEY([sys_table_id])
REFERENCES [dbo].[sys_table] ([sys_table_id])
GO
ALTER TABLE [dbo].[sys_datatrigger] CHECK CONSTRAINT [fk_sdt_st]
GO
ALTER TABLE [dbo].[sys_datatrigger_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdtl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_datatrigger_lang] CHECK CONSTRAINT [fk_sdtl_created]
GO
ALTER TABLE [dbo].[sys_datatrigger_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdtl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_datatrigger_lang] CHECK CONSTRAINT [fk_sdtl_modified]
GO
ALTER TABLE [dbo].[sys_datatrigger_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdtl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_datatrigger_lang] CHECK CONSTRAINT [fk_sdtl_owned]
GO
ALTER TABLE [dbo].[sys_datatrigger_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdtl_sdt] FOREIGN KEY([sys_datatrigger_id])
REFERENCES [dbo].[sys_datatrigger] ([sys_datatrigger_id])
GO
ALTER TABLE [dbo].[sys_datatrigger_lang] CHECK CONSTRAINT [fk_sdtl_sdt]
GO
ALTER TABLE [dbo].[sys_datatrigger_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdtl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_datatrigger_lang] CHECK CONSTRAINT [fk_sdtl_sl]
GO
ALTER TABLE [dbo].[sys_dataview]  WITH CHECK ADD  CONSTRAINT [fk_sr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview] CHECK CONSTRAINT [fk_sr_created]
GO
ALTER TABLE [dbo].[sys_dataview]  WITH CHECK ADD  CONSTRAINT [fk_sr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview] CHECK CONSTRAINT [fk_sr_modified]
GO
ALTER TABLE [dbo].[sys_dataview]  WITH CHECK ADD  CONSTRAINT [fk_sr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview] CHECK CONSTRAINT [fk_sr_owned]
GO
ALTER TABLE [dbo].[sys_dataview_field]  WITH CHECK ADD  CONSTRAINT [fk_srf_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_field] CHECK CONSTRAINT [fk_srf_created]
GO
ALTER TABLE [dbo].[sys_dataview_field]  WITH CHECK ADD  CONSTRAINT [fk_srf_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_field] CHECK CONSTRAINT [fk_srf_modified]
GO
ALTER TABLE [dbo].[sys_dataview_field]  WITH CHECK ADD  CONSTRAINT [fk_srf_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_field] CHECK CONSTRAINT [fk_srf_owned]
GO
ALTER TABLE [dbo].[sys_dataview_field]  WITH CHECK ADD  CONSTRAINT [fk_srf_sr] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_dataview_field] CHECK CONSTRAINT [fk_srf_sr]
GO
ALTER TABLE [dbo].[sys_dataview_field]  WITH CHECK ADD  CONSTRAINT [fk_srf_stf] FOREIGN KEY([sys_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_dataview_field] CHECK CONSTRAINT [fk_srf_stf]
GO
ALTER TABLE [dbo].[sys_dataview_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_srfl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_field_lang] CHECK CONSTRAINT [fk_srfl_created]
GO
ALTER TABLE [dbo].[sys_dataview_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_srfl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_field_lang] CHECK CONSTRAINT [fk_srfl_modified]
GO
ALTER TABLE [dbo].[sys_dataview_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_srfl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_field_lang] CHECK CONSTRAINT [fk_srfl_owned]
GO
ALTER TABLE [dbo].[sys_dataview_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_srfl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_dataview_field_lang] CHECK CONSTRAINT [fk_srfl_sl]
GO
ALTER TABLE [dbo].[sys_dataview_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_srfl_srf] FOREIGN KEY([sys_dataview_field_id])
REFERENCES [dbo].[sys_dataview_field] ([sys_dataview_field_id])
GO
ALTER TABLE [dbo].[sys_dataview_field_lang] CHECK CONSTRAINT [fk_srfl_srf]
GO
ALTER TABLE [dbo].[sys_dataview_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_lang] CHECK CONSTRAINT [fk_sdl_created]
GO
ALTER TABLE [dbo].[sys_dataview_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_lang] CHECK CONSTRAINT [fk_sdl_modified]
GO
ALTER TABLE [dbo].[sys_dataview_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_lang] CHECK CONSTRAINT [fk_sdl_owned]
GO
ALTER TABLE [dbo].[sys_dataview_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdl_sd] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_dataview_lang] CHECK CONSTRAINT [fk_sdl_sd]
GO
ALTER TABLE [dbo].[sys_dataview_lang]  WITH CHECK ADD  CONSTRAINT [fk_sdl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_dataview_lang] CHECK CONSTRAINT [fk_sdl_sl]
GO
ALTER TABLE [dbo].[sys_dataview_param]  WITH CHECK ADD  CONSTRAINT [fk_srp_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_param] CHECK CONSTRAINT [fk_srp_created]
GO
ALTER TABLE [dbo].[sys_dataview_param]  WITH CHECK ADD  CONSTRAINT [fk_srp_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_param] CHECK CONSTRAINT [fk_srp_modified]
GO
ALTER TABLE [dbo].[sys_dataview_param]  WITH CHECK ADD  CONSTRAINT [fk_srp_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_param] CHECK CONSTRAINT [fk_srp_owned]
GO
ALTER TABLE [dbo].[sys_dataview_param]  WITH CHECK ADD  CONSTRAINT [fk_srp_sr] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_dataview_param] CHECK CONSTRAINT [fk_srp_sr]
GO
ALTER TABLE [dbo].[sys_dataview_sql]  WITH CHECK ADD  CONSTRAINT [fk_srs_createdby] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_sql] CHECK CONSTRAINT [fk_srs_createdby]
GO
ALTER TABLE [dbo].[sys_dataview_sql]  WITH CHECK ADD  CONSTRAINT [fk_srs_modifiedby] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_sql] CHECK CONSTRAINT [fk_srs_modifiedby]
GO
ALTER TABLE [dbo].[sys_dataview_sql]  WITH CHECK ADD  CONSTRAINT [fk_srs_ownedby] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_dataview_sql] CHECK CONSTRAINT [fk_srs_ownedby]
GO
ALTER TABLE [dbo].[sys_dataview_sql]  WITH CHECK ADD  CONSTRAINT [fk_srs_sr] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_dataview_sql] CHECK CONSTRAINT [fk_srs_sr]
GO
ALTER TABLE [dbo].[sys_file]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sf_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file] CHECK CONSTRAINT [ndx_fk_sf_created]
GO
ALTER TABLE [dbo].[sys_file]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sf_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file] CHECK CONSTRAINT [ndx_fk_sf_modified]
GO
ALTER TABLE [dbo].[sys_file]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sf_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file] CHECK CONSTRAINT [ndx_fk_sf_owned]
GO
ALTER TABLE [dbo].[sys_file_group]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfg_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_group] CHECK CONSTRAINT [ndx_fk_sfg_created]
GO
ALTER TABLE [dbo].[sys_file_group]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfg_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_group] CHECK CONSTRAINT [ndx_fk_sfg_modified]
GO
ALTER TABLE [dbo].[sys_file_group]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfg_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_group] CHECK CONSTRAINT [ndx_fk_sfg_owned]
GO
ALTER TABLE [dbo].[sys_file_group_map]  WITH CHECK ADD  CONSTRAINT [fk_sfgm_sf] FOREIGN KEY([sys_file_id])
REFERENCES [dbo].[sys_file] ([sys_file_id])
GO
ALTER TABLE [dbo].[sys_file_group_map] CHECK CONSTRAINT [fk_sfgm_sf]
GO
ALTER TABLE [dbo].[sys_file_group_map]  WITH CHECK ADD  CONSTRAINT [fk_sfgm_sfg] FOREIGN KEY([sys_file_group_id])
REFERENCES [dbo].[sys_file_group] ([sys_file_group_id])
GO
ALTER TABLE [dbo].[sys_file_group_map] CHECK CONSTRAINT [fk_sfgm_sfg]
GO
ALTER TABLE [dbo].[sys_file_group_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfgm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_group_map] CHECK CONSTRAINT [ndx_fk_sfgm_created]
GO
ALTER TABLE [dbo].[sys_file_group_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfgm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_group_map] CHECK CONSTRAINT [ndx_fk_sfgm_modified]
GO
ALTER TABLE [dbo].[sys_file_group_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfgm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_group_map] CHECK CONSTRAINT [ndx_fk_sfgm_owned]
GO
ALTER TABLE [dbo].[sys_file_lang]  WITH CHECK ADD  CONSTRAINT [fk_sec_file_lang_sec_file] FOREIGN KEY([sys_file_id])
REFERENCES [dbo].[sys_file] ([sys_file_id])
GO
ALTER TABLE [dbo].[sys_file_lang] CHECK CONSTRAINT [fk_sec_file_lang_sec_file]
GO
ALTER TABLE [dbo].[sys_file_lang]  WITH CHECK ADD  CONSTRAINT [fk_sfl_sf] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_file_lang] CHECK CONSTRAINT [fk_sfl_sf]
GO
ALTER TABLE [dbo].[sys_file_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_lang] CHECK CONSTRAINT [ndx_fk_sfl_created]
GO
ALTER TABLE [dbo].[sys_file_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_lang] CHECK CONSTRAINT [ndx_fk_sfl_modified]
GO
ALTER TABLE [dbo].[sys_file_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sfl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_file_lang] CHECK CONSTRAINT [ndx_fk_sfl_owned]
GO
ALTER TABLE [dbo].[sys_group]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sg_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group] CHECK CONSTRAINT [ndx_fk_sg_created]
GO
ALTER TABLE [dbo].[sys_group]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sg_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group] CHECK CONSTRAINT [ndx_fk_sg_modified]
GO
ALTER TABLE [dbo].[sys_group]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sg_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group] CHECK CONSTRAINT [ndx_fk_sg_owned]
GO
ALTER TABLE [dbo].[sys_group_lang]  WITH CHECK ADD  CONSTRAINT [fk_sgl_sg] FOREIGN KEY([sys_group_id])
REFERENCES [dbo].[sys_group] ([sys_group_id])
GO
ALTER TABLE [dbo].[sys_group_lang] CHECK CONSTRAINT [fk_sgl_sg]
GO
ALTER TABLE [dbo].[sys_group_lang]  WITH CHECK ADD  CONSTRAINT [fk_sgl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_group_lang] CHECK CONSTRAINT [fk_sgl_sl]
GO
ALTER TABLE [dbo].[sys_group_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_lang] CHECK CONSTRAINT [ndx_fk_sgl_created]
GO
ALTER TABLE [dbo].[sys_group_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_lang] CHECK CONSTRAINT [ndx_fk_sgl_modified]
GO
ALTER TABLE [dbo].[sys_group_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_lang] CHECK CONSTRAINT [ndx_fk_sgl_owned]
GO
ALTER TABLE [dbo].[sys_group_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sgpm_sg] FOREIGN KEY([sys_group_id])
REFERENCES [dbo].[sys_group] ([sys_group_id])
GO
ALTER TABLE [dbo].[sys_group_permission_map] CHECK CONSTRAINT [fk_sgpm_sg]
GO
ALTER TABLE [dbo].[sys_group_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sgpm_sp] FOREIGN KEY([sys_permission_id])
REFERENCES [dbo].[sys_permission] ([sys_permission_id])
GO
ALTER TABLE [dbo].[sys_group_permission_map] CHECK CONSTRAINT [fk_sgpm_sp]
GO
ALTER TABLE [dbo].[sys_group_permission_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgpm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_permission_map] CHECK CONSTRAINT [ndx_fk_sgpm_created]
GO
ALTER TABLE [dbo].[sys_group_permission_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgpm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_permission_map] CHECK CONSTRAINT [ndx_fk_sgpm_modified]
GO
ALTER TABLE [dbo].[sys_group_permission_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgpm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_permission_map] CHECK CONSTRAINT [ndx_fk_sgpm_owned]
GO
ALTER TABLE [dbo].[sys_group_user_map]  WITH CHECK ADD  CONSTRAINT [fk_sgum_sg] FOREIGN KEY([sys_group_id])
REFERENCES [dbo].[sys_group] ([sys_group_id])
GO
ALTER TABLE [dbo].[sys_group_user_map] CHECK CONSTRAINT [fk_sgum_sg]
GO
ALTER TABLE [dbo].[sys_group_user_map]  WITH CHECK ADD  CONSTRAINT [fk_sgum_su] FOREIGN KEY([sys_user_id])
REFERENCES [dbo].[sys_user] ([sys_user_id])
GO
ALTER TABLE [dbo].[sys_group_user_map] CHECK CONSTRAINT [fk_sgum_su]
GO
ALTER TABLE [dbo].[sys_group_user_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgum_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_user_map] CHECK CONSTRAINT [ndx_fk_sgum_created]
GO
ALTER TABLE [dbo].[sys_group_user_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgum_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_user_map] CHECK CONSTRAINT [ndx_fk_sgum_modified]
GO
ALTER TABLE [dbo].[sys_group_user_map]  WITH CHECK ADD  CONSTRAINT [ndx_fk_sgum_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_group_user_map] CHECK CONSTRAINT [ndx_fk_sgum_owned]
GO
ALTER TABLE [dbo].[sys_index]  WITH CHECK ADD  CONSTRAINT [fk_si_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_index] CHECK CONSTRAINT [fk_si_created]
GO
ALTER TABLE [dbo].[sys_index]  WITH CHECK ADD  CONSTRAINT [fk_si_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_index] CHECK CONSTRAINT [fk_si_modified]
GO
ALTER TABLE [dbo].[sys_index]  WITH CHECK ADD  CONSTRAINT [fk_si_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_index] CHECK CONSTRAINT [fk_si_owned]
GO
ALTER TABLE [dbo].[sys_index]  WITH CHECK ADD  CONSTRAINT [fk_si_st] FOREIGN KEY([sys_table_id])
REFERENCES [dbo].[sys_table] ([sys_table_id])
GO
ALTER TABLE [dbo].[sys_index] CHECK CONSTRAINT [fk_si_st]
GO
ALTER TABLE [dbo].[sys_index_field]  WITH CHECK ADD  CONSTRAINT [fk_sif_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_index_field] CHECK CONSTRAINT [fk_sif_created]
GO
ALTER TABLE [dbo].[sys_index_field]  WITH CHECK ADD  CONSTRAINT [fk_sif_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_index_field] CHECK CONSTRAINT [fk_sif_modified]
GO
ALTER TABLE [dbo].[sys_index_field]  WITH CHECK ADD  CONSTRAINT [fk_sif_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_index_field] CHECK CONSTRAINT [fk_sif_owned]
GO
ALTER TABLE [dbo].[sys_index_field]  WITH CHECK ADD  CONSTRAINT [fk_sif_si] FOREIGN KEY([sys_index_id])
REFERENCES [dbo].[sys_index] ([sys_index_id])
GO
ALTER TABLE [dbo].[sys_index_field] CHECK CONSTRAINT [fk_sif_si]
GO
ALTER TABLE [dbo].[sys_index_field]  WITH CHECK ADD  CONSTRAINT [fk_sif_stf] FOREIGN KEY([sys_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_index_field] CHECK CONSTRAINT [fk_sif_stf]
GO
ALTER TABLE [dbo].[sys_lang]  WITH CHECK ADD  CONSTRAINT [fk_sl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_lang] CHECK CONSTRAINT [fk_sl_created]
GO
ALTER TABLE [dbo].[sys_lang]  WITH CHECK ADD  CONSTRAINT [fk_sl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_lang] CHECK CONSTRAINT [fk_sl_modified]
GO
ALTER TABLE [dbo].[sys_lang]  WITH CHECK ADD  CONSTRAINT [fk_sl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_lang] CHECK CONSTRAINT [fk_sl_owned]
GO
ALTER TABLE [dbo].[sys_permission]  WITH CHECK ADD  CONSTRAINT [fk_sp_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission] CHECK CONSTRAINT [fk_sp_created]
GO
ALTER TABLE [dbo].[sys_permission]  WITH CHECK ADD  CONSTRAINT [fk_sp_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission] CHECK CONSTRAINT [fk_sp_modified]
GO
ALTER TABLE [dbo].[sys_permission]  WITH CHECK ADD  CONSTRAINT [fk_sp_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission] CHECK CONSTRAINT [fk_sp_owned]
GO
ALTER TABLE [dbo].[sys_permission]  WITH CHECK ADD  CONSTRAINT [fk_sp_sr] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_permission] CHECK CONSTRAINT [fk_sp_sr]
GO
ALTER TABLE [dbo].[sys_permission]  WITH CHECK ADD  CONSTRAINT [fk_sp_st] FOREIGN KEY([sys_table_id])
REFERENCES [dbo].[sys_table] ([sys_table_id])
GO
ALTER TABLE [dbo].[sys_permission] CHECK CONSTRAINT [fk_sp_st]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_sp_srf] FOREIGN KEY([sys_dataview_field_id])
REFERENCES [dbo].[sys_dataview_field] ([sys_dataview_field_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_sp_srf]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_sp_stf] FOREIGN KEY([sys_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_sp_stf]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_spf_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_spf_created]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_spf_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_spf_modified]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_spf_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_spf_owned]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_spf_sp] FOREIGN KEY([sys_permission_id])
REFERENCES [dbo].[sys_permission] ([sys_permission_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_spf_sp]
GO
ALTER TABLE [dbo].[sys_permission_field]  WITH CHECK ADD  CONSTRAINT [fk_spf_stf] FOREIGN KEY([parent_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_permission_field] CHECK CONSTRAINT [fk_spf_stf]
GO
ALTER TABLE [dbo].[sys_permission_lang]  WITH CHECK ADD  CONSTRAINT [fk_spl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_permission_lang] CHECK CONSTRAINT [fk_spl_sl]
GO
ALTER TABLE [dbo].[sys_permission_lang]  WITH CHECK ADD  CONSTRAINT [fk_spl_sp] FOREIGN KEY([sys_permission_id])
REFERENCES [dbo].[sys_permission] ([sys_permission_id])
GO
ALTER TABLE [dbo].[sys_permission_lang] CHECK CONSTRAINT [fk_spl_sp]
GO
ALTER TABLE [dbo].[sys_permission_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_spl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission_lang] CHECK CONSTRAINT [ndx_fk_spl_created]
GO
ALTER TABLE [dbo].[sys_permission_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_spl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission_lang] CHECK CONSTRAINT [ndx_fk_spl_modified]
GO
ALTER TABLE [dbo].[sys_permission_lang]  WITH CHECK ADD  CONSTRAINT [ndx_fk_spl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_permission_lang] CHECK CONSTRAINT [ndx_fk_spl_owned]
GO
ALTER TABLE [dbo].[sys_search_autofield]  WITH CHECK ADD  CONSTRAINT [fk_ssa_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_search_autofield] CHECK CONSTRAINT [fk_ssa_created]
GO
ALTER TABLE [dbo].[sys_search_autofield]  WITH CHECK ADD  CONSTRAINT [fk_ssa_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_search_autofield] CHECK CONSTRAINT [fk_ssa_modified]
GO
ALTER TABLE [dbo].[sys_search_autofield]  WITH CHECK ADD  CONSTRAINT [fk_ssa_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_search_autofield] CHECK CONSTRAINT [fk_ssa_owned]
GO
ALTER TABLE [dbo].[sys_search_autofield]  WITH CHECK ADD  CONSTRAINT [fk_ssa_stf] FOREIGN KEY([sys_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_search_autofield] CHECK CONSTRAINT [fk_ssa_stf]
GO
ALTER TABLE [dbo].[sys_search_resolver]  WITH CHECK ADD  CONSTRAINT [fk_ssr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_search_resolver] CHECK CONSTRAINT [fk_ssr_created]
GO
ALTER TABLE [dbo].[sys_search_resolver]  WITH CHECK ADD  CONSTRAINT [fk_ssr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_search_resolver] CHECK CONSTRAINT [fk_ssr_modified]
GO
ALTER TABLE [dbo].[sys_search_resolver]  WITH CHECK ADD  CONSTRAINT [fk_ssr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_search_resolver] CHECK CONSTRAINT [fk_ssr_owned]
GO
ALTER TABLE [dbo].[sys_search_resolver]  WITH CHECK ADD  CONSTRAINT [fk_ssr_sd] FOREIGN KEY([sys_dataview_id])
REFERENCES [dbo].[sys_dataview] ([sys_dataview_id])
GO
ALTER TABLE [dbo].[sys_search_resolver] CHECK CONSTRAINT [fk_ssr_sd]
GO
ALTER TABLE [dbo].[sys_search_resolver]  WITH CHECK ADD  CONSTRAINT [fk_ssr_st] FOREIGN KEY([sys_table_id])
REFERENCES [dbo].[sys_table] ([sys_table_id])
GO
ALTER TABLE [dbo].[sys_search_resolver] CHECK CONSTRAINT [fk_ssr_st]
GO
ALTER TABLE [dbo].[sys_table]  WITH CHECK ADD  CONSTRAINT [fk_st_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table] CHECK CONSTRAINT [fk_st_created]
GO
ALTER TABLE [dbo].[sys_table]  WITH CHECK ADD  CONSTRAINT [fk_st_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table] CHECK CONSTRAINT [fk_st_modified]
GO
ALTER TABLE [dbo].[sys_table]  WITH CHECK ADD  CONSTRAINT [fk_st_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table] CHECK CONSTRAINT [fk_st_owned]
GO
ALTER TABLE [dbo].[sys_table_field]  WITH CHECK ADD  CONSTRAINT [fk_stf_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_field] CHECK CONSTRAINT [fk_stf_created]
GO
ALTER TABLE [dbo].[sys_table_field]  WITH CHECK ADD  CONSTRAINT [fk_stf_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_field] CHECK CONSTRAINT [fk_stf_modified]
GO
ALTER TABLE [dbo].[sys_table_field]  WITH CHECK ADD  CONSTRAINT [fk_stf_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_field] CHECK CONSTRAINT [fk_stf_owned]
GO
ALTER TABLE [dbo].[sys_table_field]  WITH CHECK ADD  CONSTRAINT [fk_stf_st] FOREIGN KEY([sys_table_id])
REFERENCES [dbo].[sys_table] ([sys_table_id])
GO
ALTER TABLE [dbo].[sys_table_field] CHECK CONSTRAINT [fk_stf_st]
GO
ALTER TABLE [dbo].[sys_table_field]  WITH CHECK ADD  CONSTRAINT [fk_stf_stffk] FOREIGN KEY([foreign_key_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_table_field] CHECK CONSTRAINT [fk_stf_stffk]
GO
ALTER TABLE [dbo].[sys_table_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_stfl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_field_lang] CHECK CONSTRAINT [fk_stfl_created]
GO
ALTER TABLE [dbo].[sys_table_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_stfl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_field_lang] CHECK CONSTRAINT [fk_stfl_modified]
GO
ALTER TABLE [dbo].[sys_table_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_stfl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_field_lang] CHECK CONSTRAINT [fk_stfl_owned]
GO
ALTER TABLE [dbo].[sys_table_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_stfl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_table_field_lang] CHECK CONSTRAINT [fk_stfl_sl]
GO
ALTER TABLE [dbo].[sys_table_field_lang]  WITH CHECK ADD  CONSTRAINT [fk_stfl_stf] FOREIGN KEY([sys_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_table_field_lang] CHECK CONSTRAINT [fk_stfl_stf]
GO
ALTER TABLE [dbo].[sys_table_lang]  WITH CHECK ADD  CONSTRAINT [fk_stl_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_lang] CHECK CONSTRAINT [fk_stl_created]
GO
ALTER TABLE [dbo].[sys_table_lang]  WITH CHECK ADD  CONSTRAINT [fk_stl_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_lang] CHECK CONSTRAINT [fk_stl_modified]
GO
ALTER TABLE [dbo].[sys_table_lang]  WITH CHECK ADD  CONSTRAINT [fk_stl_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_lang] CHECK CONSTRAINT [fk_stl_owned]
GO
ALTER TABLE [dbo].[sys_table_lang]  WITH CHECK ADD  CONSTRAINT [fk_stl_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[sys_table_lang] CHECK CONSTRAINT [fk_stl_sl]
GO
ALTER TABLE [dbo].[sys_table_lang]  WITH CHECK ADD  CONSTRAINT [fk_stl_st] FOREIGN KEY([sys_table_id])
REFERENCES [dbo].[sys_table] ([sys_table_id])
GO
ALTER TABLE [dbo].[sys_table_lang] CHECK CONSTRAINT [fk_stl_st]
GO
ALTER TABLE [dbo].[sys_table_relationship]  WITH CHECK ADD  CONSTRAINT [fk_str_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_relationship] CHECK CONSTRAINT [fk_str_created]
GO
ALTER TABLE [dbo].[sys_table_relationship]  WITH CHECK ADD  CONSTRAINT [fk_str_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_relationship] CHECK CONSTRAINT [fk_str_modified]
GO
ALTER TABLE [dbo].[sys_table_relationship]  WITH CHECK ADD  CONSTRAINT [fk_str_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_table_relationship] CHECK CONSTRAINT [fk_str_owned]
GO
ALTER TABLE [dbo].[sys_table_relationship]  WITH CHECK ADD  CONSTRAINT [fk_str_stf] FOREIGN KEY([sys_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_table_relationship] CHECK CONSTRAINT [fk_str_stf]
GO
ALTER TABLE [dbo].[sys_table_relationship]  WITH CHECK ADD  CONSTRAINT [fk_str_stf_other] FOREIGN KEY([other_table_field_id])
REFERENCES [dbo].[sys_table_field] ([sys_table_field_id])
GO
ALTER TABLE [dbo].[sys_table_relationship] CHECK CONSTRAINT [fk_str_stf_other]
GO
ALTER TABLE [dbo].[sys_user]  WITH CHECK ADD  CONSTRAINT [fk_su_co] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user] CHECK CONSTRAINT [fk_su_co]
GO
ALTER TABLE [dbo].[sys_user]  WITH CHECK ADD  CONSTRAINT [fk_su_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user] CHECK CONSTRAINT [fk_su_created]
GO
ALTER TABLE [dbo].[sys_user]  WITH CHECK ADD  CONSTRAINT [fk_su_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user] CHECK CONSTRAINT [fk_su_modified]
GO
ALTER TABLE [dbo].[sys_user]  WITH CHECK ADD  CONSTRAINT [fk_su_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user] CHECK CONSTRAINT [fk_su_owned]
GO
ALTER TABLE [dbo].[sys_user_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sup_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user_permission_map] CHECK CONSTRAINT [fk_sup_created]
GO
ALTER TABLE [dbo].[sys_user_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sup_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user_permission_map] CHECK CONSTRAINT [fk_sup_modified]
GO
ALTER TABLE [dbo].[sys_user_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sup_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[sys_user_permission_map] CHECK CONSTRAINT [fk_sup_owned]
GO
ALTER TABLE [dbo].[sys_user_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sup_sp] FOREIGN KEY([sys_permission_id])
REFERENCES [dbo].[sys_permission] ([sys_permission_id])
GO
ALTER TABLE [dbo].[sys_user_permission_map] CHECK CONSTRAINT [fk_sup_sp]
GO
ALTER TABLE [dbo].[sys_user_permission_map]  WITH CHECK ADD  CONSTRAINT [fk_sup_su] FOREIGN KEY([sys_user_id])
REFERENCES [dbo].[sys_user] ([sys_user_id])
GO
ALTER TABLE [dbo].[sys_user_permission_map] CHECK CONSTRAINT [fk_sup_su]
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map]  WITH CHECK ADD  CONSTRAINT [fk_tafm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map] CHECK CONSTRAINT [fk_tafm_created]
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map]  WITH CHECK ADD  CONSTRAINT [fk_tafm_f] FOREIGN KEY([taxonomy_family_id])
REFERENCES [dbo].[taxonomy_family] ([taxonomy_family_id])
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map] CHECK CONSTRAINT [fk_tafm_f]
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map]  WITH CHECK ADD  CONSTRAINT [fk_tafm_g] FOREIGN KEY([taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map] CHECK CONSTRAINT [fk_tafm_g]
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map]  WITH CHECK ADD  CONSTRAINT [fk_tafm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map] CHECK CONSTRAINT [fk_tafm_modified]
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map]  WITH CHECK ADD  CONSTRAINT [fk_tafm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_alt_family_map] CHECK CONSTRAINT [fk_tafm_owned]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_c]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_created]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_modified]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_owned]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_tf] FOREIGN KEY([taxonomy_family_id])
REFERENCES [dbo].[taxonomy_family] ([taxonomy_family_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_tf]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_tg] FOREIGN KEY([taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_tg]
GO
ALTER TABLE [dbo].[taxonomy_attach]  WITH CHECK ADD  CONSTRAINT [fk_tat_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_attach] CHECK CONSTRAINT [fk_tat_ts]
GO
ALTER TABLE [dbo].[taxonomy_author]  WITH CHECK ADD  CONSTRAINT [fk_ta_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_author] CHECK CONSTRAINT [fk_ta_created]
GO
ALTER TABLE [dbo].[taxonomy_author]  WITH CHECK ADD  CONSTRAINT [fk_ta_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_author] CHECK CONSTRAINT [fk_ta_modified]
GO
ALTER TABLE [dbo].[taxonomy_author]  WITH CHECK ADD  CONSTRAINT [fk_ta_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_author] CHECK CONSTRAINT [fk_ta_owned]
GO
ALTER TABLE [dbo].[taxonomy_common_name]  WITH CHECK ADD  CONSTRAINT [fk_tcn_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_common_name] CHECK CONSTRAINT [fk_tcn_cit]
GO
ALTER TABLE [dbo].[taxonomy_common_name]  WITH CHECK ADD  CONSTRAINT [fk_tcn_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_common_name] CHECK CONSTRAINT [fk_tcn_created]
GO
ALTER TABLE [dbo].[taxonomy_common_name]  WITH CHECK ADD  CONSTRAINT [fk_tcn_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_common_name] CHECK CONSTRAINT [fk_tcn_modified]
GO
ALTER TABLE [dbo].[taxonomy_common_name]  WITH CHECK ADD  CONSTRAINT [fk_tcn_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_common_name] CHECK CONSTRAINT [fk_tcn_owned]
GO
ALTER TABLE [dbo].[taxonomy_common_name]  WITH CHECK ADD  CONSTRAINT [fk_tcn_tg] FOREIGN KEY([taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_common_name] CHECK CONSTRAINT [fk_tcn_tg]
GO
ALTER TABLE [dbo].[taxonomy_common_name]  WITH CHECK ADD  CONSTRAINT [fk_tcn_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_common_name] CHECK CONSTRAINT [fk_tcn_ts]
GO
ALTER TABLE [dbo].[taxonomy_crop_map]  WITH CHECK ADD  CONSTRAINT [fc_tcm_cr] FOREIGN KEY([crop_id])
REFERENCES [dbo].[crop] ([crop_id])
GO
ALTER TABLE [dbo].[taxonomy_crop_map] CHECK CONSTRAINT [fc_tcm_cr]
GO
ALTER TABLE [dbo].[taxonomy_crop_map]  WITH CHECK ADD  CONSTRAINT [fk_tcm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_crop_map] CHECK CONSTRAINT [fk_tcm_created]
GO
ALTER TABLE [dbo].[taxonomy_crop_map]  WITH CHECK ADD  CONSTRAINT [fk_tcm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_crop_map] CHECK CONSTRAINT [fk_tcm_modified]
GO
ALTER TABLE [dbo].[taxonomy_crop_map]  WITH CHECK ADD  CONSTRAINT [fk_tcm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_crop_map] CHECK CONSTRAINT [fk_tcm_owned]
GO
ALTER TABLE [dbo].[taxonomy_crop_map]  WITH CHECK ADD  CONSTRAINT [fk_tcm_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_crop_map] CHECK CONSTRAINT [fk_tcm_ts]
GO
ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_cit]
GO
ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_created]
GO
ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_modified]
GO
ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_owned]
GO
ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_ts]
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_cit]
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_created]
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_modified]
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_owned]
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_ts]
GO
ALTER TABLE [dbo].[taxonomy_family]  WITH CHECK ADD  CONSTRAINT [fk_tf_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_family] CHECK CONSTRAINT [fk_tf_created]
GO
ALTER TABLE [dbo].[taxonomy_family]  WITH CHECK ADD  CONSTRAINT [fk_tf_cur_tf] FOREIGN KEY([current_taxonomy_family_id])
REFERENCES [dbo].[taxonomy_family] ([taxonomy_family_id])
GO
ALTER TABLE [dbo].[taxonomy_family] CHECK CONSTRAINT [fk_tf_cur_tf]
GO
ALTER TABLE [dbo].[taxonomy_family]  WITH CHECK ADD  CONSTRAINT [fk_tf_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_family] CHECK CONSTRAINT [fk_tf_modified]
GO
ALTER TABLE [dbo].[taxonomy_family]  WITH CHECK ADD  CONSTRAINT [fk_tf_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_family] CHECK CONSTRAINT [fk_tf_owned]
GO
ALTER TABLE [dbo].[taxonomy_family]  WITH CHECK ADD  CONSTRAINT [fk_tf_tg] FOREIGN KEY([type_taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_family] CHECK CONSTRAINT [fk_tf_tg]
GO
ALTER TABLE [dbo].[taxonomy_genus]  WITH CHECK ADD  CONSTRAINT [fk_tg_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_genus] CHECK CONSTRAINT [fk_tg_created]
GO
ALTER TABLE [dbo].[taxonomy_genus]  WITH CHECK ADD  CONSTRAINT [fk_tg_cur_tgt] FOREIGN KEY([current_taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_genus] CHECK CONSTRAINT [fk_tg_cur_tgt]
GO
ALTER TABLE [dbo].[taxonomy_genus]  WITH CHECK ADD  CONSTRAINT [fk_tg_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_genus] CHECK CONSTRAINT [fk_tg_modified]
GO
ALTER TABLE [dbo].[taxonomy_genus]  WITH CHECK ADD  CONSTRAINT [fk_tg_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_genus] CHECK CONSTRAINT [fk_tg_owned]
GO
ALTER TABLE [dbo].[taxonomy_genus]  WITH CHECK ADD  CONSTRAINT [fk_tg_tf] FOREIGN KEY([taxonomy_family_id])
REFERENCES [dbo].[taxonomy_family] ([taxonomy_family_id])
GO
ALTER TABLE [dbo].[taxonomy_genus] CHECK CONSTRAINT [fk_tg_tf]
GO
ALTER TABLE [dbo].[taxonomy_geography_map]  WITH CHECK ADD  CONSTRAINT [fk_tgm_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_geography_map] CHECK CONSTRAINT [fk_tgm_cit]
GO
ALTER TABLE [dbo].[taxonomy_geography_map]  WITH CHECK ADD  CONSTRAINT [fk_tgm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_geography_map] CHECK CONSTRAINT [fk_tgm_created]
GO
ALTER TABLE [dbo].[taxonomy_geography_map]  WITH CHECK ADD  CONSTRAINT [fk_tgm_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[taxonomy_geography_map] CHECK CONSTRAINT [fk_tgm_g]
GO
ALTER TABLE [dbo].[taxonomy_geography_map]  WITH CHECK ADD  CONSTRAINT [fk_tgm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_geography_map] CHECK CONSTRAINT [fk_tgm_modified]
GO
ALTER TABLE [dbo].[taxonomy_geography_map]  WITH CHECK ADD  CONSTRAINT [fk_tgm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_geography_map] CHECK CONSTRAINT [fk_tgm_owned]
GO
ALTER TABLE [dbo].[taxonomy_geography_map]  WITH CHECK ADD  CONSTRAINT [fk_tgm_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_geography_map] CHECK CONSTRAINT [fk_tgm_ts]
GO
ALTER TABLE [dbo].[taxonomy_noxious]  WITH CHECK ADD  CONSTRAINT [fk_tn_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_noxious] CHECK CONSTRAINT [fk_tn_created]
GO
ALTER TABLE [dbo].[taxonomy_noxious]  WITH CHECK ADD  CONSTRAINT [fk_tn_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[taxonomy_noxious] CHECK CONSTRAINT [fk_tn_g]
GO
ALTER TABLE [dbo].[taxonomy_noxious]  WITH CHECK ADD  CONSTRAINT [fk_tn_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_noxious] CHECK CONSTRAINT [fk_tn_modified]
GO
ALTER TABLE [dbo].[taxonomy_noxious]  WITH CHECK ADD  CONSTRAINT [fk_tn_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_noxious] CHECK CONSTRAINT [fk_tn_owned]
GO
ALTER TABLE [dbo].[taxonomy_noxious]  WITH CHECK ADD  CONSTRAINT [fk_tn_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_noxious] CHECK CONSTRAINT [fk_tn_ts]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_c] FOREIGN KEY([verifier_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_c]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_created]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_cur_t] FOREIGN KEY([current_taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_cur_t]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_curator1] FOREIGN KEY([curator1_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_curator1]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_curator2] FOREIGN KEY([curator2_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_curator2]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_modified]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_owned]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_s] FOREIGN KEY([priority1_site_id])
REFERENCES [dbo].[site] ([site_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_s]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_s2] FOREIGN KEY([priority2_site_id])
REFERENCES [dbo].[site] ([site_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_s2]
GO
ALTER TABLE [dbo].[taxonomy_species]  WITH CHECK ADD  CONSTRAINT [fk_ts_tg] FOREIGN KEY([taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_species] CHECK CONSTRAINT [fk_ts_tg]
GO
ALTER TABLE [dbo].[taxonomy_use]  WITH CHECK ADD  CONSTRAINT [fk_tu_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_use] CHECK CONSTRAINT [fk_tu_cit]
GO
ALTER TABLE [dbo].[taxonomy_use]  WITH CHECK ADD  CONSTRAINT [fk_tus_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_use] CHECK CONSTRAINT [fk_tus_created]
GO
ALTER TABLE [dbo].[taxonomy_use]  WITH CHECK ADD  CONSTRAINT [fk_tus_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_use] CHECK CONSTRAINT [fk_tus_modified]
GO
ALTER TABLE [dbo].[taxonomy_use]  WITH CHECK ADD  CONSTRAINT [fk_tus_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_use] CHECK CONSTRAINT [fk_tus_owned]
GO
ALTER TABLE [dbo].[taxonomy_use]  WITH CHECK ADD  CONSTRAINT [fk_tus_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_use] CHECK CONSTRAINT [fk_tus_ts]
GO
ALTER TABLE [dbo].[w6_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_w6ivsi_i] FOREIGN KEY([split_inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[w6_site_inventory] CHECK CONSTRAINT [fk_w6ivsi_i]
GO
ALTER TABLE [dbo].[w6_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_w6si_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[w6_site_inventory] CHECK CONSTRAINT [fk_w6si_created]
GO
ALTER TABLE [dbo].[w6_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_w6si_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO
ALTER TABLE [dbo].[w6_site_inventory] CHECK CONSTRAINT [fk_w6si_i]
GO
ALTER TABLE [dbo].[w6_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_w6si_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[w6_site_inventory] CHECK CONSTRAINT [fk_w6si_modified]
GO
ALTER TABLE [dbo].[w6_site_inventory]  WITH CHECK ADD  CONSTRAINT [fk_w6si_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[w6_site_inventory] CHECK CONSTRAINT [fk_w6si_owned]
GO
ALTER TABLE [dbo].[web_cooperator]  WITH CHECK ADD  CONSTRAINT [fk_wc_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_cooperator] CHECK CONSTRAINT [fk_wc_created]
GO
ALTER TABLE [dbo].[web_cooperator]  WITH CHECK ADD  CONSTRAINT [fk_wc_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[web_cooperator] CHECK CONSTRAINT [fk_wc_g]
GO
ALTER TABLE [dbo].[web_cooperator]  WITH CHECK ADD  CONSTRAINT [fk_wc_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_cooperator] CHECK CONSTRAINT [fk_wc_modified]
GO
ALTER TABLE [dbo].[web_cooperator]  WITH CHECK ADD  CONSTRAINT [fk_wc_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_cooperator] CHECK CONSTRAINT [fk_wc_owned]
GO
ALTER TABLE [dbo].[web_help]  WITH CHECK ADD  CONSTRAINT [fk_wh_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[web_help] CHECK CONSTRAINT [fk_wh_created]
GO
ALTER TABLE [dbo].[web_help]  WITH CHECK ADD  CONSTRAINT [fk_wh_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[web_help] CHECK CONSTRAINT [fk_wh_modified]
GO
ALTER TABLE [dbo].[web_help]  WITH CHECK ADD  CONSTRAINT [fk_wh_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[web_help] CHECK CONSTRAINT [fk_wh_owned]
GO
ALTER TABLE [dbo].[web_help]  WITH CHECK ADD  CONSTRAINT [fk_wh_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[web_help] CHECK CONSTRAINT [fk_wh_sl]
GO
ALTER TABLE [dbo].[web_order_request]  WITH CHECK ADD  CONSTRAINT [fk_wor_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request] CHECK CONSTRAINT [fk_wor_created]
GO
ALTER TABLE [dbo].[web_order_request]  WITH CHECK ADD  CONSTRAINT [fk_wor_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request] CHECK CONSTRAINT [fk_wor_owned]
GO
ALTER TABLE [dbo].[web_order_request]  WITH CHECK ADD  CONSTRAINT [fk_wor_wc] FOREIGN KEY([web_cooperator_id])
REFERENCES [dbo].[web_cooperator] ([web_cooperator_id])
GO
ALTER TABLE [dbo].[web_order_request] CHECK CONSTRAINT [fk_wor_wc]
GO
ALTER TABLE [dbo].[web_order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_wora_c] FOREIGN KEY([web_cooperator_id])
REFERENCES [dbo].[web_cooperator] ([web_cooperator_id])
GO
ALTER TABLE [dbo].[web_order_request_action] CHECK CONSTRAINT [fk_wora_c]
GO
ALTER TABLE [dbo].[web_order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_wora_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_action] CHECK CONSTRAINT [fk_wora_created]
GO
ALTER TABLE [dbo].[web_order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_wora_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_action] CHECK CONSTRAINT [fk_wora_modified]
GO
ALTER TABLE [dbo].[web_order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_wora_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_action] CHECK CONSTRAINT [fk_wora_owned]
GO
ALTER TABLE [dbo].[web_order_request_action]  WITH CHECK ADD  CONSTRAINT [fk_wora_wor] FOREIGN KEY([web_order_request_id])
REFERENCES [dbo].[web_order_request] ([web_order_request_id])
GO
ALTER TABLE [dbo].[web_order_request_action] CHECK CONSTRAINT [fk_wora_wor]
GO
ALTER TABLE [dbo].[web_order_request_address]  WITH CHECK ADD  CONSTRAINT [fk_worad_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_address] CHECK CONSTRAINT [fk_worad_created]
GO
ALTER TABLE [dbo].[web_order_request_address]  WITH CHECK ADD  CONSTRAINT [fk_worad_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[web_order_request_address] CHECK CONSTRAINT [fk_worad_g]
GO
ALTER TABLE [dbo].[web_order_request_address]  WITH CHECK ADD  CONSTRAINT [fk_worad_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_address] CHECK CONSTRAINT [fk_worad_owned]
GO
ALTER TABLE [dbo].[web_order_request_address]  WITH CHECK ADD  CONSTRAINT [fk_worad_wor] FOREIGN KEY([web_order_request_id])
REFERENCES [dbo].[web_order_request] ([web_order_request_id])
GO
ALTER TABLE [dbo].[web_order_request_address] CHECK CONSTRAINT [fk_worad_wor]
GO
ALTER TABLE [dbo].[web_order_request_address]  WITH CHECK ADD  CONSTRAINT [fk_wurad_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_address] CHECK CONSTRAINT [fk_wurad_modified]
GO
ALTER TABLE [dbo].[web_order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_worat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_attach] CHECK CONSTRAINT [fk_worat_created]
GO
ALTER TABLE [dbo].[web_order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_worat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_attach] CHECK CONSTRAINT [fk_worat_modified]
GO
ALTER TABLE [dbo].[web_order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_worat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_attach] CHECK CONSTRAINT [fk_worat_owned]
GO
ALTER TABLE [dbo].[web_order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_worat_wc] FOREIGN KEY([web_cooperator_id])
REFERENCES [dbo].[web_cooperator] ([web_cooperator_id])
GO
ALTER TABLE [dbo].[web_order_request_attach] CHECK CONSTRAINT [fk_worat_wc]
GO
ALTER TABLE [dbo].[web_order_request_attach]  WITH CHECK ADD  CONSTRAINT [fk_worat_wor] FOREIGN KEY([web_order_request_id])
REFERENCES [dbo].[web_order_request] ([web_order_request_id])
GO
ALTER TABLE [dbo].[web_order_request_attach] CHECK CONSTRAINT [fk_worat_wor]
GO
ALTER TABLE [dbo].[web_order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_wori_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[web_order_request_item] CHECK CONSTRAINT [fk_wori_a]
GO
ALTER TABLE [dbo].[web_order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_wori_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_item] CHECK CONSTRAINT [fk_wori_created]
GO
ALTER TABLE [dbo].[web_order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_wori_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_item] CHECK CONSTRAINT [fk_wori_modified]
GO
ALTER TABLE [dbo].[web_order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_wori_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_order_request_item] CHECK CONSTRAINT [fk_wori_owned]
GO
ALTER TABLE [dbo].[web_order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_wori_wc] FOREIGN KEY([web_cooperator_id])
REFERENCES [dbo].[web_cooperator] ([web_cooperator_id])
GO
ALTER TABLE [dbo].[web_order_request_item] CHECK CONSTRAINT [fk_wori_wc]
GO
ALTER TABLE [dbo].[web_order_request_item]  WITH CHECK ADD  CONSTRAINT [fk_wori_wor] FOREIGN KEY([web_order_request_id])
REFERENCES [dbo].[web_order_request] ([web_order_request_id])
GO
ALTER TABLE [dbo].[web_order_request_item] CHECK CONSTRAINT [fk_wori_wor]
GO
ALTER TABLE [dbo].[web_user]  WITH CHECK ADD  CONSTRAINT [fk_wu_sl] FOREIGN KEY([sys_lang_id])
REFERENCES [dbo].[sys_lang] ([sys_lang_id])
GO
ALTER TABLE [dbo].[web_user] CHECK CONSTRAINT [fk_wu_sl]
GO
ALTER TABLE [dbo].[web_user]  WITH CHECK ADD  CONSTRAINT [fk_wu_wc] FOREIGN KEY([web_cooperator_id])
REFERENCES [dbo].[web_cooperator] ([web_cooperator_id])
GO
ALTER TABLE [dbo].[web_user] CHECK CONSTRAINT [fk_wu_wc]
GO
ALTER TABLE [dbo].[web_user_cart]  WITH CHECK ADD  CONSTRAINT [fk_wuc_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart] CHECK CONSTRAINT [fk_wuc_created]
GO
ALTER TABLE [dbo].[web_user_cart]  WITH CHECK ADD  CONSTRAINT [fk_wuc_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart] CHECK CONSTRAINT [fk_wuc_modified]
GO
ALTER TABLE [dbo].[web_user_cart]  WITH CHECK ADD  CONSTRAINT [fk_wuc_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart] CHECK CONSTRAINT [fk_wuc_owned]
GO
ALTER TABLE [dbo].[web_user_cart]  WITH CHECK ADD  CONSTRAINT [fk_wuc_wu] FOREIGN KEY([web_user_id])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart] CHECK CONSTRAINT [fk_wuc_wu]
GO
ALTER TABLE [dbo].[web_user_cart_item]  WITH CHECK ADD  CONSTRAINT [fk_wuci_a] FOREIGN KEY([accession_id])
REFERENCES [dbo].[accession] ([accession_id])
GO
ALTER TABLE [dbo].[web_user_cart_item] CHECK CONSTRAINT [fk_wuci_a]
GO
ALTER TABLE [dbo].[web_user_cart_item]  WITH CHECK ADD  CONSTRAINT [fk_wuci_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart_item] CHECK CONSTRAINT [fk_wuci_created]
GO
ALTER TABLE [dbo].[web_user_cart_item]  WITH CHECK ADD  CONSTRAINT [fk_wuci_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart_item] CHECK CONSTRAINT [fk_wuci_modified]
GO
ALTER TABLE [dbo].[web_user_cart_item]  WITH CHECK ADD  CONSTRAINT [fk_wuci_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_cart_item] CHECK CONSTRAINT [fk_wuci_owned]
GO
ALTER TABLE [dbo].[web_user_cart_item]  WITH CHECK ADD  CONSTRAINT [fk_wuci_wuc] FOREIGN KEY([web_user_cart_id])
REFERENCES [dbo].[web_user_cart] ([web_user_cart_id])
GO
ALTER TABLE [dbo].[web_user_cart_item] CHECK CONSTRAINT [fk_wuci_wuc]
GO
ALTER TABLE [dbo].[web_user_preference]  WITH CHECK ADD  CONSTRAINT [fk_wup_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_preference] CHECK CONSTRAINT [fk_wup_created]
GO
ALTER TABLE [dbo].[web_user_preference]  WITH CHECK ADD  CONSTRAINT [fk_wup_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_preference] CHECK CONSTRAINT [fk_wup_modified]
GO
ALTER TABLE [dbo].[web_user_preference]  WITH CHECK ADD  CONSTRAINT [fk_wup_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_preference] CHECK CONSTRAINT [fk_wup_owned]
GO
ALTER TABLE [dbo].[web_user_preference]  WITH CHECK ADD  CONSTRAINT [fk_wup_wu] FOREIGN KEY([web_user_id])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_preference] CHECK CONSTRAINT [fk_wup_wu]
GO
ALTER TABLE [dbo].[web_user_shipping_address]  WITH CHECK ADD  CONSTRAINT [fk_wusa_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_shipping_address] CHECK CONSTRAINT [fk_wusa_created]
GO
ALTER TABLE [dbo].[web_user_shipping_address]  WITH CHECK ADD  CONSTRAINT [fk_wusa_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[web_user_shipping_address] CHECK CONSTRAINT [fk_wusa_g]
GO
ALTER TABLE [dbo].[web_user_shipping_address]  WITH CHECK ADD  CONSTRAINT [fk_wusa_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_shipping_address] CHECK CONSTRAINT [fk_wusa_modified]
GO
ALTER TABLE [dbo].[web_user_shipping_address]  WITH CHECK ADD  CONSTRAINT [fk_wusa_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_shipping_address] CHECK CONSTRAINT [fk_wusa_owned]
GO
ALTER TABLE [dbo].[web_user_shipping_address]  WITH CHECK ADD  CONSTRAINT [fk_wusa_wu] FOREIGN KEY([web_user_id])
REFERENCES [dbo].[web_user] ([web_user_id])
GO
ALTER TABLE [dbo].[web_user_shipping_address] CHECK CONSTRAINT [fk_wusa_wu]
GO
